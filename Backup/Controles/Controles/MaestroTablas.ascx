﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MaestroTablas.ascx.vb" Inherits="Controles.MaestroTablas" %>
<%@ Register src="CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc1" %>
<%@ Register src="NuevaTabla.ascx" tagname="NuevaTabla" tagprefix="uc2" %>
<%@ Register src="CtCombo.ascx" tagname="CtCombo" tagprefix="uc3" %>

<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc3" %>
<%@ Register src="NuevaEmpresa.ascx" tagname="NuevaEmpresa" tagprefix="uc2" %>
<table style="width: 100%;" align="center">
<tr>
<td align="center" class="titulo">
    EMPRESA
    <uc3:CtlMensajes ID="CtlMensajes1" runat="server" />
</td>
</tr>
</table>

<table style="width: 100%">
	<tr>
		<td style="width:100%;">
		<center>
		<fieldset style="width:320px;" >
			<legend>
			    FILTRAR POR
			</legend>
			
			<table style="width:100%;">
				<tr>
				        <td align="center" style="width:110px">
					        <asp:Label id="Label1" runat="server" Font-Size="11px" Text="TABLA"></asp:Label>
					        <br />
                            <uc3:CtCombo ID="cboTabla" runat="server" Activa="true" Procedimiento="TAB001" Condicion="" AutoPostBack="true" />
                        </td>
                        <td align="center" style="width:110px">
					        <asp:Label id="Label2" runat="server" Font-Size="11px" Text="ELEMENTO"></asp:Label>
					        <br />
                            <uc3:CtCombo ID="CboElemento" runat="server" Activa="true" Procedimiento="TAB002" Condicion="" />
                        </td>
				        <td align="center" style="width: 110px">
					        <asp:Label id="Label3" runat="server" Font-Size="11px" Text="DESCRIPCION CORTA"></asp:Label>
					        <br />
                            <asp:TextBox id="txtDescripcion" runat="server" Font-Size="11px"></asp:TextBox>									
                        </td>
                        <td align="center" style="width: 110px">
					        <asp:Label id="Label4" runat="server" Font-Size="11px" Text="DESCRIPCION LARGA"></asp:Label>
					        <br />
                            <asp:TextBox id="txtDescripcionLarga" runat="server" Font-Size="11px"></asp:TextBox>									
                        </td>
				        <td style="text-align:right" width="35px" >
					        <div class="curvo">
					            <asp:ImageButton id="imgBuscar" runat="server" Height="30px" Width="35px" ImageUrl="~/imagenes/boton busqueda.jpg" />
					            <asp:Label id="Label14" runat="server" Font-Size="11px" Text="Buscar"></asp:Label>
					    </td>
				    </tr>
			</table>
	    </fieldset>
	    </center>
		</td>
	</tr>
	<tr>
		<td>
		<center>
		<fieldset style="width:300px;">
		<legend>ELEMENTOS ECONTRADOS</legend>
		    <center>		    
		    <uc1:CtlGrilla ID="CtlGrilla1" runat="server" Largo="300px" Ancho="300px" Activa_ckeck="false" Activa_option="false" Desactiva_Boton="false" OpocionNuevo="true"/>		    
		    </center>
	    </fieldset>
	    </center>
		</td>
	</tr>
</table>

<asp:Panel ID="pnlNuevaTabla" runat="server" Visible="false">
<div style="position:absolute; top:200px; left:30%;" class="fondo1">
<table>
<tr>
<td>
    <uc2:NuevaTabla ID="NuevaTablaN" runat="server" titulo="Nuevo Elemento" />
</td>
</tr>
</table>
</div>
</asp:Panel>

<asp:Panel ID="pnlModificaTabla" runat="server" Visible="false">
<div style="position:absolute; top:200px; left:30%;" class="fondo1">
<table>
<tr>
<td>
    <uc2:NuevaTabla ID="NuevaTalaM" runat="server" titulo="Nueva tabla" />
</td>
</tr>
</table>
</div>
</asp:Panel>