﻿Imports ConDB
Partial Public Class NuevaTabla
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Property idElemento() As String
        Get
            Return txtidelemento.Text
        End Get
        Set(ByVal value As String)
            txtidelemento.Text = value
        End Set
    End Property

    Public Property idTabla() As String
        Get
            Return txtidtabla.Text
        End Get
        Set(ByVal value As String)
            txtidtabla.Text = value
        End Set
    End Property

    Public Property DescCorta() As String
        Get
            Return txtDescripcionCorta.Text
        End Get
        Set(ByVal value As String)
            txtDescripcionCorta.Text = value
        End Set
    End Property

    Public Property DescLarga() As String
        Get
            Return txtDescripcionLarga.Text
        End Get
        Set(ByVal value As String)
            txtDescripcionLarga.Text = value
        End Set
    End Property


    Public Property Tipo() As String
        Get
            Return lblTipo.Text
        End Get
        Set(ByVal value As String)
            lblTipo.Text = value
            If value = "Modificar" Then
                txtidtabla.ReadOnly = True
                txtidtabla.BackColor = Drawing.Color.Yellow
            End If
        End Set
    End Property

    Protected Sub imgGrabar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgGrabar.Click
        If txtidelemento.Text = "" Or txtDescripcionCorta.Text = "" Or txtDescripcionLarga.Text = "" Then
            CtlMensajes1.Mensaje("faltan ingresar datos obligatorios...", "")
            Exit Sub
        End If
        Dim ctlcon As New ConDB.ConSQL

        If Tipo = "Nuevo" Then
            ctlcon.FunctionGlobal(":pidtablaƒ:pidelementoƒ:pdesccortaƒ:pdesclarga▓" & idTabla.Trim() & "ƒ" & idElemento.Trim() & "ƒ" & Trim(DescCorta) & "ƒ" & Trim(DescLarga), "TAB004")
        ElseIf Tipo = "Modificar" Then
            ctlcon.FunctionGlobal(":pidtablaƒ:pidelementoƒ:pdesccortaƒ:pdesclarga▓" & idTabla.Trim() & "ƒ" & idElemento.Trim() & "ƒ" & Trim(DescCorta) & "ƒ" & Trim(DescLarga), "TAB005")
        End If
        CtlMensajes1.Mensaje("Se agrego el nuevo elemento satisfactoriamente")
        RaiseEvent Fin_Grabar()        
        Call imgCerrar_Click(sender, e)
    End Sub

    Private Sub imgCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCerrar.Click
        RaiseEvent Cerrar()
        Me.Visible = False
    End Sub

    Event Fin_Grabar()
    Event Cerrar()

End Class