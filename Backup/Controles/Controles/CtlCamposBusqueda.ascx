﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlCamposBusqueda.ascx.vb" Inherits="Controles.CtlCamposBusqueda" %>

<%@ Register src="~/Controles/CtCombo.ascx" tagname="CtCombo" tagprefix="uc2" %>
<%@ Register src="~/Controles/CtlTxt.ascx" tagname="CtlTxt" tagprefix="uc3" %>
<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc1" %>

<table style="width: 100%">
        <tr>
            <td class="titulo">
            <center>
                <asp:Label ID="lblTituloControl" runat="server" Text="" ForeColor="White" Font-Bold="true" Font-Size="16px"></asp:Label>
            </center>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                <legend>FILTRAR POR<uc1:CtlMensajes ID="CtlMensajes1" runat="server" /></legend>
                <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
			        <td><asp:Label ID="lblCartera" runat="server" Text="CARTERA" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" /></td>
			        <td><asp:Label ID="lblGestor" runat="server" Text="GESTOR" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" /></td>
			        <td><asp:Label ID="lblCondic" runat="server" Text="CONDIC." ForeColor="Maroon" Font-Size="10px" Font-Bold="true" /></td>
			        <td><asp:Label ID="lblDNI" runat="server" Text="DNI" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" Visible="false" /></td>
			        <td><asp:Label ID="lblRefin" runat="server" Text="REFIN." ForeColor="Maroon" Font-Size="10px" Font-Bold="true" Visible="false" /></td>
			        <td><asp:Label ID="lblCampaña" runat="server" Text="CAMPAÑA" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" Visible="false" /></td>
			        <td><asp:Label ID="lblProducto" runat="server" Text="PRODUCTO" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" Visible="false" /></td>
			        <td><asp:Label ID="lblNegocio" runat="server" Text="NEGOCIO" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" Visible="false" /></td>
			        <td><asp:Label ID="lblNroOperacion" runat="server" Text="Nro.OPERACION" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" Visible="false" /></td>
			        <td><asp:Label ID="lblNroCuenta" runat="server" Text="Nro.DE CTA." ForeColor="Maroon" Font-Size="10px" Font-Bold="true" Visible="false" /></td>
			        <td><asp:Label ID="lblCiente" runat="server" Text="NOMBRE DE CLIENTE" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" /></td>
			        <td><asp:Label ID="lblTelefono" runat="server" Text="TELEFONO" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" Visible="false" /></td>
			        <td><asp:Label ID="lblTipoCartera" runat="server" Text="TIPO CART." ForeColor="Maroon" Font-Size="10px" Font-Bold="true" Visible="false" /></td>
			        <td><asp:Label ID="lblDiasMora" runat="server" Text="DIAS MORA" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" Visible="false" /></td>
			        <td style="text-align:center" rowspan="2">
			            <table style="background-color:#CFCFCF; border:1px solid;">
			                <tr>
			                    <td style="text-align:center; color:Green;">
			                        APROBADAS
			                        <br />
			                        <asp:Button ID="lblAprobadasPropuesta" runat="server" Text="0" />
			                    </td>
			                    <td style="text-align:center; color:Yellow; background-color:Gray;">
			                        PENDIENTE
			                        <br />
			                        <asp:Button ID="lblPednientesPropuesta" runat="server" Text="0" />
			                    </td>
			                    <td style="text-align:center; color:Red">
			                        RECHAZADAS			                    
			                        <br />
			                        <asp:Button ID="lblRechazadasPropuesta" runat="server" Text="0" />
			                    </td>
			                </tr>
			            </table>
			        </td>
			        <td style="text-align:center" rowspan="2">
			            <div class="curvo">
			                <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Imagenes/BotonBusquedaPequena.png" ToolTip="Buscar" Width="35px" Height="30px"  />
			                <asp:Label ID="lblBuscar" runat="server" text="Buscar" />
			            </div>
			        </td>
		        </tr>
		        <tr>
			        <td><uc2:CtCombo ID="cboCartera" runat="server" Longitud="80" /></td>
			        <td><uc2:CtCombo ID="cboGestor" runat="server" Longitud="40"/></td>
			        <td><uc2:CtCombo ID="cboCondicion" runat="server" Longitud="40"/></td>
			        <td><uc3:CtlTxt ID="txtDNI" runat="server" Ancho="80" Visible="false" /></td>
			        <td><uc3:CtlTxt ID="txtRefin" runat="server" Ancho="40" Visible="false"/></td>
			        <td><uc3:CtlTxt ID="txtCampaña" runat="server" Ancho="80" Visible="false"/></td>
			        <td><uc3:CtlTxt ID="txtProducto" runat="server" Ancho="80" Visible="false"/></td>
			        <td><uc3:CtlTxt ID="txtNegocio" runat="server" Ancho="80" Visible="false"/></td>
			        <td><uc3:CtlTxt ID="txtNroOperacion" runat="server" Ancho="80" Visible="false"/></td>
			        <td><uc3:CtlTxt ID="txtNroCuenta" runat="server" Ancho="80" Visible="false" /></td>
			        <td><uc3:CtlTxt ID="txtCliente" runat="server" Ancho="200" /></td>
			        <td><uc3:CtlTxt ID="txtTelefono" runat="server" Ancho="80" Visible="false"/></td> 
			        <td><uc2:CtCombo ID="cboTipoCartera" runat="server" Longitud="60" Visible="false"/></td>
			        <td><uc2:CtCombo ID="cboDiasMora" runat="server" Longitud="80" Visible="false"/></td>
		        </tr>
                </table>
                </fieldset>
            </td>
        </tr>
	</table>