﻿Imports ConDB

Partial Public Class MaestroTablas
    Inherits System.Web.UI.UserControl
    Dim ctlnuevo As New ConDB.ConSQL
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            cboTabla.Limpia()
            cboTabla.Condicion = ":pidElemento▓" & "0"
            cboTabla.Cargar_dll()
        End If
    End Sub

    Private Sub cboTabla_Click() Handles cboTabla.Click
        CboElemento.Limpia()
        CboElemento.Condicion = ":pidtablaƒ:pidelemento▓" & Trim(cboTabla.Value) & "ƒ" & Trim(Val(CboElemento.Value))
        CboElemento.Cargar_dll()
    End Sub

    Protected Sub imgBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBuscar.Click
        CtlGrilla1.SourceDataTable = ctlnuevo.FunctionGlobal(":pidtablaƒ:pidelementoƒ:pDescCortaƒ:pDescLarga▓" & cboTabla.Value & "ƒ" & CboElemento.Value & "ƒ" & txtDescripcion.Text & "ƒ" & txtDescripcionLarga.Text, "TAB003")
    End Sub
    Private Sub CtlGrilla1_Nuevo() Handles CtlGrilla1.Nuevo
        NuevaTalaM.Tipo = "Nuevo"
        NuevaTalaM.idTabla = cboTabla.Value
        NuevaTalaM.Visible = True
        pnlNuevaTabla.Visible = True
    End Sub

    Private Sub CtlGrilla1_RowDeleting(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles CtlGrilla1.RowDeleting        
        Dim row As GridViewRow = GV.Rows(e.RowIndex)
        ctlnuevo.FunctionGlobal(":pidtablaƒ:pidelementoƒ:pDescCortaƒ:pDescLarga▓" & row.Cells(4).Text & "ƒ" & row.Cells(5).Text & "ƒ" & row.Cells(6).Text & "ƒ" & row.Cells(7).Text, "TAB006")
        CtlMensajes1.Mensaje("El elmento fue removido satisfactoriamente", "")
    End Sub

    Private Sub CtlGrilla1_RowEditing(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs, ByVal row As System.Web.UI.WebControls.GridViewRow) Handles CtlGrilla1.RowEditing
        NuevaTalaM.idTabla = row.Cells(4).Text
        NuevaTalaM.idElemento = row.Cells(5).Text
        NuevaTalaM.DescCorta = row.Cells(6).Text
        NuevaTalaM.DescLarga = row.Cells(7).Text
        NuevaTalaM.Visible = True
        NuevaTalaM.Tipo = "Modificar"
        pnlModificaTabla.Visible = True
    End Sub

    Private Sub NuevaTablaN_Cerrar() Handles NuevaTablaN.Cerrar
        NuevaTalaM.Visible = True
        pnlModificaTabla.Visible = False
        pnlNuevaTabla.Visible = False
        cboTabla.Limpia()
        cboTabla.Condicion = ":pidElemento▓" & "0"
        cboTabla.Cargar_dll()
        CtlGrilla1.SourceDataTable = ctlnuevo.FunctionGlobal(":pidtablaƒ:pidelementoƒ:pDescCortaƒ:pDescLarga▓" & cboTabla.Value & "ƒ" & CboElemento.Value & "ƒ" & txtDescripcion.Text & "ƒ" & txtDescripcionLarga.Text, "TAB003")
    End Sub
End Class