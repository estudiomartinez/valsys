﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlAdmGrupos.ascx.vb" Inherits="Controles.CtlAdmGrupos" %>
<%@ Register src="CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc1" %>
<%@ Register src="CtCombo.ascx" tagname="CtCombo" tagprefix="uc3" %>
<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc3" %>
<%@ Register src="CtlNuevoGrupo.ascx" tagname="CtlNuevoGrupo" tagprefix="uc4" %>
<table style="width: 100%;" align="center">
<tr>
<td align="center" class="titulo">
    GRUPOS
    <uc3:CtlMensajes ID="CtlMensajes1" runat="server" />
</td>
</tr>
</table>

<table style="width: 100%">
	<tr>
		<td style="width:100%;">
		<center>
		<fieldset style="width:320px;" >
			<legend>
			    FILTRAR POR
			</legend>
			
			<table style="width:100%;">
				<tr>
				        <td align="center" style="width:110px">
					        <asp:Label id="Label1" runat="server" Font-Size="11px" Text="CODIGO"></asp:Label>
					        <br />
                            <asp:TextBox id="txtIdGrupo" runat="server" Font-Size="11px"></asp:TextBox>									
                        </td>
				        <td align="center" style="width: 110px">
					        <asp:Label id="Label3" runat="server" Font-Size="11px" Text="GRUPOS"></asp:Label>
					        <br />
                            <asp:TextBox id="txtEmpresa" runat="server" Font-Size="11px"></asp:TextBox>									
                        </td>
                        <td align="center" style="width:110px">
					        <asp:Label id="Label2" runat="server" Font-Size="11px" Text="EMPRESA"></asp:Label>
					        <br />
                            <td><uc3:CtCombo ID="cboEmpresa" runat="server" Longitud="80" /></td>
                        </td>
				        <td style="text-align:right" width="35px" >
					        <div class="curvo">
					            <asp:ImageButton id="imgBuscar" runat="server" Height="30px" Width="35px" ImageUrl="~/imagenes/boton busqueda.jpg" />
					            <asp:Label id="Label14" runat="server" Font-Size="11px" Text="Buscar"></asp:Label>
					    </td>
				    </tr>
			</table>
	    </fieldset>
	    </center>
		</td>
	</tr>
	<tr>
		<td>
		<center>
		<fieldset style="width:300px;">
		<legend>GRUPOS ENCONTRADAS</legend>
		    <center>		    
		    <uc1:CtlGrilla ID="CtlGrilla1" runat="server" Largo="300px" Ancho="300px" Activa_ckeck="false" Activa_option="false" Desactiva_Boton="false" OpocionNuevo="true"/>		    
		    </center>
	    </fieldset>
	    </center>
		</td>
	</tr>
</table>

<asp:Panel ID="pnlGrupo" runat="server" Visible="false">
<div style="position:absolute; top:200px; left:30%;" class="fondo1">
<table>
<tr>
<td>
    <uc4:CtlNuevoGrupo ID="NuevaGrupo" runat="server" />
</td>
</tr>
</table>
</div>
</asp:Panel>
