﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="TipoCambio.ascx.vb" Inherits="Controles.TipoCambio" %>
<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc1" %>
<%@ Register src="CtCombo.ascx" tagname="CtCombo" tagprefix="uc2" %>
<asp:Label ID="lblId_cartera" runat="server" Visible="false"></asp:Label>
<table style="width: 100%;" align="center">
<tr align="center">
<td align="center" style="text-align:center;" class="titulo">
    TIPO DE CAMBIO
    <uc1:CtlMensajes ID="CtlMensajes1" runat="server" />
</td>
</tr>
<tr align="center" style="text-align:center;">
<td style="text-align:center; margin:auto;" align="center">
    <fieldset>
    <legend>AJUSTE DE TIPO DE CAMBIO</legend>
    <table style="text-align:center; margin:auto; background-color:#DCDCDC; border-radius:1em; padding:1em;">
        <tr>
            <td colspan="2">
                EMPRESA
                <br />
                <uc2:CtCombo ID="cboEmpresa" runat="server" Procedimiento="QRYMG002" Longitud="150" AutoPostBack="true"/>
            </td>
        </tr>
		<tr>
			<td align="right" style="text-align:right;">
			<asp:Label id="Label1" runat="server" Font-Size="11px" Text="Tipo de Cambio:"></asp:Label>
			</td>
			<td align="center" style="text-align:center;">
			<asp:TextBox id="TextBox1" runat="server" Font-Size="11px" Width="142px"></asp:TextBox>
			<asp:Label id="lblIdTipoCambio" runat="server" Font-Size="9px" Visible="false"></asp:Label>
			</td>
		</tr>
		<tr>
			<td align="right" style="text-align:right;">
			<asp:Label id="Label2" runat="server" Font-Size="11px" Text="Última Fecha Act:"></asp:Label>
			</td>
			<td align="center" style="text-align:center;">
			<asp:Label id="Label3" runat="server" Font-Size="11px"></asp:Label>
			</td>
		</tr>
		<tr align="center" style="text-align:center;">
			<td style="text-align:center;" align="center" colspan="2">
			<div class="curvo">
			<asp:ImageButton id="imgGrabar" runat="server" Height="30px" Width="35px" 
                    ImageUrl="~/imagenes/BotonGrabar.png" />
			<asp:Label id="Label4" runat="server" Font-Size="11px" Text="Grabar"></asp:Label>
			</div>
			</td>
		</tr>
	</table>
	</fieldset>
</td>
</tr>
</table>

