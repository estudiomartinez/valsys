﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BusquedaBolsa.ascx.vb" Inherits="Controles.BusquedaBolsa" %>
<%@ Register src="~/Controles/CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc1" %>
<%@ Register src="~/Controles/CtCombo.ascx" tagname="CtCombo" tagprefix="uc2" %>
<%@ Register src="~/Controles/CtlTxt.ascx" tagname="CtlTxt" tagprefix="uc3" %>
<%@ Register src="~/Controles/Botones.ascx" tagname="Botones" tagprefix="uc4" %>

<table width="100%" cellpadding="0" cellspacing="0" id="TablaPrincipal" runat="server">
<tr>
    <td class="titulo">
        <center>
            <asp:Label ID="lblTituloControl" runat="server" Text="" ForeColor="White" Font-Bold="true" Font-Size="16px"></asp:Label>
        </center>
    </td>
</tr>
<tr>
    <td>
        <fieldset>
	    <legend><asp:Label id="lblTituloGrupo" runat="server" Font-Size="11px" Text="FILTRAR POR"></asp:Label></legend>
	    <table style="width: 100%" cellpadding="0" cellspacing="0">
		<tr>
			<td><asp:Label ID="lblCartera" runat="server" Text="CARTERA" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" /></td>
			<td><asp:Label ID="lblGestor" runat="server" Text="GESTOR" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" /></td>
			<td><asp:Label ID="lblEstado" runat="server" Text="ESTADO" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" /></td>			
			<td><asp:Label ID="lblActiva" runat="server" Text="ACTIVA BOLSA" ForeColor="Maroon" Font-Size="10px" Font-Bold="true" /></td>			
			<td style="text-align:center" rowspan="2">
			    <div class="curvo">
			        <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Imagenes/BotonBusquedaPequena.png" ToolTip="Buscar" Width="35px" Height="30px"  />
			        <asp:Label ID="lblBuscar" runat="server" text="Buscar" />			        
			    </div>
            </td>			    
			<td style="text-align:center" rowspan="2">
			    <div class="curvo">
			        <asp:ImageButton ID="BtnEliminar" runat="server" ImageUrl="~/Imagenes/BotonEliminar.png" ToolTip="Buscar" Width="35px" Height="30px"  />
			        <asp:Label ID="lblEliminar" runat="server" text="Eliminar" />
			    </div>
			</td>			
		</tr>
		<tr>
			<td><uc2:CtCombo ID="cboCartera" runat="server" Longitud="200" AutoPostBack="true" Procedimiento="QRYCG002" /></td>
			<td><uc2:CtCombo ID="cboGestor" runat="server" Longitud="300"  AutoPostBack="true" /></td>
			<td><uc2:CtCombo ID="cboEstado" runat="server" Longitud="200"  AutoPostBack="false" Procedimiento="AGE002"/></td>
			<td><asp:CheckBox ID="chkActivaBolsa" runat="server" AutoPostBack="true"/></td>			
			<td>
			</td>
			<td>
			</td>
		</tr>
	</table>
	</fieldset>  
    </td>
</tr>
<tr>
    <td>       
    </td>   
</tr>
<tr>
    <td>
        <fieldset>ac
        <uc1:CtlGrilla ID="gvPrincipal" runat="server" Desactiva_Boton="false" Activa_option="false" largo="350px" Ancho="970px" Visualizar_Img="true" Visualizar_ChkBox="false" Activa_Edita="false" Activa_Delete="true" Activa_ckeck="false"/>
        </fieldset>
    </td>
</tr>    
<tr>
    <td style="text-align:center">
        
    </td>
</tr>
</table>
