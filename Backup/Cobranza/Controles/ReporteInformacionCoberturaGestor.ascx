﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ReporteInformacionCoberturaGestor.ascx.vb" Inherits="Controles.ReporteInformacionCoberturaGestor" %>
<%@ Register src="CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc1" %>
<table style="width: 100%;" align="center">
<tr align="center">
<td align="center" style="text-align:center;" class="titulo">
    REPORTE DE INFORMACION DE COBERTURA CARTERA X GESTOR
</td>
</tr>
</table>


<table style="width: 100%">
		<tr>
			<td>
			<fieldset>
			<legend>FILTRAR POR</legend>
			<table style="width: 100%">
				<tr>
					<td>
			<asp:Label id="Label2" runat="server" Font-Size="11px" Text="CARTERA"></asp:Label>
					<br />
			<asp:DropDownList id="cboCartera" runat="server" Font-Size="10px">
				<asp:ListItem Value="0">Seleccione</asp:ListItem>
			</asp:DropDownList>
					</td>
					<td>
			<asp:Label id="Label3" runat="server" Font-Size="11px" Text="GESTOR"></asp:Label>
					<br />
			<asp:DropDownList id="cboGestor" runat="server" Font-Size="10px">
				<asp:ListItem Value="0">Seleccione</asp:ListItem>
			</asp:DropDownList>
					</td>
					<td>
			<asp:Label id="Label4" runat="server" Font-Size="11px" Text="FECHA INICIO"></asp:Label>
					<br />
			<asp:TextBox ID="txtFechaInicio" runat="server"  Width="100px" Enabled = "false" BackColor="White" BorderColor="Black" BorderWidth = "1" Font-Size="11px" />
                            <img id="fecha1" alt="calendario" height="24" valign="middle"
                            onclick="return showCalendar('fecha1','<%=txtFechaInicio.ClientID%>','%d/%m/%Y','24', true);" 
                            src="~/Imagenes/calendario.png" width="24" />							
					</td>
					<td>
			<asp:Label id="Label22" runat="server" Font-Size="11px" Text="FECHA FIN"></asp:Label>
					<br />
			<asp:TextBox ID="txtFechaFin" runat="server"  Width="100px" Enabled = "false" BackColor="White" BorderColor="Black" BorderWidth = "1" Font-Size="11px" />
                            <img id="fecha2" alt="calendario" height="24" valign="middle"
                            onclick="return showCalendar('fecha2','<%=txtFechaFin.ClientID%>','%d/%m/%Y','24', true);" 
                            src="~/Imagenes/calendario.png" width="24" />							
					</td>
					<td align="center" style="text-align:center;">
					<div class="curvo">
			<asp:ImageButton id="imgGeneraReporte" runat="server" Height="30px" 
                            ImageUrl="~/imagenes/BotonGenerarReporte.jpg" Width="35px" />
			<asp:Label id="Label19" runat="server" Font-Size="11px" Text="Reporte"></asp:Label>
					</div>
					</td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset>
			<legend>REGISTROS DE COBERTURA X GESTOR ENCONTRADOS</legend>
				<uc1:CtlGrilla ID="CtlGrilla1" runat="server" Largo="300px" Ancho="500" Activa_ckeck="false" Activa_option="false" Desactiva_Boton="false"/>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
				<asp:GridView id="GridView2" runat="server">
				</asp:GridView>
			</td>
		</tr>
		<tr align="center">
			<td align="center" style="text-align:center;">
			<fieldset>
			<div class="curvo">
			<asp:ImageButton id="imgExportarExcel" runat="server" Height="30px" Width="35px" 
                    ImageUrl="~/imagenes/BotonExportarExcel.jpg" />
			<asp:Label id="Label20" runat="server" Font-Size="11px" Text="Exportar"></asp:Label>
			</div>
			</fieldset>
			</td>
		</tr>
	</table>