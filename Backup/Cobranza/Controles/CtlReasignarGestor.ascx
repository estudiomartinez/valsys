﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlReasignarGestor.ascx.vb" Inherits="Controles.CtlReasignarGestor" %>

<%@ Register src="CtlTxt.ascx" tagname="CtlTxt" tagprefix="uc1" %>
<%@ Register src="CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc5"%>
<%@ Register src="CtCombo.ascx" tagname="CtCombo" tagprefix="uc2" %>

<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc3" %>

<table cellpadding="0" cellspacing="0" class="fondoPantalla">
<tr>
    <td colspan="3" class="titulo">
        <center>
            <uc3:CtlMensajes ID="CtlMensajes1" runat="server" />
            <asp:Label ID="lblIdCliente" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblIdUsuario" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblIdUsuarioCliente" runat="server" Visible="false"></asp:Label>            
            
            <asp:Label ID="lblTituloControl" runat="server" Text="REASIGNAR GESTORES" ForeColor="White" Font-Bold="true" Font-Size="16px"></asp:Label>
        </center>
    </td>
</tr>
<tr>
    <td><asp:Label ID="lblClienteDNI" runat="server" text="DNI CLIENTE: " ForeColor="White" /></td>
    <td><uc1:CtlTxt ID="txtCienteDNI" runat="server"/></td>
    <td>
        <div class="curvo">
	        <asp:ImageButton id="btnBuscar" runat="server" Height="30px" Width="30px" ToolTip="Buscar" ImageUrl="~/Imagenes/BotonBusquedaPequena.png"/>
            <asp:Label id="lblBuscar" runat="server" Font-Size="11px" Text="Buscar"></asp:Label>
        </div>
    </td>
</tr>
<tr>
    <td colspan="3">
        <fieldset>
        <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="4">Clientes Encontrados</td>
        </tr>
        <tr>
            <td colspan="4"><uc5:CtlGrilla ID="CtlClenteAsignado" runat="server" Activa_ckeck="false" Activa_option="false" Desactiva_Boton="false" Activa_Titulo="false" Largo="165px" Ancho="800"  With_Grilla="800px" Activa_Delete="false" Desactivar_Exportar="0;1;2;3;4" Activa_Edita="true" OpocionNuevo="true"/></td>
        </tr>        
        </table>
        </fieldset>
    </td>
</tr>
<tr>
    <td colspan="3">
    <center>
        <asp:Label ID="lblCambiarGestor" runat="server" text="CAMBIAR A GESTOR: " ForeColor="White" />&nbsp;&nbsp;&nbsp;&nbsp;
        <uc2:CtCombo ID="cboGestor" runat="server" Procedimiento="QRYCBG002" Condicion="" Longitud="100" />
    </center>
    </td>
</tr>
<tr>
    <td colspan="3">
    <table width="100%">
        <tr>
            <td></td>
            <td  style="text-align:right;" width="35px">
				<div class="curvo">
	            <asp:ImageButton id="btnGrabar" runat="server" Height="30px" Width="30px" ToolTip="Grabar" 
	            ImageUrl="~/Imagenes/BotonGrabar.png"/>
                <asp:Label id="lblGrabar" runat="server" Font-Size="11px" Text="Grabar"></asp:Label>
                </div>															 					
			</td>   
            <td  style="text-align:right;" width="35px">
				<div class="curvo">
	            <asp:ImageButton id="BtnCerrar" runat="server" Height="30px" Width="30px" ToolTip="Cerrar"
	            ImageUrl="~/Imagenes/BotonCerrar.jpg"/>
                <asp:Label id="Label1" runat="server" Font-Size="11px" Text="Cerrar"></asp:Label>
                </div>															 					
			</td>
        </tr>
        </table>
    </td>
</tr>
</table>
