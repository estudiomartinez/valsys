﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CalculoIndicadoresCobertura.ascx.vb" Inherits="Controles.CalculoIndicadoresCobertura" %>
<%@ Register src="CtCombo.ascx" tagname="CtCombo" tagprefix="uc3" %>
<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc1" %>
<%@ Register src="GraficoIndicadores.ascx" tagname="GraficoIndicadores" tagprefix="uc2" %>
<table style="width: 100%;" align="center">
<tr align="center">
<td align="center" style="text-align:center;" class="titulo">
    CALCULO DE INDICADORES POR CARTERA Y GESTOR
    <uc1:CtlMensajes ID="CtlMensajes1" runat="server" />
</td>
</tr>
</table>


<div style="width:100%;">
<table style="width: 100%" cellpadding="0" cellspacing="0">
		<tr>
			<td>
			<fieldset>
			<legend>
			    FILTRAR POR
			</legend>
			<table style="width: 100%" cellpadding="0" cellspacing="0">
				<tr align="center">
					<td>
			        <asp:Label id="Label9" runat="server" Font-Size="11px" Text="CARTERA"></asp:Label>
					<br />
			            <uc3:CtCombo ID="cboCartera" runat="server" Activa="true" 
                            Procedimiento="QRYC007" Condicion="" AutoPostBack="true" />
					</td>
					<td>
			        <asp:Label id="Label10" runat="server" Font-Size="11px" Text="GESTOR"></asp:Label>
					<br />
			            <uc3:CtCombo ID="cboGestor" runat="server" Activa="true" 
                            Condicion=""  Longitud="80"/>
					</td>
					<td>
			            FECHA INICIO
					    <br />
			            <asp:TextBox ID="txtFechaInicio" runat="server"  Width="80px" Enabled = "false" BackColor="White"/>
                        <img ID="txtFechaInicio" alt="calendario" height="16"  onclick="return showCalendar('txtFechaInicio','<%=txtFechaInicio.ClientID%>','%d/%m/%Y','24', true);" 
                        src="Imagenes/calendario.png" width="18" />							
					</td>
					<td>
			            FECHA FIN
					    <br />
			            <asp:TextBox ID="txtFechaFin" runat="server"  Width="80px" Enabled = "false" BackColor="White"/>
                        <img ID="txtFechaFin" alt="calendario" height="16" onclick="return showCalendar('txtFechaFin','<%=txtFechaFin.ClientID%>','%d/%m/%Y','24', true);" 
                        src="Imagenes/calendario.png" width="18" />
					</td>
					<td>
			        <asp:Label id="Label13" runat="server" Font-Size="11px" Text="T. CAMBIO"></asp:Label>
					<br />
					<asp:TextBox id="txtCambio" runat="server" Font-Size="11px"></asp:TextBox>
					</td>
					<td style="text-align:center;" align="center">
					<div class="curvo">
			            <asp:ImageButton id="imgCalcularIndicadores" runat="server" Height="30px" Width="35px" 
                        ImageUrl="~/Imagenes/Aceptar.png" />
					    <asp:Label id="Label14" runat="server" Font-Size="11px" Text="Calculo"></asp:Label>
				    </div>
					</td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset>
			<legend>
			    RESULTADOS
			</legend>
			<table style="width: 100%;" cellpadding="0" cellspacing="0">
				<tr>
					<td>
			        <asp:Label id="Label15" runat="server" Font-Size="11px" Text="Clientes Asignados"></asp:Label>
					</td>
					<td>
					<asp:TextBox Enabled="false" id="txtClientesAsignados" runat="server" Font-Size="11px"></asp:TextBox>
					</td>
					<td>
			        <asp:Label id="Label18" runat="server" Font-Size="11px" Text="Capital (S + D)"></asp:Label>
					</td>
					<td>
			        <asp:Label id="Label38" runat="server" Font-Size="11px" Text="S./"></asp:Label>
					</td>
					<td>
					<asp:TextBox Enabled="false" id="txtCapital" runat="server" Font-Size="11px"></asp:TextBox>
					</td>
					<td>
					<table style="width: 100%">
						<tr>
							<td align="right">
			                <asp:Label id="Label21" runat="server" Font-Size="11px" Text="Pagos (S + D)"></asp:Label>
							</td>
							<td>
			                <asp:Label id="Label41" runat="server" Font-Size="11px" Text="S./"></asp:Label>
							</td>
							<td>
							<asp:TextBox Enabled="false" id="txtPagos" runat="server" Font-Size="11px"></asp:TextBox>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td>
			        <asp:Label id="Label16" runat="server" Font-Size="11px" Text="Cuentas Asignadas"></asp:Label>
					</td>
					<td>
					<asp:TextBox Enabled="false" id="txtCuentasAsignadas" runat="server" Font-Size="11px"></asp:TextBox>
					</td>
					<td>
			        <asp:Label id="Label19" runat="server" Font-Size="11px" Text="Deuda Total (S + D)"></asp:Label>
					</td>
					<td>
			        <asp:Label id="Label39" runat="server" Font-Size="11px" Text="S./"></asp:Label>
					</td>
					<td>
					<asp:TextBox Enabled="false" id="txtTotal" runat="server" Font-Size="11px"></asp:TextBox>
					</td>
					<td>
					<table style="width: 100%">
						<tr>
							<td>
			                <asp:Label id="Label22" runat="server" Font-Size="11px" Text="Cant PP"></asp:Label>
							</td>
							<td>
							<asp:TextBox Enabled="false" id="txtCantPP" runat="server" Font-Size="11px"></asp:TextBox>
							</td>
							<td>
			                <asp:Label id="Label23" runat="server" Font-Size="11px" Text="Cant PC"></asp:Label>
							</td>
							<td>
							<asp:TextBox Enabled="false" id="txtCantPC" runat="server" Font-Size="11px"></asp:TextBox>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td>
			        <asp:Label id="Label17" runat="server" Font-Size="11px" Text="Cuentas Gestionadas"></asp:Label>
					</td>
					<td>
					<asp:TextBox Enabled="false" id="txtCuentasGestionadas" runat="server" Font-Size="11px"></asp:TextBox>
					</td>
					<td>
			        <asp:Label id="Label20" runat="server" Font-Size="11px" Text="Deuda Vencida (S + D)"></asp:Label>
					</td>
					<td>
			        <asp:Label id="Label40" runat="server" Font-Size="11px" Text="S./"></asp:Label>
					</td>
					<td>
					<asp:TextBox Enabled="false" id="txtDeudaVencida" runat="server" Font-Size="11px"></asp:TextBox>
					</td>
					<td>
					<table style="width: 100%">
						<tr>
							<td>
			                <asp:Label id="Label24" runat="server" Font-Size="11px" Text="G. Hum."></asp:Label>
							<br />
							<asp:TextBox Enabled="false" id="txtGHum" runat="server" Font-Size="11px"></asp:TextBox>
							</td>
							<td>
			                <asp:Label id="Label25" runat="server" Font-Size="11px" Text="G. Dir."></asp:Label>
							<br />
							<asp:TextBox Enabled="false" id="txtGDir" runat="server" Font-Size="11px"></asp:TextBox>
							</td>
							<td>
			                <asp:Label id="Label26" runat="server" Font-Size="11px" Text="G. Tit."></asp:Label>
							<br />
							<asp:TextBox Enabled="false" id="txtGTit" runat="server" Font-Size="11px"></asp:TextBox>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset>
			<legend>
			    INDICADORES
			</legend>
			<table style="width: 100%" cellpadding="0" cellspacing="0">
				<tr align="center">
					<td>
			        <asp:Label id="Label27" runat="server" Font-Size="11px" Text="Efectividad"></asp:Label>
					<br />
					<asp:TextBox Enabled="false" id="txtEfectividad" runat="server" Font-Size="11px" Width="50px"></asp:TextBox>
					</td>
					<td>
			        <asp:Label id="Label28" runat="server" Font-Size="11px" Text="Cobertura"></asp:Label>
					<br />
					<asp:TextBox Enabled="false" id="txtCobertura" runat="server" Font-Size="11px" Width="50px"></asp:TextBox>
					</td>
					<td>
			        <asp:Label id="Label29" runat="server" Font-Size="11px" Text="Cont. Hum."></asp:Label>
					<br />
					<asp:TextBox Enabled="false" id="txtContHum" runat="server" Font-Size="11px" Width="50px"></asp:TextBox>
					</td>
					<td>
			        <asp:Label id="Label30" runat="server" Font-Size="11px" Text="Cont. Direc."></asp:Label>
					<br />
					<asp:TextBox Enabled="false" id="txtContDirect" runat="server" Font-Size="11px" Width="50px"></asp:TextBox>
					</td>
					<td>
			        <asp:Label id="Label31" runat="server" Font-Size="11px" Text="Intens. Gest."></asp:Label>
					<br />
					<asp:TextBox Enabled="false" id="txtIntensGest" runat="server" Font-Size="11px" Width="50px"></asp:TextBox>
					</td>
					<td>
			        <asp:Label id="Label32" runat="server" Font-Size="11px" Text="Intens. Contac."></asp:Label>
					<br />
					<asp:TextBox Enabled="false" id="txtIntensContac" runat="server" Font-Size="11px" Width="50px"></asp:TextBox>
					</td>
					<td>
			        <asp:Label id="Label33" runat="server" Font-Size="11px" Text="Cierre PDP'S"></asp:Label>
					<br />
					<asp:TextBox Enabled="false" id="txtCierrePDPS" runat="server" Font-Size="11px" Width="50px"></asp:TextBox>
					</td>
					<td>
			        <asp:Label id="Label34" runat="server" Font-Size="11px" Text="Efect PDP'S"></asp:Label>
					<br />
					<asp:TextBox Enabled="false" id="txtEfectPDPS" runat="server" Font-Size="11px" Width="50px"></asp:TextBox>
					</td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td style="text-align:center;">
			<table style="width: 100%" cellpadding="0" cellspacing="0">
				<tr align="center">
					<td style="text-align:center;" align="center">
					<div class="curvo">
			        <asp:ImageButton id="imgVerGrafico" runat="server" Height="30px" Width="35px" 
                            ImageUrl="~/Imagenes/BotonVerEstadisticas.jpg" />
			        <asp:Label id="Label36" runat="server" Font-Size="11px" Text="Graficos"></asp:Label>
					</div>
					<br />
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</div>

<table>
	<tr>
	<td>
	<asp:Panel ID="pnlGrafico" runat="server" Visible="false">
        <div style="position:absolute; left:100px; top:100px;" class="fondo1">
        <table>
        <tr>
        <td>
            <uc2:GraficoIndicadores ID="GraficoIndicadores1" runat="server" />    
        </td>
        </tr>
        </table>
        </div>
    </asp:Panel>
	</td>
	</tr>
</table>



