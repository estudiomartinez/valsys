﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.Master" CodeBehind="Asignacion.aspx.vb" Inherits="Cobranza.Formulario_web124" 
    title="Asignacion" %>
<%@ Register src="Controles/CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc1" %>
<%@ Register src="Controles/CtCombo.ascx" tagname="CtCombo" tagprefix="uc2" %>
<%@ Register src="Controles/CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Contenido" runat="server">
<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
<ProgressTemplate>
    <div class="modal">
        <div class="center">
            <img alt="" src="Imagenes/loader.gif" />
        </div>
    </div>
</ProgressTemplate>
</asp:UpdateProgress>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<asp:Label ID="lblSQL_Activo" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lblTipo" runat="server" Visible="false"></asp:Label>
<table  style="width:100%; text-align:center;">
<tr>
<td style="width:100%;text-align:center;" class="fondoPantalla">
    <table style="width: 100%;" align="center">
        <tr>
            <td align="center" class="titulo">
                ASIGNACION
                <uc3:CtlMensajes ID="CtlMensajes1" runat="server" />
            </td>
        </tr>
    </table>
    <table style="width: 100%;" align="center">
        <tr>
            <td>   
                <fieldset>
                EMPRESA:
                <uc2:CtCombo ID="cboEmpresa" runat="server" AutoPostBack="true" Procedimiento="QRYMG002" />                
                CARTERA:
                <uc2:CtCombo ID="cboCartera" runat="server" AutoPostBack="true" Procedimiento="QRYC014" Longitud="150"/>                
                </fieldset>             
            </td>
        </tr>
        <tr>
            <td>   
                <fieldset>
                    <uc1:CtlGrilla ID="gvTablaGenera" runat="server" Activa_ckeck="false" Activa_Delete="false" Activa_Edita="false" Activa_Export="false" Activa_option="false" Desactiva_Boton="false" OpocionNuevo="false" Largo="80px" />                
                </fieldset>            
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                <table>
                    <tr>
                        <td>
                            RANGO:
                            INICIO<asp:TextBox ID="txtRangoInicio" runat="server"></asp:TextBox>
                            FIN<asp:TextBox ID="txtRangoFin" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            GESTOR:
                            <uc2:CtCombo ID="cboGestor" runat="server" Procedimiento="SQL_N_GEST052" Longitud="150"/>                
                        </td>
                        <td>
                            TIP0:
                            <uc2:CtCombo ID="cboTipo" runat="server" Procedimiento="SQL_N_GEST006" Longitud="150"/>                
                        </td>
                        <td>
                            DNI:
                            <asp:TextBox ID="txtDNI" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblMensaje" runat="server" Text="Dejar getor y tipo en blanco para utilizar los clientes que no fueron asignados" ForeColor="Yellow"></asp:Label>
                        </td>
                    </tr>                
                </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    GESTOR ASIGANDO:
                    <uc2:CtCombo ID="cboGestorAsigando" runat="server" Procedimiento="SQL_N_GEST057" Longitud="150"/>      
                    <asp:Button ID="btnAplicar" Text="Aplicar" runat="server" />
                    <asp:Button ID="btnAceptar" Text="Aceptar" runat="server" />
                    <asp:Button ID="btnCancelar" Text="Cancelar" runat="server" />          
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <uc1:CtlGrilla ID="gvTemporal" runat="server" Activa_ckeck="false" Activa_Delete="false" Activa_Edita="false" Activa_Export="false" Activa_option="false" Desactiva_Boton="false" OpocionNuevo="false" Largo="400px"/>                
                </fieldset>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>



</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
