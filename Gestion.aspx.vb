﻿Imports Controles
Imports System.Threading
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Web.Services.Protocols
Imports System.ComponentModel
'Imports Microsoft.Office.Interop.Excel
'Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Net
Imports System.Drawing.Color

Imports iTextSharp
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser

Imports System.IO

Partial Public Class Gestion
    Inherits System.Web.UI.Page
    Dim idtelefono As Integer
    Public IPAsterisk As String
    Dim InicioPredictivo As String = ""
    Public DNI As String = ""
    Dim idPago As Integer
    Dim conexion As New ConDB.ConSQL
    Dim cnxn As New ConDB.Conexion
    Public Efectividad As Integer
    Public Cobertura As Integer
    Public CodGestor As String
    Public Pagos As String
    Public Perfil As String
    Dim TipoDatos As String
    Public Evento As String
    Public IpLlamada As String
    Public StrMensaje As String
    Public ppDNI As String
    Public ppidcliente As Integer
    Public ppidcartera As Integer
    Public ppidagenda As Integer
    'Public idGestion_PLM As Integer = 0
    Dim DataTableGestion As New DataTable

    '''''''''''''''''''''''''''''''VARIABLES PUBLICAS PARA AFP
    ''Dim v_Publica_TotalDevengues As Integer = 0
    ''Dim v_Publica_MinimoPagoDevengues As Integer = 0
    ''Dim v_Publica_TotalCompromisosDevengues As Integer = 0
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Private miPaginas As Integer = 0

    Sub Carga_gestion_pred()
        Try
            If Not Me.IsPostBack Then
                Dim dt As DataTable
                Crear_Cookies("Estado", "")
                Crear_Cookies("Bolsa", Request.QueryString("Bolsa"))
                Hidcliente.Value = Request.QueryString("idcliente")
                idcliente = Hidcliente.Value
                If InStr(Request.QueryString("usuario"), "-") > 0 Then
                    Husuario.Value = Mid(Request.QueryString("usuario"), 1, InStr(Request.QueryString("usuario"), "-") - 1).Trim
                Else
                    Husuario.Value = Request.QueryString("usuario")
                End If
                Crear_Cookies("idcliente", Hidcliente.Value)
                If Not Me.IsPostBack Then
                    Dim ctl As New Controles.General
                    dt = ctl.FncCliente(idcliente)
                    If Not dt Is Nothing Then
                        idEmpresa = dt(0)("idempresa")
                        idUsuario = dt(0)("idusuarioa")
                        cbocartera.Text = dt(0)("NomCartera")
                        txtnombre.Text = dt(0)("NombreCliente")
                        txtsitua.Text = dt(0)("Situacion")

                        Dim dtusu = ctl.Obtener_usuario(0, Husuario.Value, 0)
                        If Not dtusu Is Nothing Then
                            If dtusu.Rows.Count > 0 Then
                                lblGestor.Text = "USUARIO CONECTADO: " & dtusu(0)("Usuario") & " - " & dtusu(0)("Apellidos") & " " & dtusu(0)("Nombres")
                                Hidusuario.Value = dtusu(0)("idusuario")
                                lblAnexo.Text = "ANEXO: " & dtusu(0)("anexo")
                                Crear_Cookies("Anexo", dtusu(0)("anexo"))
                            End If
                        End If
                        dtusu = Nothing

                        Dim Anexo = Val(Obtiene_Cookies("Anexo"))
                        lblUuario.Text = "GESTOR ASIGNADO: " & dt(0)("Usuario") & " - " & dt(0)("NomUsu")
                        TipoDatos = ""
                        TipoDatos = Obtiene_Cookies("TipoTroncal")
                        cbocint.Value = dt(0)("idcondicion")
                        txtDNI.Text = dt(0)("DNI")
                        Dni_Activo = dt(0)("DNI")
                        DNI = dt(0)("DNI")
                        TipoCartera = dt(0)("TipoCartera")
                        idCartera = dt(0)("idcartera")
                        cbocartera.Limpia()
                        cboCarteraPagos.Limpia()
                        cbocartera.Condicion = ":criterio▓" & dt(0)("TipoCartera")
                        cbocartera.Cargar_dll()
                        cboCarteraPagos.Condicion = ":criterio▓" & dt(0)("TipoCartera")
                        cboCarteraPagos.Cargar_dll()
                        cboIndicador.Condicion = ":criterio▓" & ""
                        cbocartera.Value = dt(0)("idcartera")
                        cboCarteraPagos.Value = dt(0)("idcartera")

                        '----
                        cbotipoc.Limpia()
                        If cbocint.Text = "UT" Or cbocint.Text = "PP" Or cbocint.Text = "LL" Or cbocint.Text = "CO" Or cbocint.Text = "PI" Then
                            cbotipoc.Procedimiento = "SQL_N_GEST006"
                            cbotipoc.Condicion = ":idtabla▓124"
                            cbotipoc.Cargar_dll()
                            cbotipoc.Text = dt(0)("TipoContacto")
                            cbotipoc.Visible = True
                        Else
                            cbotipoc.Visible = False
                        End If
                        '----
                        ctl.ingresa_Telefono(idcliente, Dni_Activo)
                        Dim dta As DataTable = ctl.Obtiene_Datos_adicionales(DNI, idEmpresa)
                        'If Not dta Is Nothing Then
                        '    If dta.Rows.Count > 0 Then
                        '        txtCentroLaboral.Text = IIf(IsDBNull(dta(0)("centrolaboral")), "", dta(0)("centrolaboral"))
                        '        txtemail.Text = IIf(IsDBNull(dta(0)("email")), "", dta(0)("email"))
                        '        'Retirado segun lo acordado en la reunion que tuvo lugar el 14 Febrero con los Señores Renzo, Charles, Carlos, Cesar y Yo
                        '        'txt1erProd.Text = IIf(IsDBNull(dta(0)("Conyugie")), "", dta(0)("Conyugie"))
                        '        txtEdad.Text = IIf(IsDBNull(dta(0)("Edad")), "", dta(0)("Edad"))
                        '        txtIngreso.Text = IIf(IsDBNull(dta(0)("Sueldo")), "", dta(0)("Sueldo"))

                        '        txtAvalPersona.Text = IIf(IsDBNull(dta(0)("aval")), "", dta(0)("aval"))
                        '        txtAvalEmpresa.Text = IIf(IsDBNull(dta(0)("aval")), "", dta(0)("aval"))

                        '        'se comenta por la dinamica de cuando es empresa y cuando es persona
                        '        'txtRepresentanteLegal.Text = IIf(IsDBNull(dta(0)("RepLegal")), "", dta(0)("RepLegal"))
                        '    End If
                        'End If

                        If Not dta Is Nothing Then
                            If dta.Rows.Count > 0 Then
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 1 Columna 1
                                txtCentroLaboral.Text = IIf(IsDBNull(dta(0)("centrolaboral")), "", dta(0)("centrolaboral"))
                                txtRepresentanteLegal.Text = IIf(IsDBNull(dta(0)("RepLegal")), "", dta(0)("RepLegal"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 1 Columna 2
                                txtEdad.Text = IIf(IsDBNull(dta(0)("Edad")), "", dta(0)("Edad"))
                                txtAvalEmpresa.Text = IIf(IsDBNull(dta(0)("aval")), "", dta(0)("aval"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 2 Columna 1
                                txtConyugue.Text = IIf(IsDBNull(dta(0)("Conyugie")), "", dta(0)("Conyugie"))
                                txtEncargado.Text = IIf(IsDBNull(dta(0)("Encargado")), "", dta(0)("Encargado"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 2 Columna 2
                                txtAvalPersona.Text = IIf(IsDBNull(dta(0)("aval")), "", dta(0)("aval"))
                                txtRelacionado.Text = IIf(IsDBNull(dta(0)("Relacionado")), "", dta(0)("Relacionado"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 3 Columna 1
                                txtEmail.Text = IIf(IsDBNull(dta(0)("email")), "", dta(0)("email"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 3 Columna 2
                                txtIngreso.Text = IIf(IsDBNull(dta(0)("Sueldo")), "", dta(0)("Sueldo"))
                                txtRCC.Text = IIf(IsDBNull(dta(0)("RCC")), "", dta(0)("RCC"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 4 Columna 1
                                txtRUC.Text = IIf(IsDBNull(dta(0)("RUC")), "", dta(0)("RUC"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 4 Columna 2
                                txtSunat.Text = IIf(IsDBNull(dta(0)("Sunat")), "", dta(0)("Sunat"))
                            End If
                        End If

                        dta = Nothing
                        '--- ingresando Direcciones 

                        Call ctl.Valida_Direccion(idcliente, "D", IIf(IsDBNull(dt(0)("Direccion")), "", dt(0)("direccion")), "LIMA", IIf(IsDBNull(dt(0)("NombreProvincia")), "", dt(0)("NombreProvincia")), IIf(IsDBNull(dt(0)("NombreDistrito")), "", dt(0)("NombreDistrito")), 1, "q", "R", TipoCartera)
                        Dim Telefono As Integer = Request.QueryString("Telefono")
                        '  Call ctl.Telefono_grilla(DNI, CtlTelefono)
                        CargarTelefonos_pred(Telefono, DNI)

                        Call ctl.Direccion_grilla(dt(0)(0), CtlDireccion)
                        Call ctl.Anotaciones_grilla(dt(0)(0), CtlAnotaciones)
                        Call ctl.Gestion_Campo_grilla(DNI, idEmpresa, CtlGestionCampo)
                        txtDeudaVcdaSoles.Text = 0
                        txtDeudaVcdaCUSD.Text = 0
                        txtDeudaTotalSoles.Text = 0
                        txtDeudaVcdaUSD.Text = 0
                        txtDeudaVcdaCSoles.Text = 0
                        txtDeudaTotalUSD.Text = 0
                        If TipoCartera = "ACTIVA" Then
                            dta = ctl.Deuda_Activa_grilla(dt(0)(0))
                            If Not dta Is Nothing Then
                                If dta.Rows.Count > 0 Then
                                    'Retirado segun lo acordado en la reunion que tuvo lugar el 14 Febrero con los Señores Renzo, Charles, Carlos, Cesar y Yo
                                    'txt1erProd.Text = IIf(IsDBNull(dta(0)("PRIMERPRODUCTO")), "", dta(0)("PRIMERPRODUCTO"))
                                    For i = 0 To dta.Rows.Count - 1
                                        If dta.Rows(i)("Moneda") = "S" Then
                                            txtDeudaVcdaSoles.Text = dta.Rows(i)("DeudaVencidaSol")
                                            txtDeudaVcdaCUSD.Text = dta.Rows(i)("DeudaVencidaDol")
                                            txtDeudaTotalSoles.Text = dta.Rows(i)("DEUDATOTAL")
                                        ElseIf dta.Rows(i)("Moneda") = "D" Then
                                            txtDeudaVcdaUSD.Text = dta.Rows(i)("DeudaVencidaDol")
                                            txtDeudaVcdaCSoles.Text = dta.Rows(i)("DeudaVencidaSol")
                                            txtDeudaTotalUSD.Text = dta.Rows(i)("DEUDATOTAL")
                                        End If
                                    Next
                                End If
                            End If

                        Else
                            dta = ctl.Deuda_Castigo_grilla(dt(0)(0))
                            For i = 0 To dta.Rows.Count - 1
                                If dta.Rows(i)("Moneda") = "S" Then
                                    txtDeudaVcdaSoles.Text = dta.Rows(i)("TotalK")
                                    'txtDeudaVcdaCUSD.Text = Format(txtDeudaVcdaSoles.Text / 3.1, "#,###,###.00")
                                    txtDeudaTotalSoles.Text = dta.Rows(i)("TotalD")
                                ElseIf dta.Rows(i)("Moneda") = "D" Then
                                    txtDeudaVcdaUSD.Text = dta.Rows(i)("TotalK")
                                    'txtDeudaVcdaCSoles.Text = Format(txtDeudaVcdaUSD.Text * 3.1, "#,###,###.00")
                                    txtDeudaTotalUSD.Text = dta.Rows(i)("TotalD")
                                End If
                            Next
                        End If
                        dta = Nothing
                        'Call ctl.Gestion_Telefono_grilla(DNI, idEmpresa, CtlGestionTelefonica)
                        Call ctl.Gestion_Telefono_grilla_Positivas(Dni_Activo, idEmpresa, CtlGestTelefPositivas)
                        Call ctl.Gestion_Telefono_grilla_Negativas(Dni_Activo, idEmpresa, CtlGestTelefNegativas)
                        dt = Nothing
                        txtNroOperacionCompromisoManteniemito.Limpia()
                        If TipoCartera = "ACTIVA" Then
                            txtNroOperacionCompromisoManteniemito.Procedimiento = "SQL_N_GEST034"
                        Else
                            txtNroOperacionCompromisoManteniemito.Procedimiento = "SQL_N_GEST033"
                        End If
                        txtNroOperacionCompromisoManteniemito.Condicion = ":idcliente▓" & idcliente
                        txtNroOperacionCompromisoManteniemito.Cargar_dll()
                    Else
                        If Not Hidusuario.Value <> "" Then
                            CtlMensajes1.Mensaje("El cliente no existe o el dato requiere que se reinicie el sistema, verifique y vuelve a intentar", "")
                        Else
                            CtlMensajes1.Mensaje("EL Cliente no esta asignado, Consulte con su administrador de cuenta y vuelve a intentar", "")
                        End If
                    End If
                Else
                    If Not Hidusuario.Value <> "" Then
                        CtlMensajes1.Mensaje("El cliente no existe o el dato requiere que se reinicie el sistema, verifique y vuelve a intentar", "")
                    Else
                        CtlMensajes1.Mensaje("EL Cliente no esta asignado, Consulte con su administrador de cuenta y vuelve a intentar", "")
                    End If
                End If
            End If
        Catch ex As Exception
            CtlMensajes1.Mensaje(ex.Message, "")
        End Try
    End Sub

    Sub CargarTelefonos_pred(ByVal Telefono As String, ByVal dni As String)
        Dim fnc As New BL.Cobranza
        Dim Dt As New DataTable
        Dim xSQL As String = ""
        Dt = fnc.FunctionGlobal(":ptelefonoƒ:pdni▓" & Telefono.Trim & "ƒ" & dni.Trim, "QRYTEL001")
        Hidtelefono.Value = Dt.Rows(0)("idtelefono")
        CtlTelefono.SourceDataTable = Dt
    End Sub

    Sub Carga_gestion()
        Try
            If Not Me.IsPostBack Then
                Dim dt As DataTable
                Crear_Cookies("Estado", "")
                Hidcliente.Value = Request.QueryString("idcliente")
                idcliente = Hidcliente.Value
                If InStr(Request.QueryString("usuario"), "-") > 0 Then
                    Husuario.Value = Mid(Request.QueryString("usuario"), 1, InStr(Request.QueryString("usuario"), "-") - 1).Trim
                Else
                    Husuario.Value = Request.QueryString("usuario")
                End If
                Crear_Cookies("idcliente", Hidcliente.Value)
                If Not Me.IsPostBack Then
                    Dim ctl As New Controles.General
                    dt = ctl.FncCliente(idcliente)
                    If Not dt Is Nothing Then
                        idEmpresa = dt(0)("idempresa")
                        idUsuario = dt(0)("idusuarioa")
                        cbocartera.Text = dt(0)("NomCartera")
                        txtnombre.Text = dt(0)("NombreCliente")
                        txtsitua.Text = dt(0)("Situacion")

                        Dim dtusu = ctl.Obtener_usuario(0, Husuario.Value, 0)
                        If Not dtusu Is Nothing Then
                            If dtusu.Rows.Count > 0 Then
                                lblGestor.Text = "USUARIO CONECTADO: " & dtusu(0)("Usuario") & " - " & dtusu(0)("Apellidos") & " " & dtusu(0)("Nombres")
                                Hidusuario.Value = dtusu(0)("idusuario")
                                lblAnexo.Text = "ANEXO: " & dtusu(0)("anexo")
                                Crear_Cookies("Anexo", dtusu(0)("anexo"))
                            End If
                        End If
                        dtusu = Nothing

                        Dim Anexo = Val(Obtiene_Cookies("Anexo"))
                        lblUuario.Text = "GESTOR ASIGNADO: " & dt(0)("Usuario") & " - " & dt(0)("NomUsu")
                        TipoDatos = ""
                        TipoDatos = Obtiene_Cookies("TipoTroncal")
                        cbocint.Value = dt(0)("idcondicion")
                        txtDNI.Text = dt(0)("DNI")
                        Dni_Activo = dt(0)("DNI")
                        DNI = dt(0)("DNI")
                        idCartera = dt(0)("idcartera")
                        TipoCartera = dt(0)("TipoCartera")
                        cbocartera.Limpia()
                        cbocartera.Condicion = ":criterio▓" & dt(0)("TipoCartera")
                        cbocartera.Cargar_dll()
                        cboCarteraPagos.Limpia()
                        cboCarteraPagos.Condicion = ":criterio▓" & dt(0)("TipoCartera")
                        cboCarteraPagos.Cargar_dll()
                        cboIndicador.Condicion = ":criterio▓" & ""
                        cbocartera.Value = dt(0)("idcartera")
                        cboCarteraPagos.Value = dt(0)("idcartera")
                        '----
                        cbotipoc.Limpia()
                        If cbocint.Text = "UT" Or cbocint.Text = "PP" Or cbocint.Text = "LL" Or cbocint.Text = "CO" Or cbocint.Text = "PI" Then
                            cbotipoc.Procedimiento = "SQL_N_GEST006"
                            cbotipoc.Condicion = ":idtabla▓124"
                            cbotipoc.Cargar_dll()
                            cbotipoc.Text = dt(0)("TipoContacto")
                            cbotipoc.Visible = True
                        Else
                            cbotipoc.Visible = False
                        End If
                        '----
                        ctl.ingresa_Telefono(idcliente, Dni_Activo)
                        Dim dta As DataTable = ctl.Obtiene_Datos_adicionales(DNI, idEmpresa)
                        If Not dta Is Nothing Then
                            If dta.Rows.Count > 0 Then

                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 1 Columna 1
                                txtCentroLaboral.Text = IIf(IsDBNull(dta(0)("centrolaboral")), "", dta(0)("centrolaboral"))
                                txtRepresentanteLegal.Text = IIf(IsDBNull(dta(0)("RepLegal")), "", dta(0)("RepLegal"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 1 Columna 2
                                txtEdad.Text = IIf(IsDBNull(dta(0)("Edad")), "", dta(0)("Edad"))
                                txtAvalEmpresa.Text = IIf(IsDBNull(dta(0)("aval")), "", dta(0)("aval"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 2 Columna 1
                                txtConyugue.Text = IIf(IsDBNull(dta(0)("Conyugie")), "", dta(0)("Conyugie"))
                                txtEncargado.Text = IIf(IsDBNull(dta(0)("Encargado")), "", dta(0)("Encargado"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 2 Columna 2
                                txtAvalPersona.Text = IIf(IsDBNull(dta(0)("aval")), "", dta(0)("aval"))
                                txtRelacionado.Text = IIf(IsDBNull(dta(0)("Relacionado")), "", dta(0)("Relacionado"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 3 Columna 1
                                txtEmail.Text = IIf(IsDBNull(dta(0)("email")), "", dta(0)("email"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 3 Columna 2
                                txtIngreso.Text = IIf(IsDBNull(dta(0)("Sueldo")), "", dta(0)("Sueldo"))
                                txtRCC.Text = IIf(IsDBNull(dta(0)("RCC")), "", dta(0)("RCC"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 4 Columna 1
                                txtRUC.Text = IIf(IsDBNull(dta(0)("RUC")), "", dta(0)("RUC"))
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' Fila 4 Columna 2
                                txtSunat.Text = IIf(IsDBNull(dta(0)("Sunat")), "", dta(0)("Sunat"))
                            End If
                        End If
                        dta = Nothing
                        '--- ingresando Direcciones 

                        Call ctl.Valida_Direccion(idcliente, "D", IIf(IsDBNull(dt(0)("Direccion")), "", dt(0)("direccion")), "LIMA", IIf(IsDBNull(dt(0)("NombreProvincia")), "", dt(0)("NombreProvincia")), IIf(IsDBNull(dt(0)("NombreDistrito")), "", dt(0)("NombreDistrito")), 1, "q", "R", TipoCartera)

                        '' ''*******************************************************************************************************************
                        '' ''ESTAS LINEAS SE RETIRARAN CUANDO SE TENGA LA APROBACION DE LA GERENCIA PARA HACER LAS BUSQUEDAS POR DNI SIN EL UNO
                        ' ''Call ctl.Telefono_grilla(DNI, CtlTelefono)
                        ''If telefono_activo <> "" Then
                        ''    CargarTelefonosP(telefono_activo, DNI)
                        ''Else
                        ''    CargarTelefonos(DNI)
                        ''End If
                        '' ''ESTAS LINEAS SE RETIRARAN CUANDO SE TENGA LA APROBACION DE LA GERENCIA PARA HACER LAS BUSQUEDAS POR DNI SIN EL UNO
                        '' ''*******************************************************************************************************************

                        'CAMBIOS SOLICITADOS POR GIOVANI - A LA ESPERA DE APROBACION DE LA GERENCIA
                        'FALTAN REVISIONES PARA VALIDAR SI EL CAMBIO APLICARÍA A PREDICTIVO O SOLO SERÁ AL PROGRESIVO
                        If telefono_activo <> "" Then
                            CargarTelefonosP(telefono_activo, DNI)
                        Else
                            CargarAllTelefonos_DNI_Unificado_Parte1(DNI)
                        End If
                        'CAMBIOS SOLICITADOS POR GIOVANI - A LA ESPERA DE APROBACION DE LA GERENCIA

                        Call ctl.Direccion_grilla(dt(0)(0), CtlDireccion)
                        Call ctl.Anotaciones_grilla(dt(0)(0), CtlAnotaciones)
                        Call ctl.Gestion_Campo_grilla(Dni_Activo, idEmpresa, CtlGestionCampo)
                        txtDeudaVcdaSoles.Text = 0
                        txtDeudaVcdaCUSD.Text = 0
                        txtDeudaTotalSoles.Text = 0
                        txtDeudaVcdaUSD.Text = 0
                        txtDeudaVcdaCSoles.Text = 0
                        txtDeudaTotalUSD.Text = 0

                        If TipoCartera = "ACTIVA" Then
                            dta = ctl.Deuda_Activa_grilla(dt(0)(0))
                            If Not dta Is Nothing Then
                                If dta.Rows.Count > 0 Then
                                    'Retirado segun lo acordado en la reunion que tuvo lugar el 14 Febrero con los Señores Renzo, Charles, Carlos, Cesar y Yo
                                    'txt1erProd.Text = IIf(IsDBNull(dta(0)("PRIMERPRODUCTO")), "", dta(0)("PRIMERPRODUCTO"))
                                    For i = 0 To dta.Rows.Count - 1
                                        If dta.Rows(i)("Moneda") = "S" Then
                                            txtDeudaVcdaSoles.Text = dta.Rows(i)("DeudaVencidaSol")
                                            txtDeudaVcdaCUSD.Text = dta.Rows(i)("DeudaVencidaDol")
                                            txtDeudaTotalSoles.Text = dta.Rows(i)("DEUDATOTAL")
                                        ElseIf dta.Rows(i)("Moneda") = "D" Then
                                            txtDeudaVcdaUSD.Text = dta.Rows(i)("DeudaVencidaDol")
                                            txtDeudaVcdaCSoles.Text = dta.Rows(i)("DeudaVencidaSol")
                                            txtDeudaTotalUSD.Text = dta.Rows(i)("DEUDATOTAL")
                                        End If
                                    Next
                                End If
                            End If
                        Else
                            dta = ctl.Deuda_Castigo_grilla(dt(0)(0))
                            For i = 0 To dta.Rows.Count - 1
                                If dta.Rows(i)("Moneda") = "S" Then
                                    txtDeudaVcdaSoles.Text = dta.Rows(i)("TotalK")
                                    'txtDeudaVcdaCUSD.Text = Format(txtDeudaVcdaSoles.Text / 3.1, "#,###,###.00")
                                    txtDeudaTotalSoles.Text = dta.Rows(i)("TotalD")
                                ElseIf dta.Rows(i)("Moneda") = "D" Then
                                    txtDeudaVcdaUSD.Text = dta.Rows(i)("TotalK")
                                    'txtDeudaVcdaCSoles.Text = Format(txtDeudaVcdaUSD.Text * 3.1, "#,###,###.00")
                                    txtDeudaTotalUSD.Text = dta.Rows(i)("TotalD")
                                End If
                            Next
                        End If

                        dta = Nothing
                        Call ctl.Gestion_Telefono_grilla_Positivas(Dni_Activo, idEmpresa, CtlGestTelefPositivas)
                        Call ctl.Gestion_Telefono_grilla_Negativas(Dni_Activo, idEmpresa, CtlGestTelefNegativas)
                        dt = Nothing
                        txtNroOperacionCompromisoManteniemito.Limpia()
                        If TipoCartera = "ACTIVA" Then
                            txtNroOperacionCompromisoManteniemito.Procedimiento = "SQL_N_GEST034"
                        Else
                            txtNroOperacionCompromisoManteniemito.Procedimiento = "SQL_N_GEST033"
                        End If
                        txtNroOperacionCompromisoManteniemito.Condicion = ":idcliente▓" & idcliente
                        txtNroOperacionCompromisoManteniemito.Cargar_dll()
                    Else
                        If Not Hidusuario.Value <> "" Then
                            CtlMensajes1.Mensaje("El cliente no existe o el dato requiere que se reinicie el sistema, verifique y vuelve a intentar", "")
                        Else
                            CtlMensajes1.Mensaje("EL Cliente no esta asignado, Consulte con su administrador de cuenta y vuelve a intentar", "")
                        End If
                    End If
                Else
                    If Not Hidusuario.Value <> "" Then
                        CtlMensajes1.Mensaje("El cliente no existe o el dato requiere que se reinicie el sistema, verifique y vuelve a intentar", "")
                    Else
                        CtlMensajes1.Mensaje("EL Cliente no esta asignado, Consulte con su administrador de cuenta y vuelve a intentar", "")
                    End If
                End If

                Dim ctlp1 As New Controles.General
                Dim dtp1 = ctlp1.Obtiene_Consulta(":idusuario▓" & Obtiene_Cookies("idusuario"), "BOL001")

                If dtp1.Rows.Count <> 0 Then
                    LLamar_Primer_Telefono(DNI)
                End If

            End If
        Catch ex As Exception
            CtlMensajes1.Mensaje(ex.Message, "")
        End Try
    End Sub

    Sub LLamar_Primer_Telefono(ByVal dni As String)

        Dim Anexo = Obtiene_Cookies("Anexo")
        TipoDatos = Obtiene_Cookies("TipoTroncal")
        Dim ctl As New Controles.General

        Dim fnc As New BL.Cobranza
        Dim Dt As New DataTable

        Dt = fnc.FunctionGlobal(":DNI▓" & dni, "QRYCGT005")

        Dim row As DataRow = Dt.Rows(Dt.Rows.Count - 1)

        Me.Page.ClientScript.RegisterStartupScript(Me.GetType(), "viewFolderDocuments", "LlamarTelefono('" & Anexo & "','" & IIf(TipoDatos = "salidas-ilimitadas", "*", "") & ctl.ValidaNumero(CStr(row("TELEFONO"))) & "','" & CStr(row("IDTELEFONO")) & "','imgButton');", True)

    End Sub

    Sub CargarAllTelefonos_DNI_Unificado_Parte1(ByVal dni_telefono)

        If Len(dni_telefono) < 10 Then
            Dim NewDNI As String = ""
            If Len(dni_telefono) = 9 Then
                NewDNI = dni_telefono.Substring(1, 8)
                CargarAllTelefonos_DNI_Unificado_Parte2(NewDNI)
            Else
                CargarAllTelefonos_DNI_Unificado_Parte2(dni_telefono)
            End If
        Else
            'Carga Telefonos de Empresas (Con RUC de 11 Digitos)
            CargarTelefonos(dni_telefono)
        End If

    End Sub

    Sub CargarAllTelefonos_DNI_Unificado_Parte2(ByVal dni)
        Dim fnc As New BL.Cobranza
        CtlTelefono.SourceDataTable = fnc.FunctionGlobal(":DNI▓" & dni, "QRYCGT006")
    End Sub

    Sub CargarTelefonos(ByVal dni_telefono)
        Dim fnc As New BL.Cobranza
        CtlTelefono.SourceDataTable = fnc.FunctionGlobal(":DNI▓" & dni_telefono, "QRYCGT001")
    End Sub

    Sub CargarTelefonosP(ByVal Telefono As String, ByVal dni As String)
        Dim fnc As New BL.Cobranza
        Dim Dt As New DataTable
        Dim xSQL As String = ""

        xSQL = "SELECT top 1  t.idcliente,tiptelf,viatelf,t.telefono,area,score,t.Estado,Habilitado,Prioridad,orden,reservado,observacion,fecha_actualizacion, " & _
                "t.DNI,Origen,case when Prioridad = 'R' then 4 when  Prioridad='þ' then 1 when  Prioridad='V' then 1 when Prioridad ='A' then 2 else 3 end ordenr,idtelefono " & _
                "FROM TELEFONOS T With(NoLock) WHERE T.telefono = '" & Telefono.Trim & "'  AND t.dni='" & dni & "'  AND t.ESTADO='A' order by ordenr"

        Dt = fnc.FunctionConsulta(xSQL)
        Hidtelefono.Value = Dt.Rows(0)("idtelefono")
        CtlTelefono.SourceDataTable = Dt
    End Sub

    Private Sub CtlTelefono_Boton_Click(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles CtlTelefono.Boton_Click
        Dim COL As Integer = 12
        Dim COL1 As Integer = 19
        If UCase(e.Row.Cells(COL).Text) = "PRIORIDAD" Then
            For c = 5 To e.Row.Cells.Count - 1
                e.Row.Cells(c).Text = UCase(e.Row.Cells(c).Text)
            Next
            e.Row.Cells(COL1).Text = " ACC "
            e.Row.Cells(COL).Text = " EST "
            e.Row.Cells(3).Text = "EL"
            e.Row.Cells(18).Text = "OBSERVACION"
            e.Row.Cells(7).Text = "TELEFONOS"
            e.Row.Cells(7).Width = "150"
            'e.Row.Cells(15).Width = "750"
            e.Row.Cells(18).Width = "750"
            e.Row.Cells(7).Height = "10"
            Exit Sub
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            'ShowToolTip  
            e.Row.Cells(18).ToolTip = e.Row.Cells(15).Text.Trim
        End If

        For c = 5 To e.Row.Cells.Count - 1
            If c = COL Then
                Select Case Trim(e.Row.Cells(c).Text)
                    Case "R"
                        e.Row.Cells(c).Text = "<center><img src='Imagenes/circle_red.png' alt='Smiley face' height='12px' width='10px'></center>"
                    Case "A"
                        e.Row.Cells(c).Text = "<center><img src='Imagenes/circle_yellow.png' alt='Smiley face' height='12px' width='10px'></center>"
                    Case "V"
                        e.Row.Cells(c).Text = "<center><img src='Imagenes/circle_green.png' alt='Smiley face' height='12px' width='10px'></center>"
                    Case "&#254;"
                        e.Row.Cells(c).Text = "<center><img src='Imagenes/circle_green.png' alt='Smiley face' height='12px' width='10px'></center>"
                    Case Else
                        e.Row.Cells(c).Text = "<center><img src='Imagenes/circle_grey.png' alt='Smiley face' height='12px' width='10px'></center>"
                End Select
            End If
            If c = COL1 Then
                Dim ctl As New Controles.General
                'Dim Anexo As
                Dim Anexo = Obtiene_Cookies("Anexo")
                TipoDatos = Obtiene_Cookies("TipoTroncal")
                e.Row.Cells(c).Text = "<center><img src='Imagenes/imgCall3.png' id='imgButton' alt='Smiley face' height='10px' width='10px' ondblclick=LlamarTelefono('" & Anexo & "','" & IIf(TipoDatos = "salidas-ilimitadas", "*", "") & ctl.ValidaNumero(e.Row.Cells(7).Text) & "','" & e.Row.Cells(20).Text & "','imgButton')></center>"
                ctl = Nothing
                '
            End If
            If e.Row.Cells(15).Text = "R" Then
                e.Row.Cells(c).Text = "<b>" & e.Row.Cells(c).Text
            End If
        Next
    End Sub

    Private Sub CtlDireccion_Boton_Click(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles CtlDireccion.Boton_Click
        If e.Row.Cells(6).Text = "Direccion" Then
            e.Row.Cells(6).Width = "400"
            e.Row.Cells(7).Width = "150"
            e.Row.Cells(12).Width = "150"
            e.Row.Cells(13).Width = "150"
            For c = 5 To e.Row.Cells.Count - 1
                e.Row.Cells(c).Text = UCase(e.Row.Cells(c).Text)
            Next
        End If
    End Sub

    Private Sub CtlAnotaciones_Boton_Click(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles CtlAnotaciones.Boton_Click
        If UCase(e.Row.Cells(4).Text) = "FECHA" Then
            e.Row.Cells(4).Width = "100"
            e.Row.Cells(6).Width = "575"
            For c = 5 To e.Row.Cells.Count - 1
                e.Row.Cells(c).Text = UCase(e.Row.Cells(c).Text)
            Next
        Else
            For c = 4 To e.Row.Cells.Count - 1
                e.Row.Cells(c).Style.Add("BORDER-BOTTOM", "#aaccee 1px solid")
                e.Row.Cells(c).Style.Add("BORDER-RIGHT", "#aaccee 1px solid")
                e.Row.Cells(c).Style.Add("padding-left", "1px")
                'e.Row.Cells(c).BackColor = Drawing.Color.FromName("#F9F496")
                e.Row.Cells(c).BackColor = System.Drawing.Color.FromName("#F9F496")
                'e.Row.Cells(c).ForeColor = Drawing.Color.Black
                e.Row.Cells(c).ForeColor = System.Drawing.Color.Black
            Next
        End If
    End Sub

    ' '' '' '' ''Private Sub CtlGestionTelefonica_Boton_Click(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles CtlGestionTelefonica.Boton_Click
    ' '' '' '' ''    Dim l As Integer = 3
    ' '' '' '' ''    If UCase(e.Row.Cells(l + 1).Text) = "FECHA" Then
    ' '' '' '' ''        e.Row.Cells(l + 1).Text = "FECHA"
    ' '' '' '' ''        e.Row.Cells(l + 2).Text = "HORA"
    ' '' '' '' ''        e.Row.Cells(l + 3).Text = "TIPO"
    ' '' '' '' ''        e.Row.Cells(l + 4).Text = "OPER."
    ' '' '' '' ''        e.Row.Cells(l + 5).Text = "IND"
    ' '' '' '' ''        e.Row.Cells(l + 6).Text = "TELEFONO"
    ' '' '' '' ''        e.Row.Cells(l + 7).Text = "DESCRIPCION DE LA GESTION"
    ' '' '' '' ''        'e.Row.Cells(l + 8).Text = "USUARIO"
    ' '' '' '' ''        e.Row.Cells(l + 14).Text = "GESTOR"

    ' '' '' '' ''        e.Row.Cells(l + 1).Width = "20"
    ' '' '' '' ''        e.Row.Cells(l + 2).Width = "40"
    ' '' '' '' ''        e.Row.Cells(l + 3).Width = "10"
    ' '' '' '' ''        e.Row.Cells(l + 4).Width = "10"
    ' '' '' '' ''        e.Row.Cells(l + 5).Width = "10"
    ' '' '' '' ''        e.Row.Cells(l + 7).Width = "500"
    ' '' '' '' ''        e.Row.Cells(l + 14).Width = "250"
    ' '' '' '' ''    Else
    ' '' '' '' ''        If Not e.Row.Cells(l + 1).Text = "&nbsp;" Then : e.Row.Cells(l + 1).Text = Format(CDate(e.Row.Cells(l + 1).Text), "dd/MM/yyyy") : End If
    ' '' '' '' ''        If Not e.Row.Cells(l + 2).Text = "&nbsp;" Then : e.Row.Cells(l + 2).Text = e.Row.Cells(l + 2).Text : End If
    ' '' '' '' ''    End If
    ' '' '' '' ''End Sub

    Private Sub RDBCall_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RDBCall.CheckedChanged
        Call carga_Call()
    End Sub

    Private Sub CboContactabilidad_Click() Handles CboContactabilidad.Click
        If RDBCall.Checked = True Then
            Carga_Indicadores_Call()
        ElseIf RDBCanalesAlternos.Checked = True Then
            Carga_Indicadores_CanalesAlternos()
        End If

    End Sub

    Private Sub cboIndicador_Click() Handles cboIndicador.Click
        If cboIndicador.Value = "634?-----?-?-?CEF" Or cboIndicador.Value = "635?-----?-?-?CEF" Then
            Carga_Indicadores_Call()
        End If
    End Sub

    Public Sub carga_Call()

        CboContactabilidad.Limpia()
        CboContactabilidad.Procedimiento = "SQL_N_GEST060"
        CboContactabilidad.Cargar_dll()
        'Selecciona_Contacto()

        'Carga_Indicadores()

        CboGestion.Limpia()
        CboGestion.Procedimiento = "SQL_N_GEST006"
        CboGestion.Condicion = ":idtabla▓118"
        CboGestion.Cargar_dll()
        Tipo_Telefono()
    End Sub

    Sub Carga_Indicadores_Call()
        cboIndicador.Limpia()
        If CboContactabilidad.Value = "1" Then 'CONTACTO
            cboIndicador.Procedimiento = "SQL_N_GEST061"
            cboIndicador.Condicion = ":tipoƒ:idEmpresaƒ:TipoContacto▓TELEFONIAƒ" & idEmpresa & "ƒ'CNE','CEF'"
            cboIndicador.Cargar_dll()
        ElseIf CboContactabilidad.Value = "2" Then 'NO CONTACTO
            cboIndicador.Procedimiento = "SQL_N_GEST061"
            cboIndicador.Condicion = ":tipoƒ:idEmpresaƒ:TipoContacto▓TELEFONIAƒ" & idEmpresa & "ƒ'NOC'"
            cboIndicador.Cargar_dll()
        Else 'AMBOS
            cboIndicador.Procedimiento = "SQL_N_GEST032"
            cboIndicador.Condicion = ":tipoƒ:idEmpresa▓TELEFONIAƒ" & idEmpresa
            cboIndicador.Cargar_dll()
        End If
    End Sub

    Sub Carga_Indicadores_CanalesAlternos()
        cboIndicador.Limpia()
        If CboContactabilidad.Value = "1" Then 'CONTACTO
            cboIndicador.Procedimiento = "SQL_N_GEST061"
            cboIndicador.Condicion = ":tipoƒ:idEmpresaƒ:TipoContacto▓CARTAƒ" & idEmpresa & "ƒ'CNE','CEF'"
            cboIndicador.Cargar_dll()
        ElseIf CboContactabilidad.Value = "2" Then 'NO CONTACTO
            cboIndicador.Procedimiento = "SQL_N_GEST061"
            cboIndicador.Condicion = ":tipoƒ:idEmpresaƒ:TipoContacto▓CARTAƒ" & idEmpresa & "ƒ'NOC'"
            cboIndicador.Cargar_dll()
        Else 'AMBOS
            cboIndicador.Procedimiento = "SQL_N_GEST032"
            cboIndicador.Condicion = ":tipoƒ:idEmpresa▓TELEFONIAƒ" & idEmpresa
            cboIndicador.Cargar_dll()
        End If
    End Sub

    Private Sub RDBCanalesAlternos_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RDBCanalesAlternos.CheckedChanged
        CargaCanalesAlternos()
    End Sub

    Sub CargaCanalesAlternos()
        'cboIndicador.Limpia()
        'cboIndicador.Procedimiento = "SQL_N_GEST032"
        'cboIndicador.Condicion = ":tipoƒ:idEmpresa▓CARTAƒ" & idEmpresa
        'cboIndicador.Cargar_dll()

        CboContactabilidad.Limpia()
        CboContactabilidad.Procedimiento = "SQL_N_GEST060"
        CboContactabilidad.Cargar_dll()

        CboGestion.Limpia()
        CboGestion.Procedimiento = "SQL_N_GEST006"
        CboGestion.Condicion = ":idtabla▓119"
        CboGestion.Cargar_dll()
    End Sub

    Public Property idcliente() As String
        Get
            Return lblId_Cliente.Text
        End Get
        Set(ByVal value As String)
            lblId_Cliente.Text = value
        End Set
    End Property

    Public Property idEmpresa() As String
        Get
            Return lblId_Empresa.Text
        End Get
        Set(ByVal value As String)
            lblId_Empresa.Text = value
        End Set
    End Property

    Public Property idUsuario() As String
        Get
            Return lblId_Usuario.Text
        End Get
        Set(ByVal value As String)
            lblId_Usuario.Text = value
        End Set
    End Property

    Public Property idCartera() As String
        Get
            Return lblId_Cartera.Text
        End Get
        Set(ByVal value As String)
            lblId_Cartera.Text = value
        End Set
    End Property

    Public Property TipoCartera() As String
        Get
            Return lblTipo_Cartera.Text
        End Get
        Set(ByVal value As String)
            lblTipo_Cartera.Text = value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ctl As New ConDB.ConSQL
        'ACA DEBE IR UN METODO QUE IDENTIFIQUE SI TODOS LOS TELEFONOS
        'QUE SE MOSTRARÀN DE DETERMINADO CLIENTE ESTAN ACTIVOS

        'FsClienteProceso.visible = True

        If Not Me.IsPostBack Then
            telefono_activo = Request.QueryString("telefono")
            If telefono_activo <> "" Then
                Crear_Cookies("IpPredictivo", Request.QueryString("ip"))
                Crear_Cookies("Anexo", Request.QueryString("anexo"))
                Carga_gestion_pred()
            Else
                Carga_gestion()
                Crear_Cookies("IpPredictivo", "")
            End If
            Dim dtp As DataTable = ctl.FunctionGlobal(":pusuario▓" & Request.QueryString("usuario"), "SQLUSR003")
            If Not dtp Is Nothing Then
                If dtp.Rows.Count > 0 Then
                    If dtp.Rows(0)(0) > 0 And Trim(telefono_activo) = "" Then
                        Response.Redirect("gestionb.htm")
                    End If
                End If
            End If
            ctl.FunctionGlobal(":psesgestionƒ:pusuario▓" & 1 & "ƒ" & Request.QueryString("usuario"), "SQLUSR002")
        End If
        ctl = Nothing
        IPAsterisk = cnxn.IPAsterisk
        Dim strScript As String = "Efectividad();Cobertura();"
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "Estadistica", strScript, True)

        cboEstadoPropuesta.Text = "Pendiente"

        Dim var_tipo_per As String
        var_tipo_per = Trim(Session("NonbrePerfil"))

        ''''''''''''''''''''''''''''''''''''''''''''''' NUEVA FUNCIONALIDAD DINAMICA PARA LAS CARTERAS CUYOS CLIENTES SON EMPRESA O PERSONA
        Carga_Empresa_O_Persona()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '''''''''''''''''' NUEVA FUNCIONALIDAD DINAMICA PARA LOS GESTORES "AGENDA" LA CUAL MOSTRARA LA ULTIMA LLAMADA A EFECTUAR AL CLIENTE
        'Carga_Agenda_Cliente()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '''''''''''''''''' NUEVA FUNCIONALIDAD DINAMICA EXCLUSIVA PARA LA CARTERA BCP PLM
        If idCartera = 30 Then
            Mostrar_FieldSet_Identificacion_Cliente_Proceso_Venta()
            Carga_Identificacion_Cliente_Proceso_Venta()
        End If


        'If var_tipo_per <> "GESTOR" Then
        '    If idCartera = 63 Then
        '        htlink.Visible = True
        '        htlink.NavigateUrl = "~/img/DashBoard_FALABELLA.xlsb"
        '    End If

        '    If idCartera = 57 Then
        '        htlink.Visible = True
        '        htlink.NavigateUrl = "~/img/DashBoard_CEN-CASTIGO.xlsb"
        '    End If

        '    If idCartera = 59 Then
        '        htlink.Visible = True
        '        htlink.NavigateUrl = "~/img/DashBoard_CEN-BLOQUEO.xlsb"
        '    End If

        '    If idCartera = 106 Then
        '        htlink.Visible = True
        '        htlink.NavigateUrl = "~/img/DashBoard_FINAN_OH.xlsb"
        '    End If

        '    If idCartera = 34 Then
        '        htlink.Visible = True
        '        htlink.NavigateUrl = "~/img/DashBoard_BCP-JUDICIAL.xlsb"
        '    End If

        '    If idCartera = 85 Or idCartera = 89 Then
        '        htlink.Visible = True
        '        htlink.NavigateUrl = "~/img/DashBoard_BCP-PERSONAS.xlsb"
        '    End If

        '    If idCartera = 30 Or idCartera = 32 Then
        '        htlink.Visible = True
        '        htlink.NavigateUrl = "~/img/DashBoard_BCP-PLM.xlsb"
        '    End If

        '    If idCartera = 65 Then
        '        htlink.Visible = True
        '        htlink.NavigateUrl = "~/img/DashBoard_BCP-PYME.xlsb"
        '    End If

        'Hacemos visible el boton para ver la Liquidación de AFP
        If idCartera = 109 Or idCartera = 110 Or idCartera = 112 Then
            imgLiquidacion.Visible = True
            lblLiquidacion.Visible = True
        End If

        'End If

    End Sub

    Sub Carga_Pred()
        InicioPredictivo = Now()
        If Not Me.IsPostBack Then

            CboHoraProxAcc.Cargar_dll()
            CboMinutoProxAcc.Cargar_dll()
            DNI = Request.QueryString("DNI")

            idCartera = Request.QueryString("IdCartera")

            NumTelf.Value = Request.QueryString("Telefono")

            Hidusuario.Value = Obtiene_Cookies("idusuario")
            HNombreGestor.Value = Obtiene_Cookies("NombreUsuario")
            Husuario.Value = Obtiene_Cookies("usuario")
            Hidcliente.Value = Request.QueryString("idcliente")
            TipoCartera = Request.QueryString("tipocartera")
            Dim tp = Obtiene_Cookies("TipoUsuario")

            If tp <> "GESTOR" Then
                CtlTelefono.Activa_Delete = True
            End If

            Carga_gestion()
            Dim sendere As Object
            Dim eu As System.EventArgs
            Call form1_Load(sendere, eu)
        End If

    End Sub

    Sub Carga()
        CboHoraProxAcc.Cargar_dll()
        CboMinutoProxAcc.Cargar_dll()
        DNI = Request.QueryString("DNI")
        idCartera = Request.QueryString("IdCartera")
        Hidusuario.Value = Request.QueryString("Idusuario")
        HNombreGestor.Value = Request.QueryString("NombreGestor")
        Husuario.Value = Request.QueryString("usuario")
        Hidcliente.Value = Request.QueryString("idcliente")
        TipoCartera = Request.QueryString("tipocartera")
        telefono_activo = Request.QueryString("telefono")

        If telefono_activo <> "" And Hidcliente.Value <> "" And DNI Is Nothing Then
            Dim dt = conexion.FunctionGlobal(":pIdCliente▓" & idcliente, "PREDIC01")
            If Not dt Is Nothing Then
                If dt.rows.count > 0 Then
                    DNI = dt.Rows(0)(0)
                    idCartera = dt.Rows(0)(4)
                    Hidusuario.Value = dt.Rows(0)(1)
                    HNombreGestor.Value = dt.Rows(0)(2)
                    Husuario.Value = dt.Rows(0)(3)
                    TipoCartera = dt.Rows(0)(6)
                End If
            Else
                CtlMensajes1.Mensaje("Error... cliente no existe")
                Exit Sub
            End If
        End If
        Dim tp = Obtiene_Cookies("TipoUsuario")
        If tp <> "GESTOR" Then
            CtlTelefono.Activa_Delete = True
        End If

        Carga_gestion()

    End Sub

    Sub Carga_Empresa_O_Persona()

        If idCartera = 65 Or idCartera = 110 Then

            Visualizar_Campos_Empresa()
        Else
            If idCartera = 85 Or idCartera = 89 Or idCartera = 91 Then
                If Dni_Activo.Substring(0, 1) = 6 Then

                    Visualizar_Campos_Empresa()
                Else
                    Visualizar_Campos_Persona()
                End If
            Else
                Visualizar_Campos_Persona()
            End If
        End If
    End Sub

    Sub Visualizar_Campos_Empresa()
        lblFila1_Columna1.Text = "Rep. Legal"
        lblFila1_Columna1.Visible = True
        txtRepresentanteLegal.Visible = True

        lblFila1_Columna2.Text = "Aval"
        lblFila1_Columna2.Visible = True
        txtAvalEmpresa.Visible = True

        lblFila2_Columna1.Text = "Encargado"
        lblFila2_Columna1.Visible = True
        txtEncargado.Visible = True

        lblFila2_Columna2.Text = "Relacionado"
        lblFila2_Columna2.Visible = True
        txtRelacionado.Visible = True

        lblFila3_Columna1.Text = "Correo"
        lblFila3_Columna1.Visible = True
        txtEmail.Visible = True

        lblFila3_Columna2.Text = "RCC"
        lblFila3_Columna2.Visible = True
        txtRCC.Visible = True

        lblFila4_Columna1.Text = "RUC"
        lblFila4_Columna1.Visible = True
        txtRUC.Visible = True

        lblFila4_Columna2.Text = "SUNAT"
        lblFila4_Columna2.Visible = True
        txtSunat.Visible = True
    End Sub

    Sub Visualizar_Campos_Persona()
        lblFila1_Columna1.Text = "Cent.Laboral"
        lblFila1_Columna1.Visible = True
        txtCentroLaboral.Visible = True

        lblFila1_Columna2.Text = "Edad"
        lblFila1_Columna2.Visible = True
        txtEdad.Visible = True

        lblFila2_Columna1.Text = "Cónyuge"
        lblFila2_Columna1.Visible = True
        txtConyugue.Visible = True

        lblFila2_Columna2.Text = "Aval"
        lblFila2_Columna2.Visible = True
        txtAvalPersona.Visible = True

        lblFila3_Columna1.Text = "E.mail"
        lblFila3_Columna1.Visible = True
        txtEmail.Visible = True

        lblFila3_Columna2.Text = "Ingresos"
        lblFila3_Columna2.Visible = True
        txtIngreso.Visible = True
    End Sub

    Sub Mostrar_FieldSet_Agenda()
        lblFechaAgenda.Visible = True
        txtFechaAgenda.Visible = True
        'imgFechaAgenda.Visible = True
        lblHoraAgenda.Visible = True
        txtHoraAgenda.Visible = True
        lblMinutoAgenda.Visible = True
        txtMinutoAgenda.Visible = True
        'lblFormato.Visible = True
        lblObservacion.Visible = True
        txtObservacion.Visible = True
        btnGrabaAgenda.Visible = True

        txtFechaAgenda.Enabled = False
        txtHoraAgenda.Enabled = True
        txtMinutoAgenda.Enabled = True
        'lblFormato.Enabled = False
        lblObservacion.Enabled = True
        txtObservacion.Enabled = True
    End Sub

    Sub Carga_Agenda_Cliente()

        If txtFechaAgenda.Text = "" And txtHoraAgenda.Text = "" And txtMinutoAgenda.Text = "" Then
            Dim dt As New DataTable
            Dim criterio = ""
            Using dt
                criterio = " idUsuario = " & Hidusuario.Value & " AND idCliente = " & idcliente & " AND idCartera = " & idCartera & " AND Estado = 'A' "
                dt = conexion.FunctionGlobal(":criterio▓" & criterio, "SQL_AGE_002")
                'dt = conexion.FunctionGlobal(":criterio▓" & criterio, "SQL_AGE_002_PRUEBA")

                If dt.Rows.Count = 0 Then
                    Exit Sub
                Else
                    txtFechaAgenda.Text = IIf(Not IsDBNull(dt(0)("FechaAgenda")), Format(CDate(dt(0)("FechaAgenda")), "dd/MM/yyyy"), "")
                    txtHoraAgenda.Text = IIf(Not IsDBNull(dt(0)("HoraAgenda")), dt(0)("HoraAgenda"), "")
                    txtMinutoAgenda.Text = IIf(Not IsDBNull(dt(0)("MinutoAgenda")), dt(0)("MinutoAgenda"), "")
                End If
            End Using
        End If

    End Sub

    'ESTE METODO FUE CREADO EXCLUSIVAMENTE PARA LA CARTERA DE BCP PLM
    Sub Mostrar_FieldSet_Identificacion_Cliente_Proceso_Venta()
        lblClienteProcesoVenta.Visible = True
        chbFinanciado.Visible = True
        chbEfectivo.Visible = True
        btnGrabarProcesoVenta.Visible = True

        lblClienteProcesoVenta.Enabled = False
        chbFinanciado.Enabled = True
        chbEfectivo.Enabled = True
        btnGrabarProcesoVenta.Enabled = True
    End Sub

    'ESTE METODO FUE CREADO EXCLUSIVAMENTE PARA LA CARTERA DE BCP PLM
    Sub Carga_Identificacion_Cliente_Proceso_Venta()

        If chbFinanciado.Checked = False And chbEfectivo.Checked = False Then
            Dim dt As New DataTable
            Dim criterio = ""
            Using dt
                criterio = "idCliente = " & idcliente & " AND idCartera = " & idCartera & " AND Estado = 'A' "

                dt = conexion.FunctionGlobal(":criterio▓" & criterio, "SQL_PROVEN_002")

                If dt.Rows.Count = 0 Then
                    Exit Sub
                Else
                    'txtFechaAgenda.Text = IIf(Not IsDBNull(dt(0)("FechaAgenda")), Format(CDate(dt(0)("FechaAgenda")), "dd/MM/yyyy"), "")
                    'txtHoraAgenda.Text = IIf(Not IsDBNull(dt(0)("HoraAgenda")), dt(0)("HoraAgenda"), "")
                    'txtMinutoAgenda.Text = IIf(Not IsDBNull(dt(0)("MinutoAgenda")), dt(0)("MinutoAgenda"), "")

                    If dt(0)("Financiado") = "SI" Then
                        chbFinanciado.Checked = True
                    Else
                        chbFinanciado.Checked = False
                    End If

                    If dt(0)("Efectivo") = "SI" Then
                        chbEfectivo.Checked = True
                    Else
                        chbEfectivo.Checked = False
                    End If

                End If
            End Using
        End If

    End Sub

    Sub CargaPanelCompromiso()
        'Combo Nro Operacion
        txtNroOperacionCompromisoManteniemito.Limpia()
        If TipoCartera = "ACTIVA" Then
            txtNroOperacionCompromisoManteniemito.Procedimiento = "SQL_N_GEST034"
        Else
            txtNroOperacionCompromisoManteniemito.Procedimiento = "SQL_N_GEST033"
        End If
        txtNroOperacionCompromisoManteniemito.Condicion = ":idcliente▓" & idcliente
        txtNroOperacionCompromisoManteniemito.Cargar_dll()

        'TextBox Monto
        txtMontoCompromisoMantenimiento.Text = ""

        'Combo Moneda
        cboMonedaCompromisoMantemiento.Limpia()
        cboMonedaCompromisoMantemiento.Condicion = ":idtabla▓105"
        cboMonedaCompromisoMantemiento.Cargar_dll()

        'Combo Tipo de Pago
        cboTipoPagoCompromisoMantenimiento.Limpia()
        cboTipoPagoCompromisoMantenimiento.Condicion = ":idtabla▓104"
        cboTipoPagoCompromisoMantenimiento.Cargar_dll()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''' PARTE DEL PANEL QUE CORRESPONDE AL PAGO

        'CheckBox Pagado
        chkPagadoCompromisoMantenimiento.Checked = False

        'TextBox Fecha de Pago
        txtFechaPagoCompromisoMantenimeinto.Text = ""

        'TextBox Monto del Pago
        txtMontoCompromisoMantenimientoA.Text = ""

        'Combo Moneda parte "Pago"
        cboMonedaCompromisoMantenimientoA.Limpia()
        cboMonedaCompromisoMantenimientoA.Condicion = ":idtabla▓105"
        cboMonedaCompromisoMantenimientoA.Cargar_dll()
    End Sub

    Private Sub btnCompromisos_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCompromisos.Click
        cboMonedaCompromisoMantemiento.Limpia()
        cboMonedaCompromisoMantemiento.Condicion = ":idtabla▓105"
        cboMonedaCompromisoMantemiento.Cargar_dll()
        cboMonedaCompromisoMantenimientoA.Limpia()
        cboMonedaCompromisoMantenimientoA.Condicion = ":idtabla▓105"
        cboMonedaCompromisoMantenimientoA.Cargar_dll()
        cboTipoPagoCompromisoMantenimiento.Limpia()
        cboTipoPagoCompromisoMantenimiento.Condicion = ":idtabla▓104"
        cboTipoPagoCompromisoMantenimiento.Cargar_dll()
        CargaNombrePanel()
        pnlCompromisos.Visible = True
        CargarCompromiso()
        txtFechaPagoCompromisoMantenimeinto.Enabled = True
        txtMontoCompromisoMantenimientoA.Enabled = True
    End Sub

    Sub CargarCompromiso()
        Dim ctl As New BL.Cobranza
        Dim dt As New DataTable
        Try
            idcliente = Obtiene_Cookies("idcliente")

            dt = ctl.FunctionGlobal(":idcliente▓" & idcliente, "SQL_N_GEST026")

            gvCompromisos.SourceDataTable = dt

            Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")
            If PerfilUsuario <> "GESTOR" Then
                gvCompromisos.Activa_Delete = True
            End If
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al cargar los compromisos")
        End Try
    End Sub

    Private Sub btnDeuda_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDeuda.Click
        lblDeudaClienteTitulo.Text = txtnombre.Text
        pnlDeuda.Visible = True
        CargarDeuda()
    End Sub

    Sub CargarDeuda()
        Dim ctl As New BL.Cobranza
        Dim dt As New DataTable
        Try
            If TipoCartera = "ACTIVA" Then
                dt = ctl.FunctionGlobal(":idcliente▓" & idcliente, "SQL_N_GEST028")
            Else
                dt = ctl.FunctionGlobal(":idcliente▓" & idcliente, "SQL_N_GEST029")
            End If
            gvDeuda.SourceDataTable = dt
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al cargar las deuda")
        End Try
    End Sub

    Private Sub btnInfAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnInfAdd.Click
        pnlInformacionAdicional.Visible = True
        Label18.Text = txtnombre.Text
        CargarInformacionAdicional()
    End Sub

    Sub CargarInformacionAdicional()
        Dim ctl As New BL.Cobranza
        Try
            gvInformacionAdicional.SourceDataTable = prepara_informacion()
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al cargar la informacion adicional")
        End Try
    End Sub

    Function prepara_informacion() As DataTable
        Dim ctl As New BL.Cobranza
        Dim dt As New DataTable
        Dim table As New DataTable
        Dim column As DataColumn
        Dim row As DataRow
        Try
            dt = ctl.FunctionGlobal(":idcarteraƒ:idclienteƒ:tipo▓" & cbocartera.Text & "ƒ0ƒ1", "SQL_N_GEST025")
            'Cabecera
            If Not dt Is Nothing Then
                Dim FilArr = Split(dt(0)(3), "€")
                For i = 0 To FilArr.Length - 2
                    column = New DataColumn()
                    column.DataType = System.Type.GetType("System.String")
                    column.ColumnName = FilArr(i)
                    table.Columns.Add(column)
                Next
            End If
            dt = Nothing
            ' detalle
            dt = ctl.FunctionGlobal(":idcarteraƒ:idclienteƒ:tipo▓" & cbocartera.Text & "ƒ" & idcliente & "ƒ0", "SQL_N_GEST025")
            'Cabecera
            If Not dt Is Nothing Then
                For i = 0 To dt.Rows.Count - 1
                    Dim FilArr = Split(dt(i)(3), "€")
                    row = table.NewRow()
                    For c = 0 To FilArr.Length - 2
                        row(table.Columns(c)) = FilArr(c)
                    Next
                    table.Rows.Add(row)
                Next
            End If
            dt = Nothing
            Return table
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al cargar la informacion adicional")
            Return Nothing
        End Try
    End Function

    Public Property Dni_Activo() As String
        Get
            Return lblDni_Activo.Text
        End Get
        Set(ByVal value As String)
            lblDni_Activo.Text = value
        End Set
    End Property

    Private Sub btnPagos_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPagos.Click
        pnlPagos.Visible = True
        txtDNIPagos.Text = Dni_Activo
        txtClientePagos.Text = txtnombre.Text
        Id_Cliente_Pagos = idcliente
        cboCarteraPagos.Text = cbocartera.Text
        txtFechaInicioPagos.Text = "01/" & Date.Now.ToString("MM/yyyy")
        txtFechaFinPagos.Text = DateTime.DaysInMonth(Date.Now.Year, Date.Now.Month) & "/" & Date.Now.ToString("MM/yyyy")
        CargarPagos()
    End Sub

    Sub CargarPagos()
        Dim ctl As New BL.Cobranza
        Dim dt As New DataTable
        Dim criterio As String = ""
        If cbocartera.Text = cboCarteraPagos.Text Then
            criterio = criterio & " p.IdCliente=" & Id_Cliente_Pagos
        Else
            criterio = criterio & " p.Dni='" & txtDNIPagos.Text & "' and c.IdCartera='" & cboCarteraPagos.Value & "'"
        End If
        If Len(Trim(txtFechaInicioPagos.Text)) <> 0 Then
            criterio = criterio & " and convert(date,p.FechaPago,103)>='" & txtFechaInicioPagos.Text & "'"
        End If
        If Len(Trim(txtFechaFinPagos.Text)) <> 0 Then
            criterio = criterio & " and convert(date,p.FechaPago,103)<='" & txtFechaFinPagos.Text & "'"
        End If
        Try
            dt = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_N_GEST023")
            gvConsultaPagos.SourceDataTable = dt
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al cargar los pagos del cliente")
        End Try
    End Sub

    Public Property Id_Cliente_Pagos() As String
        Get
            Return lblIdClientePagos.Text
        End Get
        Set(ByVal value As String)
            lblIdClientePagos.Text = value
        End Set
    End Property

    Private Sub btnCerrarPagos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrarPagos.Click
        pnlPagos.Visible = False
        Crear_Cookies("Estado", "")
    End Sub

    Private Sub btnCarrarInformacionAdicional_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCarrarInformacionAdicional.Click
        pnlInformacionAdicional.Visible = False
    End Sub

    Private Sub btnAgregarCompromisos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregarCompromisos.Click
        pnlCompromisoMantenimiento.Visible = True
        CargaPanelCompromiso()
    End Sub

    Private Sub btnCerrarCompromisos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrarCompromisos.Click
        pnlCompromisos.Visible = False
    End Sub

    Public Property Id_Propuesta_Activa() As String
        Get
            Return lblId_Propuesta.Text
        End Get
        Set(ByVal value As String)
            lblId_Propuesta.Text = value
        End Set
    End Property

    Private Sub imgGrabarPropuesta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgGrabarPropuesta.Click

        'model.SavePost(currentId, __txtSustentoPropuesta.Text)

        If Id_Propuesta_Activa = "" Then
            RegistraNuevaPropuesta()
        Else
            ActualizaPropuesta()
        End If

    End Sub

    Sub RegistraNuevaPropuesta()
        Dim ctl As New BL.Cobranza

        Try
            ctl.FunctionGlobal(":idclienteƒ:dniƒ:nrocuentaƒ:monedaƒ:montopropuestaƒ:fechapropuestaƒ:sustentoƒ:idusuarioƒ:nrocuotasƒ:estado▓" & _
                            idcliente & "ƒ" & Dni_Activo & "ƒ" & cboOperacionPropuesta.Text & "ƒ" & cboMonedaPropuesta.Value & "ƒ" & txtMontoPropuesta.Text & "ƒ" & txtFechaGeneroPropuesta.Text & "ƒ" & _
                            txtSustentoPropuesta.Text & "ƒ" & idUsuario & "ƒ" & txtNroPartesPropuesta.Text & "ƒ" & "P", "SQL_N_GEST027")
            pnlMantenimientoPropuesta.Visible = False
            cargarPropuesta()

        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al ingresar la propuesta")
        End Try
    End Sub

    Sub On_Off_Controles_Propuesta()
        pnlMantenimientoPropuesta.Visible = True
        txtFechaGeneroPropuesta.Enabled = True
        txtFechaPagoPropuesta.Enabled = True
        txtFechaRespuesta.Enabled = True

        txtFechaGeneroPropuesta.ReadOnly = True
        txtFechaGeneroPropuesta.BackColor = Gainsboro
        cboEstadoPropuesta.Visible = True
        cboOperacionPropuesta.Activa = False
        cboOperacionPropuesta.Color = Gainsboro
        txtNroPartesPropuesta.ReadOnly = True
        txtNroPartesPropuesta.BackColor = Gainsboro
        txtMontoPropuesta.ReadOnly = True
        txtMontoPropuesta.BackColor = Gainsboro
        cboMonedaPropuesta.Activa = False
        cboMonedaPropuesta.Color = Gainsboro
        txtSustentoPropuesta.ReadOnly = True
        txtSustentoPropuesta.BackColor = Gainsboro

        lblFechaActivacionPropuesta.Visible = True
        txtFechaRespuesta.Visible = True
        'imgFechaRespuesta.Visible = True
        lblObservacionesPropuesta.Visible = True
        txtObservacionPropuesta.Visible = True
    End Sub

    Sub ActualizaPropuesta()
        Dim ctl As New BL.Cobranza

        Try
            ctl.FunctionGlobal(":idpropuestaƒ:estadoƒ:fechaAceptacionƒ:obser_validƒ:idUsuarioPropuesta▓" & _
                            Id_Propuesta_Activa & "ƒ" & cboEstadoPropuesta.Value & "ƒ" & txtFechaRespuesta.Text & "ƒ" & txtObservacionPropuesta.Text & "ƒ" & Hidusuario.Value, "SQL_N_GEST069")

            pnlMantenimientoPropuesta.Visible = False
            cargarPropuesta()
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al actualizar la propuesta")
        End Try
    End Sub

    Private Sub btnCerrarDeuda_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrarDeuda.Click
        pnlDeuda.Visible = False
    End Sub

    Private Sub gvCompromisos_Nuevo() Handles gvCompromisos.Nuevo
        pnlCompromisoMantenimiento.Visible = True
        imgGrabarCompromisoMantenimiento.Visible = True
        pnlCompromisoMantenimiento.Enabled = True
        txtFechaGeneroCompromisoMantenieminto.Text = Date.Now.ToString("dd/MM/yyyy")
        txtFechaCompromisoMantenimiento.Text = Date.Now.ToString("dd/MM/yyyy")
        txtMontoCompromisoMantenimiento.Text = ""
        chkPagadoCompromisoMantenimiento.Checked = False
        txtMontoCompromisoMantenimientoA.Text = ""
        id_gestion_activa = Hidgestion.Value
        cboMonedaCompromisoMantemiento.Limpia()
        cboMonedaCompromisoMantemiento.Condicion = ":idtabla▓105"
        cboMonedaCompromisoMantemiento.Cargar_dll()
        cboMonedaCompromisoMantenimientoA.Limpia()
        cboMonedaCompromisoMantenimientoA.Condicion = ":idtabla▓105"
        cboMonedaCompromisoMantenimientoA.Cargar_dll()
        cboTipoPagoCompromisoMantenimiento.Limpia()
        cboTipoPagoCompromisoMantenimiento.Condicion = ":idtabla▓104"
        cboTipoPagoCompromisoMantenimiento.Cargar_dll()
        lblId_Compromiso.Text = ""
        Dim ctls As New Controles.General
        If RDBCall.Checked = True Then
            'Call ctls.Gestion_Telefono_grilla(Dni_Activo, idEmpresa, CtlGestionTelefonica)
            Call ctls.Gestion_Telefono_grilla_Positivas(Dni_Activo, idEmpresa, CtlGestTelefPositivas)
            Call ctls.Gestion_Telefono_grilla_Negativas(Dni_Activo, idEmpresa, CtlGestTelefNegativas)
        ElseIf RDBCanalesAlternos.Checked = True Then
            Call ctls.Gestion_Campo_grilla(Dni_Activo, idEmpresa, CtlGestionCampo)
        End If
        Limpiar_Gestion()
        CargarTelefonos(Dni_Activo)
        gvCompromisos.OpocionNuevo = True
        txtFechaPagoCompromisoMantenimeinto.Enabled = True
        txtMontoCompromisoMantenimientoA.Enabled = True
        Crear_Cookies("Estado", "")
    End Sub

    ''ELIMINAR REGISTRO DE COMPROMISO - SOLO ADMINISTRADOR
    Private Sub gvCompromisos_RowDeleting(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvCompromisos.RowDeleting
        Dim ctl As New BL.Cobranza
        Dim row As GridViewRow = GV.Rows(e.RowIndex)
        Try
            Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")

            If PerfilUsuario = "ADMINISTRADOR" Or PerfilUsuario = "SECTORISTA" Then

                ctl.FunctionGlobal(":idcompromiso▓" & row.Cells(14).Text, "SQL_N_GEST031")
                CargarCompromiso()
                CtlMensajes1.Mensaje("El compromiso ha sido eliminado con éxito.")

            ElseIf PerfilUsuario = "SUPERVISOR" Then

                If Hidusuario.Value = 345 Or Hidusuario.Value = 350 Or Hidusuario.Value = 833 Or Hidusuario.Value = 1982 Then

                    ctl.FunctionGlobal(":idcompromiso▓" & row.Cells(14).Text, "SQL_N_GEST031")
                    CargarCompromiso()
                    CtlMensajes1.Mensaje("El compromiso ha sido eliminado con éxito.")

                Else
                    CtlMensajes1.Mensaje("Usted no tiene permiso para eliminar compromisos.", "Advertencia")
                End If
            Else
                CtlMensajes1.Mensaje("Usted no tiene permiso para eliminar compromisos.", "Advertencia")
            End If
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al eliminar el compromiso, vuelva a intertarlo")
        End Try
    End Sub

    ''EDITAR REGISTRO DE COMPROMISO - SOLO ADMINISTRADOR
    Private Sub gvCompromisos_RowEditing(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs, ByVal row As System.Web.UI.WebControls.GridViewRow) Handles gvCompromisos.RowEditing

        Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")

        'FALTA REGISTRAR EL PAGO
        If row.Cells(10).Text = "NO" Then

            'HACER VISIBLE EL PANEL
            pnlCompromisoMantenimiento.Visible = True

            'MOSTRAR LA DATA DE LA PARTE DEL COMPROMISO
            MostrarData_En_Controles_Compromiso(row)

            'BLOQUEAR LA PARTE CORRESPONDIENTE AL COMPROMISO PARA QUE NO LE PERMITA EDITAR
            If PerfilUsuario = "ADMINISTRADOR" Or PerfilUsuario = "SECTORISTA" Then
                Gestionar_Controles_Compromiso(True)

            ElseIf PerfilUsuario = "SUPERVISOR" Then

                If Hidusuario.Value = 345 Or Hidusuario.Value = 350 Or Hidusuario.Value = 833 Or Hidusuario.Value = 1982 Then
                    Gestionar_Controles_Compromiso(True)
                Else
                    Gestionar_Controles_Compromiso(False)
                End If
            Else
                Gestionar_Controles_Compromiso(False)
            End If

            'SOLO DEJAR HABILITADO LA PARTE QUE CORRESPONDE AL REGISTRO DEL PAGO
            Gestionar_Controles_Pago(True)

            Crear_Cookies("idPago", "0")
            Seleccionar_chkPagadoCompromisoMantenimiento(False)

            'EL PAGO YA FUE REGISTRADO
        Else
            If PerfilUsuario = "ADMINISTRADOR" Or PerfilUsuario = "SECTORISTA" Then
                'HACER VISIBLE EL PANEL
                pnlCompromisoMantenimiento.Visible = True

                'MOSTRAR LA DATA DEL COMPROMISO Y DEL PAGO
                MostrarData_En_Controles_Compromiso(row)
                MostrarData_En_Controles_Pago(row)

                'DEJAR HABILITADO TODO LO DEL PANEL (COMPROMISO + PAGO)
                Gestionar_Controles_Compromiso(True)
                Gestionar_Controles_Pago(True)
            ElseIf PerfilUsuario = "SUPERVISOR" Then

                If Hidusuario.Value = 345 Or Hidusuario.Value = 350 Or Hidusuario.Value = 833 Or Hidusuario.Value = 1982 Then
                    'HACER VISIBLE EL PANEL
                    pnlCompromisoMantenimiento.Visible = True

                    'MOSTRAR LA DATA DEL COMPROMISO Y DEL PAGO
                    MostrarData_En_Controles_Compromiso(row)
                    MostrarData_En_Controles_Pago(row)

                    'DEJAR HABILITADO TODO LO DEL PANEL (COMPROMISO + PAGO)
                    Gestionar_Controles_Compromiso(True)
                    Gestionar_Controles_Pago(True)
                Else
                    CtlMensajes1.Mensaje("Usted no tiene permiso para editar este compromiso.", "Advertencia!!")
                End If
            Else
                CtlMensajes1.Mensaje("Usted no tiene permiso para editar este compromiso.", "Advertencia!!")
            End If

            Crear_Cookies("idPago", row.Cells(15).Text)
            idPago = Replace(row.Cells(15).Text, "&nbsp;", "0")
            PerfilUsuario = Obtiene_Cookies("TipoUsuario")

        End If

    End Sub

    Sub MostrarData_En_Controles_Compromiso(ByVal row As System.Web.UI.WebControls.GridViewRow)

        lblId_Compromiso.Text = row.Cells(14).Text
        txtFechaGeneroCompromisoMantenieminto.Text = Left(row.Cells(4).Text, 10)
        txtFechaCompromisoMantenimiento.Text = Left(row.Cells(5).Text, 10)
        txtNroOperacionCompromisoManteniemito.Text = row.Cells(6).Text
        txtMontoCompromisoMantenimiento.Text = row.Cells(7).Text
        cboMonedaCompromisoMantemiento.Value = Mid(row.Cells(8).Text, 1, 1)
        cboTipoPagoCompromisoMantenimiento.Value = row.Cells(9).Text

    End Sub

    Sub MostrarData_En_Controles_Pago(ByVal row As System.Web.UI.WebControls.GridViewRow)

        Seleccionar_chkPagadoCompromisoMantenimiento(True)
        txtFechaPagoCompromisoMantenimeinto.Text = Left(Replace(row.Cells(11).Text, "&nbsp;", ""), 10)
        txtMontoCompromisoMantenimientoA.Text = Replace(row.Cells(12).Text, "&nbsp;", "")
        cboMonedaCompromisoMantenimientoA.Value = Replace(row.Cells(13).Text, "&nbsp;", "")

        Crear_Cookies("idPago", row.Cells(15).Text)
        idPago = Replace(row.Cells(15).Text, "&nbsp;", "0")
        Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")

    End Sub

    Sub Gestionar_Controles_Compromiso(ByVal valor As Boolean)

        txtFechaGeneroCompromisoMantenieminto.Enabled = valor
        txtFechaGeneroCompromisoMantenieminto.ReadOnly = Not valor
        'imgFechaGeneroCOmpromisoMantenimiento.Visible = valor

        txtFechaCompromisoMantenimiento.Enabled = valor
        txtFechaCompromisoMantenimiento.ReadOnly = Not valor
        'imgFechaCOmpromisoMantenimeinto.Visible = valor

        txtNroOperacionCompromisoManteniemito.Activa = valor

        txtMontoCompromisoMantenimiento.Enabled = valor
        txtMontoCompromisoMantenimiento.ReadOnly = Not valor

        cboMonedaCompromisoMantemiento.Activa = valor

        cboTipoPagoCompromisoMantenimiento.Activa = valor

        If valor = False Then
            txtFechaGeneroCompromisoMantenieminto.BackColor = Gainsboro
            txtFechaCompromisoMantenimiento.BackColor = Gainsboro
            txtNroOperacionCompromisoManteniemito.Color = Gainsboro
            txtMontoCompromisoMantenimiento.BackColor = Gainsboro
            cboMonedaCompromisoMantemiento.Color = Gainsboro
            cboTipoPagoCompromisoMantenimiento.Color = Gainsboro
        End If

    End Sub

    Sub Gestionar_Controles_Pago(ByVal valor As Boolean)

        chkPagadoCompromisoMantenimiento.Enabled = valor

        txtFechaPagoCompromisoMantenimeinto.Enabled = valor

        txtMontoCompromisoMantenimientoA.Enabled = valor

        cboMonedaCompromisoMantenimientoA.Activa = valor

    End Sub

    Sub Seleccionar_chkPagadoCompromisoMantenimiento(ByVal valor As Boolean)
        chkPagadoCompromisoMantenimiento.Checked = valor
    End Sub

    Private Sub imgCerrarCOmpromisoMantenimiento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCerrarCOmpromisoMantenimiento.Click

        If idCartera = 109 Or idCartera = 110 Or idCartera = 112 Then
            pnlCompromisoMantenimiento.Visible = False
            Exit Sub
        End If

        '-VERIFICA ANTES DE CERRAR
        CtlMensajes1.Mensaje("El boton esta desactivado por el ingreso compromisos o pago del mismo es obligatorio", "")
        Exit Sub
        If txtFechaGeneroCompromisoMantenieminto.Text = "" Then
            CtlMensajes1.Mensaje("La fecha de registro del compromiso es obligatorio", "")
            Exit Sub
        End If
        If txtFechaCompromisoMantenimiento.Text = "" Then
            CtlMensajes1.Mensaje("La moneda del compromiso es obligatorio", "")
            Exit Sub
        End If
        If cboMonedaCompromisoMantemiento.Text = "" Then
            CtlMensajes1.Mensaje("La moneda del compromiso es obligatorio", "")
            Exit Sub
        End If
        If cboTipoPagoCompromisoMantenimiento.Text = "" Then
            CtlMensajes1.Mensaje("El tipo de pago del compromiso es obligatorio", "")
            Exit Sub
        End If
        If txtMontoCompromisoMantenimiento.Text = "" Then
            CtlMensajes1.Mensaje("El Monto del compromiso es obligatorio", "")
            Exit Sub
        End If

        If chkPagadoCompromisoMantenimiento.Checked Then
            If cboMonedaCompromisoMantenimientoA.Text = "" Then
                CtlMensajes1.Mensaje("moneda de pago del compromiso es obligatorio", "")
                Exit Sub
            End If
            If txtMontoCompromisoMantenimientoA.Text = "" Then
                CtlMensajes1.Mensaje("Monto de pago del compromiso es obligatorio", "")
                Exit Sub
            End If
            If txtFechaPagoCompromisoMantenimeinto.Text = "" Then
                CtlMensajes1.Mensaje("El pago del compromiso es obligatorio", "")
                Exit Sub
            End If
        End If
        pnlCompromisoMantenimiento.Visible = False
    End Sub

    Function Obtiene_Cookies(ByVal id) As String
        '// Recogemos la cookie que nos interese
        Try
            Dim cogeCookie As HttpCookie = Request.Cookies.Get(id)
            Return cogeCookie.Value
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Sub imgGrabarCompromisoMantenimiento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgGrabarCompromisoMantenimiento.Click
        Dim ctl As New BL.Cobranza
        idcliente = Obtiene_Cookies("idcliente")

        Dim cadenanopdp = ""

        '''''''' VARIABLES EXCLUSIVAS PARA CARTERA AFP
        Dim TotalDevengues As Integer = 0
        Dim MinimoPagoDevengues As Integer = 0
        Dim TotalCompromisosDevengues As Integer = 0
        '''''''''''''''''''''''''''''''''''''''''''''

        Try

            Dim fnci1 As New BL.Cobranza
            Dim Dtj1 As New DataTable
            Dim xSQL1 As String = ""
            Dim valor_car1 As Integer
            Dim Valor_diass As Integer

            xSQL1 = "select isnull(PagosSPDP,0) as PagosSPDP from cartera where idcartera= " & idCartera & " and estado='A'"

            Dtj1 = fnci1.FunctionConsulta(xSQL1)
            valor_car1 = Dtj1.Rows(0)("PagosSPDP")

            xSQL1 = "select isnull(caida,0) as caidat from cartera where idcartera= " & idCartera & " and estado='A'"

            Dtj1 = fnci1.FunctionConsulta(xSQL1)
            Valor_diass = Dtj1.Rows(0)("caidat")

            If valor_car1 = 1 And Day(CDate(txtFechaCompromisoMantenimiento.Text)) >= Valor_diass Then
                CtlMensajes1.Mensaje("La fecha de compromiso debe ser menor al " & Valor_diass & " del presente mes..")
                Exit Sub
            End If


            Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")
            If InStr(cadenanopdp, idCartera.ToString.Trim() + ",") > 0 Then

            End If
            '----
            If txtFechaGeneroCompromisoMantenieminto.Text = "" Then
                CtlMensajes1.Mensaje("La fecha de registro del compromiso es obligatorio", "")
                Exit Sub
            Else

            End If
            If txtFechaCompromisoMantenimiento.Text = "" Then
                CtlMensajes1.Mensaje("La moneda del compromiso es obligatorio", "")
                Exit Sub
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''' ESTA ZONA ES DE FILTROS (CANDADOS) POR FECHA ''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim Mesactu = Now.Date().Month
            Dim ComMens = CDate(txtFechaCompromisoMantenimiento.Text).Month

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''' ESTE FILTRO PERMITIRA QUE SE GRABEN SOLO GESTIONES DEL MES '''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If ComMens > Mesactu Then
                CtlMensajes1.Mensaje("El compromiso tiene que generarse para el MES actual", "")
                Return
            End If


            Dim Anoctu = Now.Date().Year
            Dim AnoMens = CDate(txtFechaCompromisoMantenimiento.Text).Year

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''' ESTE FILTRO PERMITIRA QUE SE GRABEN SOLO GESTIONES DEL AÑO '''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If AnoMens > Anoctu Then
                CtlMensajes1.Mensaje("El compromiso tiene que generarse para el Año actual", "")
                Return
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''' ESTE FILTRO PERMITIRA QUE NO SE GRABEN COMPROMISOS DE FECHA MENOR A LA FECHA QUE SE EFECTUA EL COMPROMISO ''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If CDate(txtFechaCompromisoMantenimiento.Text) < CDate(txtFechaGeneroCompromisoMantenieminto.Text) Then
                CtlMensajes1.Mensaje("La fecha de compromiso debe ser igual o mayor", "")
                Return
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''' ESTE CANDADO SE PUSO A SOLICITUD DEL SR. CHARLES Y APLICA A TODAS LAS CARTERAS ''''''''''''''''''
            '''''''''''''''''''''''''''' SOLO PERMITIRA GRABAR GESTIONES COMO MAXIMO HASTA EL 15 DEL PRESENTE '''''''''''''''''''''''
            ''''''''''''''''''''''''''''''' APLICADO A TODAS LAS CARTERAS, POR ELLO SE COLOCA EN ESTA LINEA '''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' '' ''Dim DiaCompromiso = CDate(txtFechaCompromisoMantenimiento.Text).Day

            ' '' ''If DiaCompromiso > 15 Then
            ' '' ''    CtlMensajes1.Mensaje("El compromiso a generar solo puede efectuarse como máximo hasta el 15 del presente.")
            ' '' ''    Exit Sub
            ' '' ''End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            'If cboMonedaCompromisoMantemiento.Text = "" Then
            If Trim(cboMonedaCompromisoMantemiento.Text) = "" Then
                CtlMensajes1.Mensaje("La moneda del compromiso es obligatorio", "")
                'Me.IsPostBack
                Exit Sub
            End If

            'If cboTipoPagoCompromisoMantenimiento.Text = "" Then
            If Trim(cboTipoPagoCompromisoMantenimiento.Text) = "" Then
                CtlMensajes1.Mensaje("El tipo de pago del compromiso es obligatorio", "")
                Exit Sub
            End If

            If txtMontoCompromisoMantenimiento.Text = "" Then
                CtlMensajes1.Mensaje("El Monto del compromiso es obligatorio", "")
                Exit Sub
            End If
            If cboMonedaCompromisoMantemiento.Text = "" Then
                CtlMensajes1.Mensaje("La moneda del compromiso es obligatorio", "")
                Exit Sub
            End If
            If Trim(txtNroOperacionCompromisoManteniemito.Text) = "" Then
                CtlMensajes1.Mensaje("Error... El número de operacion es obligatorio, verifique y vuelva a intentar")
                Exit Sub
            End If

            Dim moneda As String = ""
            If chkPagadoCompromisoMantenimiento.Checked Then
                If Trim(cboMonedaCompromisoMantenimientoA.Text) = "" Then
                    CtlMensajes1.Mensaje("moneda de pago del compromiso es obligatorio", "")
                    Exit Sub
                Else
                    moneda = Mid(cboMonedaCompromisoMantenimientoA.Text, 1, 1)
                End If
                If txtMontoCompromisoMantenimientoA.Text = "" Then
                    CtlMensajes1.Mensaje("Monto de pago del compromiso es obligatorio", "")
                    Exit Sub
                End If
                If txtFechaPagoCompromisoMantenimeinto.Text = "" Then
                    CtlMensajes1.Mensaje("El pago del compromiso es obligatorio", "")
                    Exit Sub
                End If
                If txtFechaPagoCompromisoMantenimeinto.Text <> "" And CDate(txtFechaPagoCompromisoMantenimeinto.Text) > Now.Date() Then
                    CtlMensajes1.Mensaje("El pago no puede tener fecha futura", "")
                    Exit Sub
                End If
            End If
            Dim pagado As String = ""
            If chkPagadoCompromisoMantenimiento.Checked = True Then
                pagado = "SI"
            Else
                pagado = "NO"
            End If

            Dim fechapago As String = ""

            If Trim(txtFechaPagoCompromisoMantenimeinto.Text) = "" Then
                fechapago = "null"
            Else
                fechapago = "'" & txtFechaPagoCompromisoMantenimeinto.Text & "'"
            End If

            ' Validando fechas
            Dim ComDia = CDate(txtFechaCompromisoMantenimiento.Text).Day
            Dim GenDia = CDate(txtFechaGeneroCompromisoMantenieminto.Text).Day
            Dim HoyDia = Now.Date().Day

            If Val(lblId_Compromiso.Text) = 0 Then

            End If


            Dim deudad As Single = 0
            Dim valor_mo_compro, valor_deud As Double
            Dim ctln As New Controles.General
            Dim Mensajenopago As String = ""
            Dim cadcartera = "7,28,33,46,50,"

            Dim fnci As New BL.Cobranza
            Dim Dtj As New DataTable
            Dim xSQL As String = ""
            Dim valor_car As Integer
            xSQL = "select isnull(PagosSPDP,0) as PagosSPDP from cartera where idcartera= " & idCartera & " and estado='A'"

            Dtj = fnci.FunctionConsulta(xSQL)
            valor_car = Dtj.Rows(0)("PagosSPDP")

            If valor_car = 1 Then
                If TipoCartera = "ACTIVA" Then
                    Dim dt As DataTable = ctln.Obtiene_Consulta(":pcriterio▓ cliente.idcliente=" & idcliente & " and Operacion2.NroCuenta ='" & txtNroOperacionCompromisoManteniemito.Text & "'", "SQL_BUSCAR_ACTIVA")
                    If Not dt Is Nothing Then
                        If dt.Rows.Count > 0 Then
                            deudad = dt.Rows(0)("DeudaTotal")
                        End If
                    End If
                Else
                    Dim dt = ctln.Obtiene_Consulta(":pcriterio▓ cliente.idcliente=" & idcliente & " and Operacion.NroOperacion='" & txtNroOperacionCompromisoManteniemito.Text & "'", "SQL_BUSCAR_CASTIGO")
                    If Not dt Is Nothing Then
                        If dt.Rows.Count > 0 Then
                            deudad = dt.Rows(0)("DeudaTotal")
                        End If
                    End If
                End If

                valor_mo_compro = txtMontoCompromisoMantenimiento.Text

                Dim xSQL2 As String
                xSQL2 = "select isnull(MetaRECT,0) as MetaRECT from cartera where idcartera= " & idCartera & " and estado='A'"

                Dtj = fnci.FunctionConsulta(xSQL2)
                valor_deud = Dtj.Rows(0)("MetaRECT")

                moneda = Mid(cboMonedaCompromisoMantemiento.Text, 1, 1)
                If moneda = "D" Then
                    valor_mo_compro = valor_mo_compro * 3.25
                End If

                If valor_mo_compro < valor_deud Then

                    CtlMensajes1.Mensaje("El compromiso generado debe ser mayor a " & valor_deud & " soles")
                    Exit Sub

                End If
            End If


            '---- Valida las fechas de compromisos que se han mayores a 7 dias despues de su fecha de genero de su compromiso
            Dim dtp = ctln.Obtiene_Consulta(":pidcliente▓" & idcliente, "GETPAG001")
            If dtp Is Nothing Then
                If Not chkPagadoCompromisoMantenimiento.Checked Then

                End If
            ElseIf dtp.rows.count < 1 Then
                If Not chkPagadoCompromisoMantenimiento.Checked Then

                End If
            End If

            id_gestion_activa = Obtiene_Cookies("idgestion")

            If Not Obtiene_Cookies("idPago") Is Nothing Then
                idPago = Obtiene_Cookies("idPago")
            End If

            If Trim(txtMontoCompromisoMantenimientoA.Text) <> "" And idPago < 1 Then
                moneda = IIf(Trim(cboMonedaCompromisoMantenimientoA.Text) = "", "S", Trim(cboMonedaCompromisoMantenimientoA.Text))
                moneda = Mid(moneda, 1, 1)
                Dim dtpp = ctl.FunctionGlobal(":pIdClienteƒ:pFechaPagoƒ:pmonedaƒ:pmontoƒ:pconceptoƒ:pnumoperacionƒ:pdniƒ:pidcartera▓" & idcliente & "ƒ" & Replace(fechapago, "'", "") & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & IIf(Trim(txtMontoCompromisoMantenimientoA.Text) = "", "S", Trim(txtMontoCompromisoMantenimientoA.Text)) & "ƒ" & "PAGO DEL COMPROMISO" & "ƒ" & txtNroOperacionCompromisoManteniemito.Text & "ƒ" & Dni_Activo & "ƒ" & idCartera, "QRYCP005")
                If dtpp.rows.count > 0 Then
                    idPago = dtpp.rows(0)(0)
                End If
                dtpp = Nothing
            Else
                If idPago > 0 Then
                    CtlMensajes1.Mensaje("El compromiso ya tiene un pago ingresado por lo que no puede ser modificado...")
                    pnlCompromisoMantenimiento.Visible = False
                    Exit Sub
                End If
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''' EN ESTA ZONA SE COLOCARÁ LA NUEVA VERSIÓN DEL CANDADO DINÁMICO '''''''''''''''''''''''''''''
            ''''''''''''''''' EL CUAL PERMITIRÁ ESTABLECER LOS FILTROS DEL MISMO A PARTIR DE LO REGISTRADO EN LA BD '''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim DiaCompromiso = CDate(txtFechaCompromisoMantenimiento.Text).Day
            Dim DiaGeneroCompromiso = CDate(txtFechaGeneroCompromisoMantenieminto.Text).Day

            Dim dt_Estado_Candado As DataTable
            Dim dt_NumDias_Candado As DataTable

            dt_Estado_Candado = ctl.FunctionGlobal(":pidcartera▓" & idCartera, "QRYC023")

            'Si la cartera tiene su candado con "Estado" activo
            If dt_Estado_Candado(0)("Estado") = "A" Then

                dt_NumDias_Candado = ctl.FunctionGlobal(":pidcartera▓" & idCartera, "QRYC024")
                Dim NumeroDias As Integer = dt_NumDias_Candado(0)("NumeroDias")

                If idCartera = 85 Or idCartera = 89 Or idCartera = 91 Then
                    'Valida que exista compriso
                    If lblId_Compromiso.Text <> "" Then
                        If PerfilUsuario <> "GESTOR" Then
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030A")
                        Else
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030")
                        End If

                        ''-----------------------------------------------------
                        '' CANDADOS POR RANGO DE MONTO DE LA DEUDA POR CLIENTE |
                        ''-----------------------------------------------------
                        ''       DT       |  CANDADO          |
                        ''-------------------------------------
                        ''    [1 - 500>   |  CANCELAR DT      |
                        ''   <501-10MIL>  |  MONTO MINIMO 500 |
                        ''  <10,001 - ++] |  5%  DE LA DT     |
                        ''-------------------------------------
                        'Si no Existe Compromiso Valida el Candado 
                    Else
                        If DiaCompromiso > ((NumeroDias - 1) + DiaGeneroCompromiso) Then
                            CtlMensajes1.Mensaje("El compromiso a generar solo puede efectuarse con un máximo de " & NumeroDias & " dias considerando hoy.")
                            Exit Sub
                        Else
                            Dim DeudaTotal As Decimal = 0

                            If cboMonedaCompromisoMantemiento.Text = "DOLARES" Then
                                DeudaTotal = Decimal.Parse(txtDeudaTotalUSD.Text)
                            Else
                                DeudaTotal = Decimal.Parse(txtDeudaTotalSoles.Text)
                            End If

                            If DeudaTotal > 10000.0 Then
                                '' MONTO MAYOR O IGUAL AL 5% DE LA DEUDA TOTAL
                                If txtMontoCompromisoMantenimiento.Text >= Porcentaje_5(DeudaTotal) Then
                                    ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                    txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                    txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                    txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")
                                Else
                                    CtlMensajes1.Mensaje("RECUERDA: El monto del compromiso debe ser mayor o igual al 5% de la deuda total.")

                                    ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                    txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                    txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                    txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")

                                End If
                            ElseIf DeudaTotal > 500.0 Then
                                '' MONTO MAYOR O IGUAL A 500
                                If txtMontoCompromisoMantenimiento.Text >= 500 Then
                                    ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                    txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                    txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                    txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")
                                Else
                                    CtlMensajes1.Mensaje("RECUERDA: El monto del compromiso debe ser como minimo S/.500.00.")

                                    ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                    txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                    txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                    txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")

                                End If
                            ElseIf DeudaTotal > 1.0 Then
                                '' CANCELAR DEUDA TOTAL
                                If txtMontoCompromisoMantenimiento.Text = DeudaTotal Then
                                    ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                    txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                    txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                    txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")
                                Else
                                    CtlMensajes1.Mensaje("RECUERDA: El monto del compromiso debe ser igual a la deuda total.")

                                    ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                    txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                    txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                    txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")

                                End If
                            End If
                        End If
                    End If
                    'Aqui Termina Bcp Candado Activo 

                    'Aqui Comienza Afp con Candado Activo
                ElseIf idCartera = 109 Or idCartera = 110 Or idCartera = 112 Then
                    If lblId_Compromiso.Text <> "" Then
                        If PerfilUsuario <> "GESTOR" Then
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030A")
                        Else
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030")
                        End If

                        ''-----------------------------------------------------------------------------------------------------------------------
                        ''FUNCIONALIDAD ADICIONAL PARA AFP (DEVENGUES)
                        ''SE COBRARÁ COMO MÍNIMO EL 50% DE LA DEUDA TOTAL + 1
                    Else
                        If DiaCompromiso > ((NumeroDias - 1) + DiaGeneroCompromiso) Then
                            CtlMensajes1.Mensaje("El compromiso a generar solo puede efectuarse con un máximo de " & NumeroDias & " dias a partir de hoy.")
                            Exit Sub
                        Else
                            imgCerrarCOmpromisoMantenimiento.Visible = False

                            Dtj.Clear()

                            Dim TraeCantidadDevengue As String
                            TraeCantidadDevengue = "select count(producto) as cantidad from Operacion where Estado not in ('E') and IdCliente = " & idcliente
                            Dtj = fnci.FunctionConsulta(TraeCantidadDevengue)
                            TotalDevengues = Dtj.Rows(0)("cantidad")

                            TotalCompromisosDevengues = TraerCompromisos()

                            If TotalDevengues = 0 Then
                                CtlMensajes1.Mensaje("Ocrurrio un problema con los devengues")
                                Exit Sub
                            ElseIf TotalDevengues = 1 Then
                                MinimoPagoDevengues = 1
                            ElseIf TotalDevengues = 2 Then
                                MinimoPagoDevengues = 2
                            Else
                                MinimoPagoDevengues = CInt(Math.Floor(TotalDevengues / 2)) + 1
                            End If

                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")

                            If TotalCompromisosDevengues < MinimoPagoDevengues Then

                                Dim MinimoPagoDevenguesMensaje As Integer = 0
                                MinimoPagoDevenguesMensaje = MinimoPagoDevengues - TotalCompromisosDevengues - 1

                                TotalCompromisosDevengues = TraerCompromisos()

                                If MinimoPagoDevenguesMensaje <> 0 Then

                                    CtlMensajes1.Mensaje("Le falta registrar " + CStr(MinimoPagoDevenguesMensaje) + " devengue(s).")
                                Else
                                    imgCerrarCOmpromisoMantenimiento.Visible = True
                                End If
                            Else
                                imgCerrarCOmpromisoMantenimiento.Visible = True
                            End If

                            CargaPanelCompromiso()
                        End If
                    End If
                    'Aqui termina AFp 	
                    'Aqui Comienza carteras distintas a AFP, o Bcp con Candado Activo -   
                Else
                    'Validamos si existe para poder actualizar el Pago 
                    If lblId_Compromiso.Text <> "" Then
                        If PerfilUsuario <> "GESTOR" Then
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                      txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                      txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                      txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030A")
                        Else
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                      txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                      txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                      txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030")
                        End If
                        'Si no existe el compromiso entonces es nuevo y validamos el Candado   
                    Else
                        If DiaCompromiso > ((NumeroDias - 1) + DiaGeneroCompromiso) Then
                            CtlMensajes1.Mensaje("El compromiso a generar solo puede efectuarse con un máximo de " & NumeroDias & " dias a partir de hoy.")
                            Exit Sub
                        Else
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & _
                                                IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & txtNroOperacionCompromisoManteniemito.Text & "ƒ" & _
                                                cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & _
                                                id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")
                        End If
                    End If
                End If
            Else
                ''If idCartera = 30 Then
                If idCartera = 85 Or idCartera = 89 Or idCartera = 91 Then
                    If lblId_Compromiso.Text <> "" Then
                        If PerfilUsuario <> "GESTOR" Then
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030A")
                        Else
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030")
                        End If

                        ''-----------------------------------------------------
                        '' CANDADOS POR RANGO DE MONTO DE LA DEUDA POR CLIENTE |
                        ''-----------------------------------------------------
                        ''       DT       |  CANDADO          |
                        ''-------------------------------------
                        ''    [1 - 500>   |  CANCELAR DT      |
                        ''   <501-10MIL>  |  MONTO MINIMO 500 |
                        ''  <10,001 - ++] |  5%  DE LA DT     |
                        ''-------------------------------------
                    Else
                        Dim DeudaTotal As Decimal = 0

                        If cboMonedaCompromisoMantemiento.Text = "DOLARES" Then
                            DeudaTotal = Decimal.Parse(txtDeudaTotalUSD.Text)
                        Else
                            DeudaTotal = Decimal.Parse(txtDeudaTotalSoles.Text)
                        End If

                        If DeudaTotal > 10000.0 Then
                            '' MONTO MAYOR O IGUAL AL 5% DE LA DEUDA TOTAL
                            If txtMontoCompromisoMantenimiento.Text >= Porcentaje_5(DeudaTotal) Then
                                ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")
                            Else
                                CtlMensajes1.Mensaje("RECUERDA: El monto del compromiso debe ser mayor o igual al 5% de la deuda total.")

                                ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")

                            End If
                        ElseIf DeudaTotal > 500.0 Then
                            '' MONTO MAYOR O IGUAL A 500
                            If txtMontoCompromisoMantenimiento.Text >= 500 Then
                                ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")
                            Else
                                CtlMensajes1.Mensaje("RECUERDA: El monto del compromiso debe ser como minimo S/.500.00.")

                                ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")

                            End If
                        ElseIf DeudaTotal > 1.0 Then
                            '' CANCELAR DEUDA TOTAL
                            If txtMontoCompromisoMantenimiento.Text = DeudaTotal Then
                                ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")
                            Else
                                CtlMensajes1.Mensaje("RECUERDA: El monto del compromiso debe ser igual a la deuda total.")

                                ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")

                            End If
                        End If
                    End If

                ElseIf idCartera = 109 Or idCartera = 110 Or idCartera = 112 Then

                    'VALIDAMOS SI ES UPDATE O INSERT DEPENDIENDO DEL PARAMETRO (QUE TENGA DATOS)
                    If lblId_Compromiso.Text <> "" Then
                        If PerfilUsuario <> "GESTOR" Then
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030A")
                        Else
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030")
                        End If

                        ''-----------------------------------------------------------------------------------------------------------------------
                        ''FUNCIONALIDAD ADICIONAL PARA AFP (DEVENGUES)
                        ''SE COBRARÁ COMO MÍNIMO EL 50% DE LA DEUDA TOTAL + 1
                    Else
                        imgCerrarCOmpromisoMantenimiento.Visible = False

                        Dtj.Clear()

                        Dim TraeCantidadDevengue As String
                        TraeCantidadDevengue = "select count(producto) as cantidad from Operacion where Estado not in ('E') and IdCliente = " & idcliente
                        Dtj = fnci.FunctionConsulta(TraeCantidadDevengue)
                        TotalDevengues = Dtj.Rows(0)("cantidad")

                        TotalCompromisosDevengues = TraerCompromisos()

                        If TotalDevengues = 0 Then
                            CtlMensajes1.Mensaje("Ocrurrio un problema con los devengues")
                            Exit Sub
                        ElseIf TotalDevengues = 1 Then
                            MinimoPagoDevengues = 1
                        ElseIf TotalDevengues = 2 Then
                            MinimoPagoDevengues = 2
                        Else
                            MinimoPagoDevengues = CInt(Math.Floor(TotalDevengues / 2)) + 1
                        End If

                        ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                            txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                            txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                            txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")

                        If TotalCompromisosDevengues < MinimoPagoDevengues Then

                            Dim MinimoPagoDevenguesMensaje As Integer = 0
                            MinimoPagoDevenguesMensaje = MinimoPagoDevengues - TotalCompromisosDevengues - 1

                            TotalCompromisosDevengues = TraerCompromisos()

                            If MinimoPagoDevenguesMensaje <> 0 Then

                                CtlMensajes1.Mensaje("Le falta registrar " + CStr(MinimoPagoDevenguesMensaje) + " devengue(s).")
                            Else
                                imgCerrarCOmpromisoMantenimiento.Visible = True
                            End If
                        Else
                            imgCerrarCOmpromisoMantenimiento.Visible = True
                        End If

                        CargaPanelCompromiso()
                    End If

                Else
                    'VALIDAMOS SI ES UPDATE O INSERT DEPENDIENDO DEL PARAMETRO (QUE TENGA DATOS)
                    If lblId_Compromiso.Text <> "" Then
                        If PerfilUsuario <> "GESTOR" Then
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030A")
                        Else
                            ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idcompromisoƒ:idPago▓" & _
                                                txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & _
                                                txtNroOperacionCompromisoManteniemito.Text & "ƒ" & cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & _
                                                txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & lblId_Compromiso.Text & "ƒ" & idPago, "SQL_N_GEST030")
                        End If
                    Else
                        ctl.FunctionGlobal(":fechageneroƒ:fechacompromisoƒ:montoƒ:monedaƒ:numoperacionƒ:tipopagoƒ:pagadoƒ:fechapagoƒ:montpagadoƒ:monedpagoƒ:idgestionƒ:idclienteƒ:idPago▓" & _
                                            txtFechaGeneroCompromisoMantenieminto.Text & "ƒ" & txtFechaCompromisoMantenimiento.Text & "ƒ" & txtMontoCompromisoMantenimiento.Text & "ƒ" & _
                                            IIf(cboMonedaCompromisoMantemiento.Value = "0", "S", cboMonedaCompromisoMantemiento.Value) & "ƒ" & txtNroOperacionCompromisoManteniemito.Text & "ƒ" & _
                                            cboTipoPagoCompromisoMantenimiento.Text & "ƒ" & pagado & "ƒ" & fechapago & "ƒ" & txtMontoCompromisoMantenimientoA.Text & "ƒ" & IIf(moneda = "0", "S", moneda) & "ƒ" & _
                                            id_gestion_activa & "ƒ" & idcliente & "ƒ" & idPago, "SQL_N_GEST035")
                    End If
                End If
            End If

            If idCartera = 110 Then

                TotalCompromisosDevengues = TraerCompromisos()

                If TotalCompromisosDevengues = MinimoPagoDevengues Then
                    CargaNombrePanel()
                    pnlCompromisos.Visible = True
                    'esta linea se retira porque el panel debe permitir adicionar mas registros.
                    'pnlCompromisoMantenimiento.Visible = False
                    CtlMensajes1.Mensaje("Usted terminó de registrar el mínimo de devengues requeridos (50% + 1).")
                    'CtlMensajes1.Mensaje("Se grabo correctamente" + Mensajenopago)
                Else
                    CtlMensajes1.Mensaje("Se grabo correctamente" + Mensajenopago)
                    'imgCerrarCOmpromisoMantenimiento.Visible = True
                End If
                CargarCompromiso()
            Else
                pnlCompromisos.Visible = True
                CargarCompromiso()
                pnlCompromisoMantenimiento.Visible = False
                CtlMensajes1.Mensaje("Se grabo correctamente" + Mensajenopago)
            End If

        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocrurrio un error al grabar compromiso")
            Crear_Cookies("Estado", "")
        End Try
    End Sub

    Public Function Porcentaje_5(ByVal cantidad As Decimal) As Decimal
        Return (cantidad * 5) / 100
    End Function

    Private Sub RDBPropuesta_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RDBPropuesta.CheckedChanged
        cboIndicador.Limpia()
        CboGestion.Limpia()
        CboContactabilidad.Limpia()
    End Sub

    Private Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click

        Dim FechaHora

        FechaHora = Now()

        Dim ctlsvc As New Controles.General
        Dim id_gestion As String = ""
        Dim dt_gestion As New DataTable
        'Dim dt_gestion_2 As New DataTable
        Dim ctl As New BL.Cobranza
        Dim ip As String = ""
        Dim ip_Altitude As String = ""

        Evento = Obtiene_Cookies("Estado")
        If Evento = "P" Then Exit Sub
        Crear_Cookies("Estado", "P")

        'If ImageButton4.AlternateText = "1" Then Exit Sub
        'ImageButton4.AlternateText = "1"
        Try
            If (Len(Trim(cboIndicador.Text)) = 0 Or Len(Trim(CboContactabilidad.Text)) = 0 Or Len(Trim(CboGestion.Text)) = 0) And RDBPropuesta.Checked = False Then
                CtlMensajes1.Mensaje("Falta seleccionar datos en la gestion")
            Else
                Dim dt_semaforo As DataTable = Nothing
                Dim codindicador As String
                Dim idindicador As String
                Dim semafono As String
                Dim propuesta As Boolean

                If RDBPropuesta.Checked = True Then
                    codindicador = "0"
                    idindicador = "0"
                Else
                    Dim array
                    array = Split(cboIndicador.Value, "?")
                    idindicador = array(0)
                    codindicador = array(1)
                    semafono = array(2)
                    If array(3) <> "N" Then
                        propuesta = True
                    Else
                        propuesta = False
                    End If

                    Dim tipo As String = ""
                    If RDBCall.Checked Then
                        tipo = "TELEFONIA"
                    ElseIf RDBCanalesAlternos.Checked Then
                        'tipo = "CARTA"
                        'REVISAR - SI GRABA PERO SALE UN ERROR
                        'tipo = CboGestion.Value
                        tipo = CboGestion.Text
                    ElseIf RDBPropuesta.Checked Then
                        tipo = "PROPUESTA"
                    End If
                    telefono_activo = NumTelf.Value
                    Dim canllamc = 0
                    Dim canllam = 0
                    Dim canllamm = 0

                    ip = IIf(Obtiene_Cookies("IpPredictivo") Is Nothing, "", Obtiene_Cookies("IpPredictivo"))
                    Dim respuesta = "NO ANSWER"

                    If ip = "" Then
                        respuesta = "CLICKTOCALL"
                    Else
                        respuesta = "PREDICTIVO"
                    End If


                    Dim Anexo = Obtiene_Cookies("Anexo")

                    'Dim IdAltitude = Obtiene_Cookies("IdAlt")
                    Dim IdAltitude = Request.QueryString("IdAlt")

                    'Dim IdCampana = Obtiene_Cookies("IdCamp")
                    Dim IdCampana = Request.QueryString("IdCamp")

                    Dim fecaill = Format(CDate("01/01/1900 00:00:00"), "dd/MM/yyyy hh:mm:ss")
                    Dim fecafll = Format(CDate("01/01/1900 00:00:00"), "dd/MM/yyyy hh:mm:ss")
                    Dim fecais = Format(CDate("01/01/1900 00:00:00"), "dd/MM/yyyy hh:mm:ss")
                    Dim fechasistema

                    fechasistema = Now()

                    Dim Diferencia
                    If Len(HoraClick.Value) > 6 Then
                        Diferencia = -1 * DateDiff(DateInterval.Second, CDate(HoraClick.Value), Now())
                        FechaHora = Replace(Replace(DateAdd(DateInterval.Second, Diferencia, CDate(fechasistema)), ". ", ""), ".", "")
                    Else
                        Diferencia = 0
                        FechaHora = fechasistema
                    End If

                    If CodTelefono.Value Is Nothing Then
                        CodTelefono.Value = 0
                    ElseIf Trim(CodTelefono.Value) = "" Then
                        CodTelefono.Value = 0
                    End If

                    Crear_Cookies("idPago", "0")

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    '''''''''''''''' OJO!! ESTA PARTE CORRESPONDE A LA IMPLEMEMTACION DEL ALTITUDE EXCLUSIVAMENTE PARA BCP '''''''''''''''''
                    ''''' COMO EL PARAMETRO QUE VIAJA SE LLAMARA "Altitude" ESE SERA EL UNICO FILTRO PARA GRABAR EN LA TABLA "Gestion" '''''
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ip_Altitude = Request.QueryString("IP")

                    If ip_Altitude = "Altitude" Then
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        ''''' ESTE INSERT ES PARA GRABAR EL "IdAlt" Y EL "IdCamp", COLUMNAS NUEVAS QUE SE ADICIONARON A LA TABLA GESTION ''''
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        dt_gestion = ctl.FunctionGlobal(":gestionƒ:tipoƒ:detallegestionƒ:idusuarioƒ:idclienteƒ:codindicadorƒ:idindicadorƒ:telefonoƒ:DNIƒ:idcarteraƒ:anexoƒ:fingcallsisƒ:fmarccallastƒ:fllamcallastƒ:ffincallastƒ:ffincallsisƒ:durarƒ:durallƒ:duracƒ:respastƒ:idtelefonoƒ:idaltitudeƒ:idcampana▓" & _
                                    txtGestion.Text & "ƒ" & tipo & "ƒ" & CboGestion.Text & "ƒ" & Hidusuario.Value & "ƒ" & idcliente & "ƒ" & codindicador & "ƒ" & idindicador & "ƒ" & telefono_activo & "ƒ" & Dni_Activo & "ƒ" & idCartera & "ƒ" & Anexo & _
                                    "ƒ" & Format(CDate(FechaHora), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fecais), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fecaill), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fecafll), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fechasistema), "dd/MM/yyyy HH:mm:ss") & "ƒ" & canllamm & "ƒ" & canllam & "ƒ" & canllamc & "ƒ" & respuesta & "ƒ" & _
                                    CodTelefono.Value & "ƒ" & IdAltitude & "ƒ" & IdCampana, "SQL_N_GEST062")

                    Else
                        dt_gestion = ctl.FunctionGlobal(":gestionƒ:tipoƒ:detallegestionƒ:idusuarioƒ:idclienteƒ:codindicadorƒ:idindicadorƒ:telefonoƒ:DNIƒ:idcarteraƒ:anexoƒ:fingcallsisƒ:fmarccallastƒ:fllamcallastƒ:ffincallastƒ:ffincallsisƒ:durarƒ:durallƒ:duracƒ:respastƒ:idtelefono▓" & _
                                    txtGestion.Text & "ƒ" & tipo & "ƒ" & CboGestion.Text & "ƒ" & Hidusuario.Value & "ƒ" & idcliente & "ƒ" & codindicador & "ƒ" & idindicador & "ƒ" & telefono_activo & "ƒ" & Dni_Activo & "ƒ" & idCartera & "ƒ" & Anexo & _
                                    "ƒ" & Format(CDate(FechaHora), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fecais), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fecaill), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fecafll), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fechasistema), "dd/MM/yyyy HH:mm:ss") & "ƒ" & canllamm & "ƒ" & canllam & "ƒ" & canllamc & "ƒ" & respuesta & "ƒ" & _
                                    CodTelefono.Value, "SQL_N_GEST015")
                    End If

                    If dt_gestion.Rows.Count > 0 Then
                        id_gestion = dt_gestion.Rows(0)(0).ToString

                        If idCartera = 30 Then
                            lblId_Gestion_PLM.Text = id_gestion
                        End If

                        Crear_Cookies("idgestion", id_gestion)

                        If RDBCanalesAlternos.Checked Then
                            dt_semaforo = ctl.FunctionGlobal(":idEmpresaƒ:CodIndicadorƒ:TipoIndicador▓" & idEmpresa & "ƒ" & codindicador & "ƒCARTA", "QRYSC001")
                        Else
                            dt_semaforo = ctl.FunctionGlobal(":idEmpresaƒ:CodIndicadorƒ:TipoIndicador▓" & idEmpresa & "ƒ" & codindicador & "ƒ" & tipo, "QRYSC001")
                        End If

                    End If

                    'If RDBCall.Checked Then
                    If RDBCall.Checked Or RDBCanalesAlternos.Checked Then
                        Dim SemaForoAtual As String = ""
                        Dim dtTelf = CtlTelefono.GV
                        For i = 0 To dtTelf.Rows.Count - 1
                            If dtTelf.Rows(i).Cells(7).Text.Trim = telefono_activo.ToString.Trim Then
                                SemaForoAtual = dtTelf.Rows(i).Cells(12).Text.ToString.Trim
                                Exit For
                            End If
                        Next
                        Dim dtIndicadorVal As New DataTable
                        If NumTelf.Value = Nothing Then
                        Else
                            Dim NewDNI As String = ""
                            'Dim NewDNI As String = "1" + Dni_Activo
                            Dim val_dni As Boolean



                            'Obtenemos la prioridad con dni de gestion y numero telefono
                            dtIndicadorVal = conexion.Obtiene_dt("SELECT Prioridad FROM Telefonos where dni ='" & Dni_Activo & "'  and telefono ='" & NumTelf.Value & "'", 0)

                            'validamos si obtiene registros es mayor a 0  
                            If dtIndicadorVal.Rows.Count > 0 Then
                                val_dni = True
                            Else
                                If Len(Dni_Activo) = 9 And Mid(Trim(Dni_Activo), 1, 1) = "1" Then
                                    'modificamos dni le quitamos el 1 delante
                                    NewDNI = Dni_Activo.Substring(1, Dni_Activo.Length - 1)
                                    dtIndicadorVal = conexion.Obtiene_dt("SELECT Prioridad FROM Telefonos where dni ='" & NewDNI & "'  and telefono ='" & NumTelf.Value & "'", 0)
                                    val_dni = False
                                ElseIf Len(Dni_Activo) = 8 Then
                                    'añadimos
                                    NewDNI = "1" + Dni_Activo
                                    dtIndicadorVal = conexion.Obtiene_dt("SELECT Prioridad FROM Telefonos where dni ='" & NewDNI & "'  and telefono ='" & NumTelf.Value & "'", 0)
                                    val_dni = False
                                End If

                            End If

                            dtTelf = Nothing
                            Select Case dt_semaforo(0)("indSemaforo")
                                Case "P"

                                Case "R"
                                    If dtIndicadorVal(0)("Prioridad").ToString <> "R" Then
                                        'si es verdadero actualizo con el dni normal, sino con el dni editado
                                        If val_dni Then
                                            'NO ES LA SOLUCION MAS OPTIMA (ESTE CAMBIO ES MOMENTANEO)
                                            ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'R' where  DNI ='" & Dni_Activo & "' and telefono = " & NumTelf.Value)
                                        Else
                                            ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'R' where  DNI ='" & NewDNI & "' and telefono = " & NumTelf.Value)
                                        End If
                                        'NO ES LA SOLUCION MAS OPTIMA (ESTE CAMBIO ES MOMENTANEO)
                                        'ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'R' where  DNI ='" & Dni_Activo & "' and telefono = " & NumTelf.Value)
                                        'ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'R' where  DNI ='" & NewDNI & "' and telefono = " & NumTelf.Value)

                                        'If Mid(Trim(Val(telefono_activo)), 1, 1) = "9" Or Mid(Trim(Val(telefono_activo)), 1, 1) = "8" Then
                                        '    ctl.FunctionEjecuta("Update DatosCliente " & _
                                        '            " set celular= isnull((select top 1 telefono " & _
                                        '                  " From TelefonoS " & _
                                        '                 " where Prioridad='þ' " & _
                                        '                  " and tiptelf ='C' and estado <> 'E' " & _
                                        '                  " AND TelefonoS.idcliente= DatosCliente.idcliente " & _
                                        '                  " order by fecha_actualizacion desc),0) " & _
                                        '            " where idcliente =" & idcliente)
                                        'Else
                                        '    ctl.FunctionEjecuta("Update DatosCliente " & _
                                        '        " set telefono= isnull((select top 1 telefono " & _
                                        '              " From TelefonoS " & _
                                        '             " where Prioridad='þ' " & _
                                        '              " and tiptelf <>'C' and estado <> 'E' " & _
                                        '              " AND TelefonoS.idcliente= DatosCliente.idcliente " & _
                                        '              " order by fecha_actualizacion desc),0) " & _
                                        '        " where idcliente =" & idcliente)
                                        'End If

                                    End If
                                Case "A"
                                    If dtIndicadorVal(0)("Prioridad").ToString <> "A" And dtIndicadorVal(0)("Prioridad").ToString <> "V" And dtIndicadorVal(0)("Prioridad").ToString <> "R" Then
                                        If val_dni Then
                                            ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'A' where  DNI ='" & Dni_Activo & "' and telefono = " & Trim(Val(NumTelf.Value)))
                                        Else
                                            ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'A' where  DNI ='" & NewDNI & "' and telefono = " & Trim(Val(NumTelf.Value)))
                                        End If
                                        'NO ES LA SOLUCION MAS OPTIMA (ESTE CAMBIO ES MOMENTANEO)
                                        'ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'A' where  DNI ='" & Dni_Activo & "' and telefono = " & Trim(Val(NumTelf.Value)))
                                        'ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'A' where  DNI ='" & NewDNI & "' and telefono = " & Trim(Val(NumTelf.Value)))

                                    End If
                                Case "V"
                                    If dtIndicadorVal(0)("Prioridad").ToString <> "V" Then
                                        If val_dni Then
                                            ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'V' where DNI ='" & Dni_Activo & "' and telefono = " & Trim(Val(NumTelf.Value)))
                                        Else
                                            ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'V' where DNI ='" & NewDNI & "' and telefono = " & Trim(Val(NumTelf.Value)))
                                        End If
                                        'NO ES LA SOLUCION MAS OPTIMA (ESTE CAMBIO ES MOMENTANEO)
                                        'ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'V' where DNI ='" & Dni_Activo & "' and telefono = " & Trim(Val(NumTelf.Value)))
                                        'ctl.FunctionEjecuta("update telefonos set fecha_actualizacion = getdate(), prioridad = 'V' where DNI ='" & NewDNI & "' and telefono = " & Trim(Val(NumTelf.Value)))

                                        'If Mid(Trim(Val(telefono_activo)), 1, 1) = "9" Or Mid(Trim(Val(telefono_activo)), 1, 1) = "8" Then
                                        '    ctl.FunctionEjecuta("Update DatosCliente " & _
                                        '            " set celular= " & telefono_activo & _
                                        '            " where idcliente =" & idcliente)
                                        'Else
                                        '    ctl.FunctionEjecuta("Update DatosCliente " & _
                                        '        " set telefono= " & telefono_activo & _
                                        '        " where idcliente =" & idcliente)
                                        'End If
                                    End If
                                Case Else

                            End Select
                        End If
                    End If

                    If dt_semaforo(0)("indPromesa") = "S" Then
                        pnlCompromisoMantenimiento.Visible = True
                        txtFechaGeneroCompromisoMantenieminto.Text = Date.Now.ToString("dd/MM/yyyy")
                        txtFechaCompromisoMantenimiento.Text = Date.Now.ToString("dd/MM/yyyy")
                        txtMontoCompromisoMantenimiento.Text = ""
                        chkPagadoCompromisoMantenimiento.Checked = False
                        txtMontoCompromisoMantenimientoA.Text = ""
                        Hidgestion.Value = id_gestion
                        id_gestion_activa = Hidgestion.Value
                        Crear_Cookies("idgestion", id_gestion)
                        cboMonedaCompromisoMantemiento.Limpia()
                        cboMonedaCompromisoMantemiento.Condicion = ":idtabla▓105"
                        cboMonedaCompromisoMantemiento.Cargar_dll()
                        cboMonedaCompromisoMantenimientoA.Limpia()
                        cboMonedaCompromisoMantenimientoA.Condicion = ":idtabla▓105"
                        cboMonedaCompromisoMantenimientoA.Cargar_dll()
                        cboTipoPagoCompromisoMantenimiento.Limpia()
                        cboTipoPagoCompromisoMantenimiento.Condicion = ":idtabla▓104"
                        cboTipoPagoCompromisoMantenimiento.Cargar_dll()
                        lblId_Compromiso.Text = ""
                        Dim ctls As New Controles.General
                        If RDBCall.Checked = True Then
                            'Call ctls.Gestion_Telefono_grilla(Dni_Activo, idEmpresa, CtlGestionTelefonica)
                            Call ctls.Gestion_Telefono_grilla_Positivas(Dni_Activo, idEmpresa, CtlGestTelefPositivas)
                            Call ctls.Gestion_Telefono_grilla_Negativas(Dni_Activo, idEmpresa, CtlGestTelefNegativas)
                        ElseIf RDBCanalesAlternos.Checked = True Then
                            Call ctls.Gestion_Campo_grilla(Dni_Activo, idEmpresa, CtlGestionCampo)
                        End If
                        'Limpiar_Gestion()
                        'CargarTelefonos(Dni_Activo)
                        CargarAllTelefonos_DNI_Unificado_Parte1(Dni_Activo)
                        gvCompromisos.OpocionNuevo = True
                    Else
                        Dim ctls As New Controles.General
                        If RDBCall.Checked = True Then
                            'Call ctls.Gestion_Telefono_grilla(Dni_Activo, idEmpresa, CtlGestionTelefonica)
                            Call ctls.Gestion_Telefono_grilla_Positivas(Dni_Activo, idEmpresa, CtlGestTelefPositivas)
                            Call ctls.Gestion_Telefono_grilla_Negativas(Dni_Activo, idEmpresa, CtlGestTelefNegativas)
                        ElseIf RDBCanalesAlternos.Checked = True Then
                            Call ctls.Gestion_Campo_grilla(Dni_Activo, idEmpresa, CtlGestionCampo)
                        End If
                        'Limpiar_Gestion()
                        'CargarTelefonos(Dni_Activo)
                        CargarAllTelefonos_DNI_Unificado_Parte1(Dni_Activo)
                        CtlMensajes1.Mensaje("Se ha grabado la gestion satisfactoriamente")
                        Dim dt = ctls.Obtiene_Consulta(":idusuario▓" & Obtiene_Cookies("idusuario"), "GES008")
                        If Not dt Is Nothing Then
                            For i = 0 To dt.rows.count - 1
                                Crear_Cookies("ppDNI", dt.ROWS(i)("dni"))
                                Crear_Cookies("ppidcartera", dt.ROWS(i)("idcartera"))
                                Crear_Cookies("ppidcliente", dt.ROWS(i)("idcliente"))
                                Crear_Cookies("ppidagenda", dt.ROWS(i)("idagenda"))
                                'txtAnotacionMensaje.BackColor = Drawing.Color.Yellow
                                txtAnotacionMensaje.BackColor = System.Drawing.Color.Yellow
                                txtAnotacionMensaje.Text = "!ALERTA...! TIENES UNA PROXIMA GESTION PENDIENTE" & vbCrLf
                                txtAnotacionMensaje.Text &= "================================================" & vbCrLf
                                txtAnotacionMensaje.Text &= "Fecha y Hora : " & dt.rows(i)(2) & vbCrLf
                                txtAnotacionMensaje.Text &= "Mensaje      : " & dt.rows(i)("anotacion")
                                PnlMensajeAlerta.Visible = True
                                Exit For
                            Next
                        End If
                    End If
                End If
            End If

            NumTelf.Value = ""
            btnAvanzar.Visible = True

            'REVISAR
            pnlmarcatelf.Visible = False

            Dim ArrInd = Split(cboIndicador.Value, "?")
            Call Crear_Cookies("TipoContactoPredictivo", ArrInd(4))

            If idCartera = 109 Or idCartera = 110 Or idCartera = 112 Then
                If cboIndicador.Value.ToString() = "614?44?V?S?CEF" Then
                    MostrarLiquidacion()
                End If
            End If

            ' ''PRUEBA DE HABILITAR BOTON CUANDO YA GRABO REGISTRO
            'CtlMensajes1.Mensaje("El registro se grabo satisfactoriamente.")
            ''ImageButton4.Enabled = False
            'ImageButton4.Visible = True

            Limpiar_Gestion()

        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al grabar" & ex.Message)
            If Not dt_gestion Is Nothing Then
                If dt_gestion.Rows.Count > 0 Then
                    id_gestion = dt_gestion.Rows(0)(0).ToString
                End If
            End If
            'ImageButton4.Enabled = True
            Crear_Cookies("Estado", "")
        End Try
    End Sub

    'Sub grabarDataTable(ByVal tipo As String, ByVal codindicador As String, ByVal idindicador As String, ByVal Anexo As String, ByVal FechaHora As String, ByVal fecais As String, ByVal fecaill As String, ByVal fecafll As String, ByVal fechasistema As String, ByVal canllamm As String, ByVal canllam As String, ByVal canllamc As String, ByVal respuesta As String)
    Function grabarDataTable(ByVal tipo As String, ByVal codindicador As String, ByVal idindicador As String, ByVal Anexo As String, ByVal FechaHora As String, ByVal fecais As String, ByVal fecaill As String, ByVal fecafll As String, ByVal fechasistema As String, ByVal canllamm As String, ByVal canllam As String, ByVal canllamc As String, ByVal respuesta As String)

        DataTableGestion.Columns.Add("IdGestion")
        DataTableGestion.Columns.Add("IdCliente")
        DataTableGestion.Columns.Add("IdUsuario")
        DataTableGestion.Columns.Add("Gestion")
        DataTableGestion.Columns.Add("Fecha")
        DataTableGestion.Columns.Add("Hora")
        DataTableGestion.Columns.Add("Estado")
        DataTableGestion.Columns.Add("TipoTel")
        DataTableGestion.Columns.Add("TipoOperador")
        DataTableGestion.Columns.Add("CodIndicador")
        DataTableGestion.Columns.Add("IdIndicador")
        DataTableGestion.Columns.Add("telefono")
        DataTableGestion.Columns.Add("DNI")
        DataTableGestion.Columns.Add("idCartera")
        DataTableGestion.Columns.Add("anexo")
        DataTableGestion.Columns.Add("fingcallsis")
        DataTableGestion.Columns.Add("fmarccallast")
        DataTableGestion.Columns.Add("fllamcallast")
        DataTableGestion.Columns.Add("ffincallast")
        DataTableGestion.Columns.Add("ffincallsis")
        DataTableGestion.Columns.Add("durar")
        DataTableGestion.Columns.Add("durall")
        DataTableGestion.Columns.Add("durac")
        DataTableGestion.Columns.Add("respast")
        DataTableGestion.Columns.Add("idtelefono")
        DataTableGestion.Columns.Add("anio")
        DataTableGestion.Columns.Add("mes")
        DataTableGestion.Columns.Add("IdAltitude")
        DataTableGestion.Columns.Add("IdCampana")

        Dim row As DataRow = DataTableGestion.NewRow

        row("IdGestion") = "automatico"
        row("IdCliente") = idcliente
        row("IdUsuario") = Hidusuario.Value
        row("Gestion") = txtGestion.Text
        row("Fecha") = Format(CDate(FechaHora), "dd/MM/yyyy HH:mm:ss")
        row("Hora") = Format(CDate(FechaHora), "dd/MM/yyyy HH:mm:ss")
        row("Estado") = ""
        row("TipoTel") = tipo
        row("TipoOperador") = CboGestion.Text
        row("CodIndicador") = codindicador
        row("IdIndicador") = idindicador
        row("telefono") = telefono_activo  'revisar este parametro
        row("DNI") = Dni_Activo
        row("idCartera") = idCartera
        row("anexo") = Anexo
        row("fingcallsis") = Format(CDate(FechaHora), "dd/MM/yyyy HH:mm:ss")
        row("fmarccallast") = Format(CDate(fecais), "dd/MM/yyyy HH:mm:ss")
        row("fllamcallast") = Format(CDate(fecaill), "dd/MM/yyyy HH:mm:ss")
        row("ffincallast") = Format(CDate(fecafll), "dd/MM/yyyy HH:mm:ss")
        row("ffincallsis") = Format(CDate(fechasistema), "dd/MM/yyyy HH:mm:ss")
        row("durar") = canllamm
        row("durall") = canllam
        row("durac") = canllamc
        row("respast") = respuesta
        row("idtelefono") = CodTelefono.Value
        row("anio") = ""
        row("mes") = ""
        row("IdAltitude") = ""
        row("IdCampana") = ""

        DataTableGestion.Rows.Add(row)

        '(":gestionƒ:            tipoƒ:       detallegestionƒ:       idusuarioƒ:               idclienteƒ:       codindicadorƒ:       idindicadorƒ:     telefonoƒ:                DNIƒ:              idcarteraƒ:       anexoƒ         :fingcallsisƒ:                                          fmarccallastƒ:                                       fllamcallastƒ:                                        ffincallastƒ:                                         ffincallsisƒ:                                              durarƒ:          durallƒ:        duracƒ:          respastƒ:           idtelefono▓" & _
        'txtGestion.Text & "ƒ" & tipo & "ƒ" & CboGestion.Text & "ƒ" & Hidusuario.Value & "ƒ" & idcliente & "ƒ" & codindicador & "ƒ" & idindicador & "ƒ"
        '& telefono_activo & "ƒ" & Dni_Activo & "ƒ" & idCartera & "ƒ" & Anexo &  "ƒ" & Format(CDate(FechaHora), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fecais), "dd/MM/yyyy HH:mm:ss") 
        '& "ƒ" & Format(CDate(fecaill), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fecafll), "dd/MM/yyyy HH:mm:ss") & "ƒ" & Format(CDate(fechasistema), "dd/MM/yyyy HH:mm:ss")
        '& "ƒ" & canllamm & "ƒ" & canllam & "ƒ" & canllamc & "ƒ" & respuesta & "ƒ" &   CodTelefono.Value, "SQL_N_GEST015")
        Return DataTableGestion
    End Function

    'Private Sub ForzarPostback()
    '    ScriptManager.RegisterStartupScript(Me, GetType(Page), "jsKeys", "javascript:Forzar();", True)
    '    'ClientScript.RegisterClientScriptBlock(GetType(Page), "jskeys", "javascript:Forzar();", True)
    'End Sub

    Sub MostrarLiquidacion()

        'LLAMAR AL PANEL
        PnlMostrarLiquidacion.Visible = True

        Dim ctl As New BL.Cobranza
        Dim dt As New DataTable
        Dim criterio As String = ""

        criterio = criterio & " RUC = '" & Dni_Activo & "'"

        Try
            dt = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_N_GEST059")
            gvMostrarLiquidacion.SourceDataTable = dt

        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al cargar la liquidacion.")
        End Try

    End Sub

    Sub Limpiar_Gestion()
        'CboGestion.Text = " "
        CboGestion.Limpia()
        'CboContactabilidad.Text = " "
        CboContactabilidad.Limpia()
        'cboIndicador.Value = " "
        cboIndicador.Limpia()
        RDBCall.Checked = False
        RDBCanalesAlternos.Checked = False
        RDBPropuesta.Checked = False
        txtGestion.Text = ""
    End Sub

    Public Property id_gestion_activa() As String
        Get
            Return lblId_Gestion.Text
        End Get
        Set(ByVal value As String)
            lblId_Gestion.Text = value
        End Set
    End Property

    Public Property id_gestion_plm() As String
        Get
            Return lblId_Gestion_PLM.Text
        End Get
        Set(ByVal value As String)
            lblId_Gestion_PLM.Text = value
        End Set
    End Property

    Public Property telefono_activo()
        Get
            Return lblTelefono_Activo.Text
        End Get
        Set(ByVal value)
            lblTelefono_Activo.Text = value
        End Set
    End Property

    Public Property Tipo_Mantenimiento_Telefono() As String
        Get
            Return lblTipoTelefonoMantenieminto.Text
        End Get
        Set(ByVal value As String)
            lblTipoTelefonoMantenieminto.Text = value
        End Set
    End Property

    Sub CargarComboTelefonoMantenimeinto()
        cboContactoTelefono.Procedimiento = "SQL_N_GEST080"
        cboContactoTelefono.Condicion = ":idtablaƒ:condicion▓121ƒAND SUBSTRING(IDElemento, 1, 2) =" & Format(Val(idEmpresa), "00")
        cboContactoTelefono.Limpia()
        cboContactoTelefono.Cargar_dll()
        cboViaTelefono.Limpia()
        cboViaTelefono.Cargar_dll()
    End Sub

    Private Sub CtlTelefono_elegirfila(ByVal sender As Object, ByVal e As System.EventArgs, ByVal row As System.Web.UI.WebControls.GridViewRow) Handles CtlTelefono.elegirfila
        telefono_activo = row.Cells(5).Text
    End Sub

    Private Sub CtlTelefono_Nuevo() Handles CtlTelefono.Nuevo
        CargarComboTelefonoMantenimeinto()
        pnlMantenimeintoTelefono.Visible = True
        Tipo_Mantenimiento_Telefono = "N"
        txtNumeroTelefono.Text = ""
        txtOrigenTelefono.Text = ""
    End Sub

    Private Sub CtlTelefono_RowDeleting(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles CtlTelefono.RowDeleting
        Dim ctl As New BL.Cobranza
        Dim row As GridViewRow = GV.Rows(e.RowIndex)
        Numero_ant = row.Cells(7).Text
        txtNumeroTelefono.Text = row.Cells(7).Text
        Try
            Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")
            If PerfilUsuario <> "GESTOR" Then
                ctl.FunctionGlobal(":dniƒ:telefono▓" & txtDNI.Text & "ƒ" & txtNumeroTelefono.Text, "SQL_N_GEST058")
                CargarTelefonos(txtDNI.Text)
                CtlMensajes1.Mensaje("El telefono ha sido eliminado con exito")
            Else
                CtlMensajes1.Mensaje("Error.. usted no tiene permiso para eliminar telefonos, consulte con su administrador")
            End If
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al eliminar el telefono, vuelva a intertarlo")
        End Try
    End Sub

    Private Sub CtlTelefono_RowEditing(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs, ByVal row As System.Web.UI.WebControls.GridViewRow) Handles CtlTelefono.RowEditing
        Try
            CargarComboTelefonoMantenimeinto()
            pnlMantenimeintoTelefono.Visible = True
            Numero_ant = row.Cells(7).Text
            txtNumeroTelefono.Text = row.Cells(7).Text
            cboContactoTelefono.Text = row.Cells(15).Text
            txtOrigenTelefono.Text = row.Cells(18).Text
            cboViaTelefono.Value = row.Cells(6).Text
        Catch ex As Exception
        End Try
    End Sub

    Private Sub CtlTelefono_Seleccionarfila(ByVal sender As Object, ByVal e As System.EventArgs, ByVal row As System.Web.UI.WebControls.GridViewRow) Handles CtlTelefono.Seleccionarfila
        telefono_activo = row.Cells(7).Text
        Tipo_Telefono()
    End Sub

    Private Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        Dim ctl As New BL.Cobranza
        Dim request As WebRequest
        Try
            ctl.FunctionGlobal(":idcarteraƒ:idcondicionƒ:situacionƒ:tipocontactoƒ:idcliente▓" & _
                               idCartera & "ƒ" & cbocint.Value & "ƒ" & txtsitua.Text & "ƒ" & cbotipoc.Text & "ƒ" & idcliente, "SQL_N_GEST036")
            If Not telefono_activo <> "" Then
                ctl.FunctionGlobal(":psesgestionƒ:pusuario▓" & 0 & "ƒ" & Husuario.Value, "SQLUSR002")
                If Obtiene_Cookies("Bolsa") = "N" Then

                    Response.Redirect("Grabo.htm")

                Else

                    CtlMensajes1.Mensaje("Usted ha grabado los indicadores del cliente de forma exitosa", "")

                End If
                Exit Sub
            End If
        Catch ex As Exception
        End Try
        'ocomentado por error con altituts
        'If Obtiene_Cookies("IPAgente") <> "" And Obtiene_Cookies("IPAgente") <> "altitude" And telefono_activo <> "" Then
        '    Dim contactabilidad As String = Obtiene_Cookies("TipoContactoPredictivo")
        '    Dim url As String = ""
        '    Dim ip = Obtiene_Cookies("IpUsada")
        '    If Trim(contactabilidad) = "CEF" Then
        '        url = "http://" & Obtiene_Cookies("IPAgente") & "/inactivacliente.php?id_cliente=" & idcliente
        '        request = WebRequest.Create(url)
        '        request.Credentials = CredentialCache.DefaultCredentials
        '    End If
        '    url = ""
        '    url = "http://" & Obtiene_Cookies("IPAgente") & "/despausaragente.php?agente=" & Husuario.Value
        '    Dim request1 As WebRequest
        '    request1 = WebRequest.Create(url)
        '    request1.Credentials = CredentialCache.DefaultCredentials
        '    url = "http://" & Obtiene_Cookies("IPAgente") & "/martinez/agente/inicio.php"
        '    Response.Redirect(url, False)
        'End If
    End Sub

    Sub Tipo_Telefono()
        If RDBCall.Checked = True Then
            If Left(NumTelf.Value, 1) = "9" Then
                CboGestion.Text = "CELULAR"
            Else
                CboGestion.Text = "TELEF. FIJO"
            End If
        End If
    End Sub

    Sub Selecciona_Contacto()
        If RDBCall.Checked = True Then
            CboContactabilidad.Text = "CONTACTO"
        End If
    End Sub

    Private Sub btnAceptarTelefono_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarTelefono.Click
        Try
            Dim ctl As New BL.Cobranza
            Dim tipotelefono As String

            lblmensajetel.Text = ""
            If Len(txtNumeroTelefono.Text) < 7 Then
                lblmensajetel.Text = "Telefono incorrecto verifique.."
                Exit Sub
            Else
                lblmensajetel.Text = ""
            End If

            Dim fnci As New BL.Cobranza
            Dim Dtj As New DataTable
            Dim valor_telef As Integer
            Dim xSQLw As String

            xSQLw = "select count(telefono) as tot from CONSULTA_VALIDA_TELEFONOS where telefono='" & txtNumeroTelefono.Text & "'"
            Dtj = fnci.FunctionConsulta(xSQLw)
            valor_telef = Dtj.Rows(0)("tot")

            If valor_telef > 0 Then
                CtlMensajes1.Mensaje("El numero telefonico ingresado ya se encuentra registrado ... ")
                Exit Sub
            End If

            If Tipo_Mantenimiento_Telefono = "N" Then
                If Left(Trim(txtNumeroTelefono.Text), 1) = "9" Then
                    tipotelefono = "C"
                Else
                    tipotelefono = "F"
                End If
                ctl.FunctionGlobal(":idclienteƒ:tipotelefonoƒ:viatelefonoƒ:telefonoƒ:prioridadƒ:contactoƒ:dniƒ:origen▓" & _
                                   idcliente & "ƒ" & tipotelefono & "ƒ" & Mid(cboViaTelefono.Text, 1, 1) & "ƒ" & txtNumeroTelefono.Text & "ƒ" & _
                                   "Pƒ" & cboContactoTelefono.Text & "ƒ" & Dni_Activo & "ƒ" & txtOrigenTelefono.Text, "SQL_N_GEST045")
                CtlMensajes1.Mensaje("Se grabo satisfactoriamente")
            Else
                If Left(Trim(txtNumeroTelefono.Text), 1) = "9" Then
                    tipotelefono = "C"
                Else
                    tipotelefono = "F"
                End If
                ctl.FunctionGlobal(":numero_nuevoƒ:contactoƒ:origenƒ:viatelefonoƒ:idclienteƒ:numero_antiguoƒ:dni▓" & txtNumeroTelefono.Text & "ƒ" & cboContactoTelefono.Text & "ƒ" & _
                                    txtOrigenTelefono.Text & "ƒ" & Mid(cboViaTelefono.Text, 1, 1) & "ƒ" & idcliente & "ƒ" & Numero_ant & "ƒ" & Dni_Activo, "SQL_N_GEST046")
                CtlMensajes1.Mensaje("Se grabo satisfactoriamente")
            End If

            pnlMantenimeintoTelefono.Visible = False
            CargarTelefonos(Dni_Activo)

        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al grabar")
        End Try
    End Sub

    Public Property Numero_ant() As String
        Get
            Return lblNumero_Activo.Text
        End Get
        Set(ByVal value As String)
            lblNumero_Activo.Text = value
        End Set
    End Property

    Private Sub btnCancelaTelefono_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelaTelefono.Click
        pnlMantenimeintoTelefono.Visible = False
    End Sub

    Private Sub imgBuscarPagos_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBuscarPagos.Click
        CargarPagos()
    End Sub

    Private Sub btnCerrarPropuesta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrarPropuesta.Click
        pnlPropuesta.Visible = False
        Crear_Cookies("Estado", "P")
    End Sub

    Private Sub gvPropuesta_Nuevo() Handles gvPropuesta.Nuevo
        pnlMantenimientoPropuesta.Visible = True
        CargarPropuestaMantenieminto()
        cboEstadoPropuesta.Text = "Pendiente"
        txtFechaGeneroPropuesta.Text = Date.Now.ToString("dd/MM/yyyy")
        txtFechaRespuesta.Text = Date.Now.ToString("dd/MM/yyyy")
        txtFechaPagoPropuesta.Text = Date.Now.ToString("dd/MM/yyyy")
        txtMontoPropuesta.Text = ""
        txtMontoPropuestaMantenimiento.Text = ""
        txtSustentoPropuesta.Text = ""
        txtNroPartesPropuesta.Text = ""
    End Sub

    Sub CargarPropuestaMantenieminto()
        cboEstadoPropuesta.Limpia()
        cboEstadoPropuesta.Cargar_dll()
        cboMonedaPropuesta.Limpia()
        cboMonedaPropuesta.Cargar_dll()
        cboMonedaPropuestaMantenimeinto.Limpia()
        cboMonedaPropuestaMantenimeinto.Cargar_dll()
        If TipoCartera = "ACTIVA" Then
            cboOperacionPropuesta.Procedimiento = "SQL_N_GEST034"
        Else
            cboOperacionPropuesta.Procedimiento = "SQL_N_GEST033"
        End If
        cboOperacionPropuesta.Condicion = ":idcliente▓" & idcliente
        cboOperacionPropuesta.Limpia()
        cboOperacionPropuesta.Cargar_dll()
    End Sub

    Sub cargarPropuesta()
        Dim ctl As New BL.Cobranza
        Dim dt As New DataTable
        Try
            dt = ctl.FunctionGlobal(":dni▓" & Dni_Activo, "SQL_N_GEST048")
            gvPropuesta.SourceDataTable = dt
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al cargar propuestas")
        End Try
    End Sub

    Private Sub imgPropuesta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPropuesta.Click
        pnlPropuesta.Visible = True
        cargarPropuesta()
    End Sub

    Private Sub imgCerrarPropuesta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCerrarPropuesta.Click
        pnlMantenimientoPropuesta.Visible = False
    End Sub

    Private Sub gvPropuesta_RowEditing(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs, ByVal row As System.Web.UI.WebControls.GridViewRow) Handles gvPropuesta.RowEditing
        'ESTA LINEA SOLO SE HABILITA PARA APLICACION PUBLICADA EN EL SERVIDOR 3.7
        'UpdatePropuesta(GV, row)
    End Sub

    Sub UpdatePropuesta(ByVal GV As System.Web.UI.WebControls.GridView, ByVal row As System.Web.UI.WebControls.GridViewRow)

        On_Off_Controles_Propuesta()

        CargarPropuestaMantenieminto()

        Id_Propuesta_Activa = row.Cells(4).Text

        'txtFechaGeneroPropuesta.Text = Left(row.Cells(13).Text, 10)
        txtFechaGeneroPropuesta.Text = Left(row.Cells(8).Text, 10)

        cboEstadoPropuesta.Text = row.Cells(15).Text
        cboOperacionPropuesta.Value = row.Cells(7).Text

        'txtNroPartesPropuesta.Text = row.Cells(14).Text
        txtNroPartesPropuesta.Text = row.Cells(9).Text

        'txtMontoPropuesta.Text = row.Cells(9).Text
        txtMontoPropuesta.Text = row.Cells(11).Text

        'cboMonedaPropuesta.Text = row.Cells(8).Text
        If row.Cells(10).Text = "S" Then
            cboMonedaPropuesta.Text = "SOLES"
        ElseIf row.Cells(10).Text = "D" Then
            cboMonedaPropuesta.Text = "DOLARES"
        End If


        'txtSustentoPropuesta.Text = row.Cells(12).Text
        'txtSustentoPropuesta.Text = Convert.ToInt32(row.Cells(12).Text.ToString())

        'ASI SE DEBE DE RECIBIR EN EL TEXTBOX PARA QUE LA "Ñ" NO SE VEA AFECTADA Y NO GENERE ERROR
        txtSustentoPropuesta.Text = HttpUtility.HtmlDecode(row.Cells(12).Text)

        'txtFechaPagoPropuesta.Text = Left(row.Cells(21).Text, 10)
        'txtFechaRespuesta.Text = Left(row.Cells(17).Text, 10)

        'If txtFechaPagoPropuesta.Text <> "" Then
        '    chkPagoPropuesta.Checked = True
        '    txtMontoPropuestaMantenimiento.Text = row.Cells(22).Text
        '    cboMonedaPropuestaMantenimeinto.Text = row.Cells(23).Text
        'End If
        ''MsgBox (Id_Propuesta_Activa)
    End Sub

    Function ColocarTelefono(ByVal telef As String) As String
        telefono_activo = telef
        Return ""
    End Function

    Private Sub CtlDireccion_Nuevo() Handles CtlDireccion.Nuevo
        pnlMantenimientoDireccion.Visible = True
        tipo_direccion = "N"
        CargarCombosDireccion()
    End Sub

    Sub CargarCombosDireccion()
        With cboDepartamentoMantenimietoDireccion
            .Limpia()
            .Cargar_dll()
        End With
        With cboTipoMantenimietoDireccion
            .Limpia()
            .Cargar_dll()
        End With
        cbotipoDireccion1.Limpia()
        cbotipoDireccion1.Procedimiento = "SQL_N_GEST006"
        cbotipoDireccion1.Condicion = ":idtabla▓125"
        cbotipoDireccion1.Cargar_dll()
        cboCondVivienda.Limpia()
        cboCondVivienda.Procedimiento = "SQL_N_GEST006"
        cboCondVivienda.Condicion = ":idtabla▓126"
        cboCondVivienda.Cargar_dll()
    End Sub

    Private Sub cboDepartamentoMantenimietoDireccion_Click() Handles cboDepartamentoMantenimietoDireccion.Click
        With cboProvinciaMantenimietoDireccion
            .Limpia()
            .Condicion = ":cod▓" & cboDepartamentoMantenimietoDireccion.Value
            .Cargar_dll()
        End With
    End Sub

    Private Sub cboProvinciaMantenimietoDireccion_Click() Handles cboProvinciaMantenimietoDireccion.Click
        With cboDistritoMantenimientoDireccion
            .Limpia()
            .Condicion = ":cod▓" & cboProvinciaMantenimietoDireccion.Value
            .Cargar_dll()
        End With
    End Sub

    Public Property id_direccion_activa() As String
        Get
            Return lblId_Direccion_Activa.Text
        End Get
        Set(ByVal value As String)
            lblId_Direccion_Activa.Text = value
        End Set
    End Property

    Public Property tipo_direccion() As String
        Get
            Return lblTipo_Mantenimiento_Direccion.Text
        End Get
        Set(ByVal value As String)
            lblTipo_Mantenimiento_Direccion.Text = value
        End Set
    End Property

    Private Sub btnAceptarMantenimietoDireccion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarMantenimietoDireccion.Click
        Try
            Dim ctl As New BL.Cobranza
            Dim Control As New Controles.General
            If tipo_direccion = "N" Then
                ctl.FunctionGlobal(":idclienteƒ:departamentoƒ:distritoƒ:provinciaƒ:direccionƒ:tipoƒ:TipoDireccionƒ:CondVivienda▓" & _
                          idcliente & "ƒ" & cboDepartamentoMantenimietoDireccion.Text & "ƒ" & cboDistritoMantenimientoDireccion.Text & "ƒ" & cboProvinciaMantenimietoDireccion.Text & "ƒ" & txtDireccionMantenimietoDireccion.Text & "ƒ" & cboTipoMantenimietoDireccion.Value & "ƒ" & cbotipoDireccion1.Value & "ƒ" & cboCondVivienda.Value, "SQL_N_GEST013")
            ElseIf tipo_direccion = "M" Then
                If id_direccion_activa <> "" Then
                    ctl.FunctionGlobal(":departamentoƒ:provinciaƒ:distritoƒ:direccionƒ:tipoƒ:iddireccionƒ:TipoDireccionƒ:CondVivienda▓" & _
                          cboDepartamentoMantenimietoDireccion.Text & "ƒ" & cboProvinciaMantenimietoDireccion.Text & "ƒ" & cboDistritoMantenimientoDireccion.Text & "ƒ" & txtDireccionMantenimietoDireccion.Text & "ƒ" & cboTipoMantenimietoDireccion.Value & "ƒ" & id_direccion_activa & "ƒ" & cbotipoDireccion1.Value & "ƒ" & cboCondVivienda.Value, "SQL_N_GEST011")
                Else
                    CtlMensajes1.Mensaje("No se selecciono una direccion, actualize y vuelva a intentarlo")
                End If
            End If
            pnlMantenimientoDireccion.Visible = False
            Call Control.Direccion_grilla(idcliente, CtlDireccion)
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al guardar la direccion, vuelva a intentarlo")
        End Try
    End Sub

    Private Sub CtlDireccion_RowDeleting(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles CtlDireccion.RowDeleting
        If GV.Rows(e.RowIndex).Cells(GV.Rows(e.RowIndex).Cells.Count - 1).Text <> "R" Then
            Dim ctl As New Controles.General
            Call ctl.eliimina_Direccion(Val(GV.Rows(e.RowIndex).Cells(4).Text))
            Call ctl.Direccion_grilla(Val(GV.Rows(e.RowIndex).Cells(5).Text), CtlDireccion)
            ctl = Nothing
            CtlMensajes1.Mensaje("La Direccion fue eliminado con exito", "")
        Else
            CtlMensajes1.Mensaje("La Direccion no puede ser eliminado por ser informacion reservada", "")
        End If
    End Sub

    Private Sub CtlDireccion_RowEditing(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs, ByVal row As System.Web.UI.WebControls.GridViewRow) Handles CtlDireccion.RowEditing
        Try
            pnlMantenimientoDireccion.Visible = True
            CargarCombosDireccion()
            id_direccion_activa = row.Cells(4).Text
            CtlMensajes1.Mensaje(row.Cells(4).Text)
            cboDepartamentoMantenimietoDireccion.Text = row.Cells(13).Text
            Call cboDepartamentoMantenimietoDireccion_Click()
            cboProvinciaMantenimietoDireccion.Text = row.Cells(12).Text
            Call cboProvinciaMantenimietoDireccion_Click()
            cboDistritoMantenimientoDireccion.Text = row.Cells(7).Text
            txtDireccionMantenimietoDireccion.Text = row.Cells(6).Text
            cboTipoMantenimietoDireccion.Value = row.Cells(16).Text
            cbotipoDireccion1.Limpia()
            cbotipoDireccion1.Procedimiento = "SQL_N_GEST006"
            cbotipoDireccion1.Condicion = ":idtabla▓125"
            cbotipoDireccion1.Cargar_dll()
            cbotipoDireccion1.Value = row.Cells(17).Text
            cboCondVivienda.Limpia()
            cboCondVivienda.Procedimiento = "SQL_N_GEST006"
            cboCondVivienda.Condicion = ":idtabla▓126"
            cboCondVivienda.Cargar_dll()
            cboCondVivienda.Value = row.Cells(18).Text
            tipo_direccion = "M"
        Catch ex As Exception
            CtlMensajes1.Mensaje("No se seleciono una direccion, vuelva a intentarlo")
        End Try
    End Sub

    Private Sub btnCancelarMantenimietoDireccion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelarMantenimietoDireccion.Click
        pnlMantenimientoDireccion.Visible = False
    End Sub

    Private Sub CtlAnotaciones_RowEditing(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs, ByVal row As System.Web.UI.WebControls.GridViewRow) Handles CtlAnotaciones.RowEditing
        pnlAnotaciones.Visible = True
        lblIndexAnotacion.Text = e.NewEditIndex
        txtAnotacionNueva.Text = GV.Rows(e.NewEditIndex).Cells(6).Text
    End Sub

    Private Sub btnAceptarAnotacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarAnotacion.Click
        Dim ctl As New BL.Cobranza
        Dim control As New Controles.General
        Try
            Dim dt As New DataTable
            dt = CtlAnotaciones.Datos
            Dim filanuevas As String = ""
            Dim ingreso_anotacion As String = ""
            filanuevas = Date.Now.ToString("dd/MM/yyyy hh:mm:ss") & "|@" & Husuario.Value & "|@" & txtAnotacionNueva.Text.Replace("|@", "") & "|@|@@"

            Dim arreglo As String()
            ReDim arreglo(3)

            For i = 0 To dt.Rows.Count - 1
                For y = 0 To dt.Columns.Count - 1
                    arreglo(i) = arreglo(i) & dt.Rows(i)(y) & "|@"
                Next
                arreglo(i) = arreglo(i) & "|@@"
            Next

            If lblIndexAnotacion.Text = "0" Then
                arreglo(0) = filanuevas
            ElseIf lblIndexAnotacion.Text = "1" Then
                arreglo(1) = filanuevas
            ElseIf lblIndexAnotacion.Text = "2" Then
                arreglo(2) = filanuevas
            End If

            For i = 0 To 2
                ingreso_anotacion = ingreso_anotacion & arreglo(i)
            Next
            'CtlMensajes1.Mensaje(ingreso_anotacion)
            ctl.FunctionGlobal(":idclienteƒ:mensaje▓" & idcliente & "ƒ" & ingreso_anotacion, "SQL_N_GEST049")
            CtlMensajes1.Mensaje("Se registro con exito")
            pnlAnotaciones.Visible = False
            Call control.Anotaciones_grilla(idcliente, CtlAnotaciones)
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error" & ex.Message)
            pnlAnotaciones.Visible = False
        End Try
    End Sub

    Private Sub btnCancelarAnotacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelarAnotacion.Click
        pnlAnotaciones.Visible = False
    End Sub

    Private Sub NumTelf_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NumTelf.ValueChanged
        If NumTelf.Value.Length > 3 Then
            RDBCall.Checked = True
        End If
    End Sub

    Public Function ActivarRaB() As String
        RDBCall.Checked = True
        Return ""
    End Function

    Private Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load
        If Me.Request.Params("__EVENTTARGET") = "ActivarRDB" Then
            'en este caso lanzamos un alert                     
            Crear_Cookies("IPAgente", "")
            ActivarRaB()

            pnlmarcatelf.Visible = True

            '''''''''''''''''''''''''''''''''''' Carga Agendas
            Mostrar_FieldSet_Agenda()
            Carga_Agenda_Cliente()
            '''''''''''''''''''''''''''''''''''' 

            lblNunmTelfC.Text = NumTelf.Value
            'ImageButton4.AlternateText = ""
            Crear_Cookies("Estado", "")
            carga_Call()

        ElseIf telefono_activo <> "" And Not Me.IsPostBack Then
            'en este caso lanzamos un alert            
            Crear_Cookies("IPAgente", Request.QueryString("IP"))
            NumTelf.Value = telefono_activo
            ActivarRaB()

            pnlmarcatelf.Visible = True

            '''''''''''''''''''''''''''''''''''' Carga Agendas
            Mostrar_FieldSet_Agenda()
            Carga_Agenda_Cliente()
            '''''''''''''''''''''''''''''''''''' 

            lblNunmTelfC.Text = NumTelf.Value
            'ImageButton4.AlternateText = ""
            Crear_Cookies("Estado", "")
            carga_Call()
        Else
            pnlmarcatelf.Visible = False
        End If

        Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")
        If PerfilUsuario <> "GESTOR" Then
            'CtlGestionTelefonica.Activa_Delete = True
            CtlGestTelefPositivas.Activa_Delete = True
            CtlGestTelefNegativas.Activa_Delete = True
            CtlGestionCampo.Activa_Delete = True
        End If

        Dim idPerfil = Obtiene_Cookies("idPerfil")
        If idPerfil = "7" Then
            'ImageButton4.Visible = False
            imgGrabarPropuesta.Visible = False
            btnAceptarTelefono.Visible = False
            gvCompromisos.Activa_Delete = False
            gvCompromisos.Activa_Edita = False
            gvConsultaPagos.Activa_Delete = False
            gvConsultaPagos.Activa_Edita = False
            gvConsultaPagos.OpocionNuevo = False
            'CtlGestionTelefonica.Activa_Delete = False
            CtlGestTelefPositivas.Activa_Delete = False
            CtlGestTelefNegativas.Activa_Delete = False
            CtlGestionCampo.Activa_Delete = False
        End If

        Refresh_Anotaciones()
        IpLlamada = ConfigurationManager.ConnectionStrings("IPServicio").ConnectionString

    End Sub

    Sub Refresh_Estadistica()
        For p = 0 To CtlAnotaciones.GV.Rows.Count - 1
            Dim e = CtlAnotaciones.GV.Rows(p)
            If UCase(e.Cells(4).Text) = "TOTAL" Then
                e.Cells(5).Text = "CANTIDAD"
                e.Cells(6).Text = "(%)COB"
                e.Cells(4).Width = "50"
                e.Cells(5).Width = "50"
                e.Cells(6).Width = "50"
                e.Cells(4).BackColor = System.Drawing.Color.Orange
                e.Cells(5).BackColor = System.Drawing.Color.Orange
                e.Cells(6).BackColor = System.Drawing.Color.Orange

            ElseIf UCase(e.Cells(4).Text) = "META" Then
                e.Cells(4).Text = "<center>META</center>"
                e.Cells(5).Text = "<center>PAGOS</center>"
                e.Cells(6).Text = "<center>(%)EFE</center>"
                e.Cells(4).BackColor = System.Drawing.Color.Orange
                e.Cells(5).BackColor = System.Drawing.Color.Orange
                e.Cells(6).BackColor = System.Drawing.Color.Orange
                e.Cells(4).BackColor = System.Drawing.Color.FromName("#565C54")
                e.Cells(5).BackColor = System.Drawing.Color.FromName("#565C54")
                e.Cells(6).BackColor = System.Drawing.Color.FromName("#565C54")
                e.Cells(4).ForeColor = System.Drawing.Color.White
                e.Cells(5).ForeColor = System.Drawing.Color.White
                e.Cells(6).ForeColor = System.Drawing.Color.White
                e.Cells(4).HorizontalAlign = HorizontalAlign.Center
                e.Cells(5).HorizontalAlign = HorizontalAlign.Center
                e.Cells(6).HorizontalAlign = HorizontalAlign.Center
            Else
                e.Cells(4).BackColor = System.Drawing.Color.White
                e.Cells(5).BackColor = System.Drawing.Color.White
                e.Cells(6).BackColor = System.Drawing.Color.White
                If e.RowIndex = 0 Then
                    Cobertura = e.Cells(6).Text
                ElseIf e.RowIndex = 2 Then
                    Efectividad = e.Cells(6).Text
                End If
                Dim strScript = "<script>drawGauge();</script>"
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "MyScript1", strScript, True)
                e.Cells(4).Text = "<center>" & e.Cells(4).Text & "<center>"
                e.Cells(5).Text = "<center>" & e.Cells(5).Text & "<center>"
                e.Cells(6).Text = "<center>" & e.Cells(6).Text & "<center>"
            End If
        Next
    End Sub

    Sub Refresh_Anotaciones()
        For p = 0 To CtlAnotaciones.GV.Rows.Count - 1
            Dim e = CtlAnotaciones.GV.Rows(p)
            If UCase(e.Cells(4).Text) = "FECHA" Then
                e.Cells(4).Width = "100"
                e.Cells(6).Width = "575"
                For c = 5 To e.Cells.Count - 1
                    e.Cells(c).Text = UCase(e.Cells(c).Text)
                Next
            Else
                For c = 4 To e.Cells.Count - 1
                    e.Cells(c).Style.Add("BORDER-BOTTOM", "#aaccee 1px solid")
                    e.Cells(c).Style.Add("BORDER-RIGHT", "#aaccee 1px solid")
                    e.Cells(c).Style.Add("padding-left", "1px")
                    'e.Cells(c).BackColor = Drawing.Color.FromName("#F9F496")
                    e.Cells(c).BackColor = System.Drawing.Color.FromName("#F9F496")
                    'e.Cells(c).ForeColor = Drawing.Color.Black
                    e.Cells(c).ForeColor = System.Drawing.Color.Black
                Next
            End If
        Next
    End Sub

    Private Sub gvConsultaPagos_Nuevo() Handles gvConsultaPagos.Nuevo
        pnlPagosMantenimiento.Visible = True
        CboMonedaPago.Limpia()
        CboMonedaPago.Condicion = ":idtabla▓105"
        CboMonedaPago.Cargar_dll()
        If TipoCartera = "ACTIVA" Then
            CboNroOperacioPago.Procedimiento = "SQL_N_GEST034"
        Else
            CboNroOperacioPago.Procedimiento = "SQL_N_GEST033"
        End If

        CboNroOperacioPago.Condicion = ":idcliente▓" & idcliente
        CboNroOperacioPago.Limpia()
        CboNroOperacioPago.Cargar_dll()

        cboConceptoPago.Limpia()
        cboConceptoPago.Condicion = ":idtabla▓123"
        cboConceptoPago.Cargar_dll()
        txtFechaPago.Text = Format(Now(), "dd/MM/yyyy")
        Crear_Cookies("Estado", "")
    End Sub

    Private Sub ImgGrabraPago_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgGrabraPago.Click
        Dim ctl As New BL.Cobranza

        Try
            Pagos = "P"
            '----
            If txtFechaPago.Text <> "" And CDate(txtFechaPago.Text) > Now.Date() Then
                CtlMensajes1.Mensaje("El pago no puede tener fecha futura", "")
                Exit Sub
            End If
            If txtFechaPago.Text = "" Then
                CtlMensajes1.Mensaje("La fecha de pago es obligatorio", "")
                Exit Sub
            End If
            If CboMonedaPago.Text = "" Then
                CtlMensajes1.Mensaje("La moneda del pago es obligatorio", "")
                Exit Sub
            End If
            If txtMontoPago.Text = "" Then
                CtlMensajes1.Mensaje("El importe del pago es obligatorio", "")
                Exit Sub
            End If
            If cboConceptoPago.Text = "" Then
                CtlMensajes1.Mensaje("El concepto de pago es obligatorio", "")
                Exit Sub
            End If


            ctl.FunctionGlobal(":pIdClienteƒ:pFechaPagoƒ:pmonedaƒ:pmontoƒ:pconceptoƒ:pnumoperacionƒ:pdniƒ:pidcartera▓" & idcliente & "ƒ" & Format(CDate(txtFechaPago.Text), "dd/MM/yyyy") & "ƒ" & Mid(CboMonedaPago.Text, 1, 1) & "ƒ" & txtMontoPago.Text & "ƒ" & cboConceptoPago.Text & "ƒ" & CboNroOperacioPago.Text & "ƒ" & Dni_Activo & "ƒ" & idCartera, "QRYCP005")

            pnlPagosMantenimiento.Visible = False
            CargarPagos()
            CtlMensajes1.Mensaje("Los pagos se crearon correctamente...!")
            Pagos = ""
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al grabar el pago, consulte o verufique el ingreso de su pago")
        End Try

    End Sub

    Private Sub chkPagadoCompromisoMantenimiento_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPagadoCompromisoMantenimiento.CheckedChanged
        txtFechaPagoCompromisoMantenimeinto.Enabled = True
        txtMontoCompromisoMantenimientoA.Enabled = True
        Crear_Cookies("Estado", "")
    End Sub

    Private Sub ImgCerrarPago_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgCerrarPago.Click
        pnlPagosMantenimiento.Visible = False
        Crear_Cookies("Estado", "")
    End Sub

    ''ELIMINAR REGISTRO DE PAGO - SOLO ADMINISTRADOR
    Private Sub gvConsultaPagos_RowDeleting(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvConsultaPagos.RowDeleting

        Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")

        'If PerfilUsuario <> "Gestor" Then
        If PerfilUsuario = "ADMINISTRADOR" Or PerfilUsuario = "SECTORISTA" Then
            Dim ctl As New BL.Cobranza
            ctl.FunctionGlobal(":idpago▓" & GV.Rows(e.RowIndex).Cells(12).Text, "QRYCP009")
            CtlMensajes1.Mensaje("El pago ha sido eliminado con éxito", "Mensaje")
            pnlPagos.Visible = True
            txtDNIPagos.Text = Dni_Activo
            txtClientePagos.Text = txtnombre.Text
            Id_Cliente_Pagos = idcliente
            cboCarteraPagos.Text = cbocartera.Text
            txtFechaInicioPagos.Text = "01/" & Date.Now.ToString("MM/yyyy")
            txtFechaFinPagos.Text = DateTime.DaysInMonth(Date.Now.Year, Date.Now.Month) & "/" & Date.Now.ToString("MM/yyyy")
            CargarPagos()
            Crear_Cookies("Estado", "")
        ElseIf PerfilUsuario = "SUPERVISOR" Then

            If Hidusuario.Value = 345 Or Hidusuario.Value = 350 Or Hidusuario.Value = 833 Or Hidusuario.Value = 1982 Then
                Dim ctl As New BL.Cobranza
                ctl.FunctionGlobal(":idpago▓" & GV.Rows(e.RowIndex).Cells(12).Text, "QRYCP009")
                CtlMensajes1.Mensaje("El pago ha sido eliminado con éxito", "Mensaje")
                pnlPagos.Visible = True
                txtDNIPagos.Text = Dni_Activo
                txtClientePagos.Text = txtnombre.Text
                Id_Cliente_Pagos = idcliente
                cboCarteraPagos.Text = cbocartera.Text
                txtFechaInicioPagos.Text = "01/" & Date.Now.ToString("MM/yyyy")
                txtFechaFinPagos.Text = DateTime.DaysInMonth(Date.Now.Year, Date.Now.Month) & "/" & Date.Now.ToString("MM/yyyy")
                CargarPagos()
                Crear_Cookies("Estado", "")
            Else
                CtlMensajes1.Mensaje("Usted no tiene permiso para eliminar este pago.", "Advertencia")
            End If
        Else
            CtlMensajes1.Mensaje("Usted no tiene permiso para eliminar este pago.", "Advertencia")
        End If
    End Sub

    ''EDITAR REGISTRO DE PAGO - SOLO ADMINISTRADOR
    Private Sub gvConsultaPagos_RowEditing1(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs, ByVal row As System.Web.UI.WebControls.GridViewRow) Handles gvConsultaPagos.RowEditing

        Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")

        If PerfilUsuario = "ADMINISTRADOR" Or PerfilUsuario = "SECTORISTA" Or PerfilUsuario = "SUPERVISOR" Then

            CtlMensajes1.Mensaje("Panel en construcción. Contactarse con el personal de TI.")

        Else
            CtlMensajes1.Mensaje("Usted no tiene permiso para editar este pago.", "Advertencia!!")
        End If

    End Sub

    Sub Crear_Cookies(ByVal id, ByVal valor)
        '// Creamos elemento HttpCookie con su nombre y su valor
        Response.Cookies.Remove(id)
        Dim addCookie As HttpCookie = New HttpCookie(id, valor)
        '// Si queremos le asignamos un fecha de expiración: mañana
        addCookie.Expires = DateTime.Today.AddDays(1)
        '// Y finalmente ñadimos la cookie a nuestro usuario
        Response.Cookies.Add(addCookie)
    End Sub

    'Private Sub CtlGestionTelefonica_RowDeleting(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles CtlGestionTelefonica.RowDeleting

    '    Dim ctl As New Controles.General
    '    Dim row As GridViewRow = GV.Rows(e.RowIndex)
    '    Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")

    '    'If PerfilUsuario <> "GESTOR" Then
    '    If PerfilUsuario = "ADMINISTRADOR" Then
    '        If ctl.Elimina_Gestion(row.Cells(0).Text) Then
    '            CtlMensajes1.Mensaje("La gestion ha sido eliminada.", "")
    '            'Call ctl.Gestion_Telefono_grilla(DNI, idEmpresa, CtlGestionTelefonica)
    '            Call ctl.Gestion_Telefono_grilla_Positivas(Dni_Activo, idEmpresa, CtlGestTelefPositivas)
    '            Call ctl.Gestion_Telefono_grilla_Negativas(Dni_Activo, idEmpresa, CtlGestTelefNegativas)
    '        Else
    '            CtlMensajes1.Mensaje("La gestion no ha sido eliminado. Contactarse con el personal de TI.", "Advertencia!!")
    '        End If
    '    Else
    '        CtlMensajes1.Mensaje("Usted no tiene permiso para eliminar gestiones.", "Advertencia!!")
    '    End If

    'End Sub

    Private Sub CtlGestTelefPositivas_RowDeleting(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles CtlGestTelefPositivas.RowDeleting

        Dim ctl As New Controles.General
        Dim row As GridViewRow = GV.Rows(e.RowIndex)
        Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")

        'If PerfilUsuario <> "GESTOR" Then
        If PerfilUsuario = "ADMINISTRADOR" Then
            If ctl.Elimina_Gestion(row.Cells(0).Text) Then
                CtlMensajes1.Mensaje("La gestion ha sido eliminada.", "")
                'Call ctl.Gestion_Telefono_grilla(DNI, idEmpresa, CtlGestionTelefonica)
                Call ctl.Gestion_Telefono_grilla_Positivas(Dni_Activo, idEmpresa, CtlGestTelefPositivas)
            Else
                CtlMensajes1.Mensaje("La gestion no ha sido eliminado. Contactarse con el personal de TI.", "Advertencia!!")
            End If
        Else
            CtlMensajes1.Mensaje("Usted no tiene permiso para eliminar gestiones.", "Advertencia!!")
        End If

    End Sub

    Private Sub CtlGestTelefNegativas_RowDeleting(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles CtlGestTelefNegativas.RowDeleting

        Dim ctl As New Controles.General
        Dim row As GridViewRow = GV.Rows(e.RowIndex)
        Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")

        'If PerfilUsuario <> "GESTOR" Then
        If PerfilUsuario = "ADMINISTRADOR" Then
            If ctl.Elimina_Gestion(row.Cells(0).Text) Then
                CtlMensajes1.Mensaje("La gestion ha sido eliminada.", "")
                'Call ctl.Gestion_Telefono_grilla(DNI, idEmpresa, CtlGestionTelefonica)
                Call ctl.Gestion_Telefono_grilla_Negativas(Dni_Activo, idEmpresa, CtlGestTelefNegativas)
            Else
                CtlMensajes1.Mensaje("La gestion no ha sido eliminado. Contactarse con el personal de TI.", "Advertencia!!")
            End If
        Else
            CtlMensajes1.Mensaje("Usted no tiene permiso para eliminar gestiones.", "Advertencia!!")
        End If

    End Sub

    Private Sub CtlGestionCampo_RowDeleting(ByVal GV As System.Web.UI.WebControls.GridView, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles CtlGestionCampo.RowDeleting

        Dim ctl As New Controles.General
        Dim row As GridViewRow = GV.Rows(e.RowIndex)
        Dim PerfilUsuario = Obtiene_Cookies("TipoUsuario")

        'If PerfilUsuario <> "GESTOR" Then
        If PerfilUsuario = "ADMINISTRADOR" Then
            If ctl.Elimina_Gestion(row.Cells(0).Text) Then
                CtlMensajes1.Mensaje("La gestion de campo ha sido eliminada.", "")
                'Call ctl.Gestion_Telefono_grilla(DNI, idEmpresa, CtlGestionTelefonica)
                Call ctl.Gestion_Telefono_grilla_Positivas(Dni_Activo, idEmpresa, CtlGestTelefPositivas)
                Call ctl.Gestion_Telefono_grilla_Negativas(Dni_Activo, idEmpresa, CtlGestTelefNegativas)
            Else
                CtlMensajes1.Mensaje("La gestion de campo no ha sido eliminado. Contactarse con el personal de TI.", "Advertencia!!")
            End If
        Else
            CtlMensajes1.Mensaje("Usted no tiene permiso para eliminar gestiones de campo.", "Advertencia!!")
        End If
    End Sub

    Protected Sub btnProximaAccion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnProximaAccion.Click
        PnlProximaAccion.Visible = True
        txtAnotacion.Text = ""
        CboHoraProxAcc.Value = ""
        CboMinutoProxAcc.Value = ""
        txtProximaAccion.Text = ""
    End Sub

    Private Sub btnAceptarFP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarFP.Click
        Dim ctl As New Controles.General
        ctl.Obtiene_Consulta(":idclienteƒ:fechaProxAccƒ:anotacionƒ:idusuario▓" & idcliente & "ƒ" & txtProximaAccion.Text & " " & CboHoraProxAcc.Text & ":" & CboMinutoProxAcc.Text & ":00ƒ" & _
                             txtAnotacion.Text & "ƒ" & Hidusuario.Value, "GES007")
        CtlMensajes1.Mensaje("Cliente Agendado...")
        PnlProximaAccion.Visible = False
    End Sub

    Private Sub btnCerrarFP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrarFP.Click
        CtlMensajes1.Mensaje("Se ha cancelado la agenda del presente cliente...", "")
        PnlProximaAccion.Visible = False
    End Sub

    Private Sub btnCerrarLiquidacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrarLiquidacion.Click
        PnlMostrarLiquidacion.Visible = False
    End Sub

    Function GetUserName() As String
        If TypeOf My.User.CurrentPrincipal Is System.Security.Principal.WindowsPrincipal Then
            ' The application is using Windows authentication.
            ' The name format is DOMAIN\USERNAME.
            Dim parts() As String = Split(My.User.Name, "\")
            Dim username As String = parts(1)
            Return username
        Else
            ' The application is using custom authentication.
            Return My.User.Name
        End If
    End Function

    Private Sub BtnRedirect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnRedirect.Click
        Dim ctl As New Controles.General
        ctl.Obtiene_Consulta(":idagenda▓" & Obtiene_Cookies("ppidagenda"), "GES009")
        Dim Url As String = "Gestion.aspx?DNI=" & Obtiene_Cookies("ppDNI") & "&idcliente=" & Obtiene_Cookies("ppidcliente") & "&idcartera=" & Obtiene_Cookies("ppidcartera") & "&idusuario=" & Obtiene_Cookies("idusuario") & "&NombreGestor=" & HNombreGestor.Value & "&tipocartera=CASTIGO&usuario=" & Husuario.Value
        Response.Redirect(Url)
        ctl = Nothing
    End Sub

    Private Sub BtnPosponer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPosponer.Click
        Dim ctl As New Controles.General
        ctl.Obtiene_Consulta(":idagenda▓" & Obtiene_Cookies("ppidagenda"), "GES009")
        ctl = Nothing
        PnlProximaAccion.Visible = True
        PnlMensajeAlerta.Visible = False
        CtlMensajes1.Mensaje("Accion suspendida, dar click en aceptar para ingresar nueva programacion... ", "")
    End Sub

    Private Sub BtnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
        Dim ctl As New Controles.General
        ctl.Obtiene_Consulta(":idagenda▓" & Obtiene_Cookies("ppidagenda"), "GES009")
        ctl = Nothing
        CtlMensajes1.Mensaje("Se eliminacion la accion de gestion ...", "")
        PnlMensajeAlerta.Visible = False
    End Sub

    Protected Sub btnAvanzar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAvanzar.Click
        Dim ctl As New ConDB.ConSQL
        ctl.FunctionGlobal(":psesgestionƒ:pusuario▓" & 0 & "ƒ" & Husuario.Value, "SQLUSR002")
        ctl.FunctionGlobal(":idusuarioƒ:idclienteƒ:idgestion▓" & Obtiene_Cookies("idusuario") & "ƒ" & idcliente & "ƒ" & Obtiene_Cookies("idgestion"), "BOL002")
        Dim dtp1 = ctl.FunctionGlobal(":idusuario▓" & Obtiene_Cookies("idusuario"), "BOL001")
        If Not dtp1 Is Nothing Then
            If dtp1.rows.count > 0 Then
                Response.Redirect("Gestion.aspx?Bolsa=S" & _
                              "&DNI=" & dtp1.rows(0)(0) & _
                              "&idcliente=" & dtp1.rows(0)(1) & _
                              "&idcartera=" & dtp1.rows(0)(2) & _
                              "&idusuario=" & dtp1.rows(0)(3) & _
                              "&NombreGestor=" & dtp1.rows(0)(4) & _
                              "&tipocartera=" & dtp1.rows(0)(5) & _
                              "&usuario=" & dtp1.rows(0)(6))
                Exit Sub
            End If
        End If
        dtp1 = Nothing
    End Sub

    'Protected Sub chkHistoricoGest_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkHistoricoGest.CheckedChanged
    '    Dim ctl As New Controles.General
    '    If chkHistoricoGest.Checked Then
    '        Call ctl.Gestion_Telefono_grilla_h(txtDNI.Text, idEmpresa, CtlGestionTelefonica)
    '    Else
    '        Call ctl.Gestion_Telefono_grilla(txtDNI.Text, idEmpresa, CtlGestionTelefonica)
    '    End If
    '    ctl = Nothing
    'End Sub

    Protected Sub chkHistGestPositivas_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkHistGestPositivas.CheckedChanged
        Dim ctl As New Controles.General
        If chkHistGestPositivas.Checked Then
            Call ctl.Gestion_Telefono_grilla_HPositivas(txtDNI.Text, idEmpresa, CtlGestTelefPositivas)
        Else
            Call ctl.Gestion_Telefono_grilla_Positivas(txtDNI.Text, idEmpresa, CtlGestTelefPositivas)
        End If
        ctl = Nothing
    End Sub

    Protected Sub chkHistGestNegativas_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkHistGestNegativas.CheckedChanged
        Dim ctl As New Controles.General
        If chkHistGestNegativas.Checked Then
            Call ctl.Gestion_Telefono_grilla_HNegativas(txtDNI.Text, idEmpresa, CtlGestTelefNegativas)
        Else
            Call ctl.Gestion_Telefono_grilla_Negativas(txtDNI.Text, idEmpresa, CtlGestTelefNegativas)
        End If
        ctl = Nothing
    End Sub

    Protected Sub chkHistoricoGestC_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkHistoricoGestC.CheckedChanged
        Dim ctl As New Controles.General
        If chkHistoricoGestC.Checked Then
            Call ctl.Gestion_Campo_grilla_H(txtDNI.Text, idEmpresa, CtlGestionCampo)
        Else
            Call ctl.Gestion_Campo_grilla(txtDNI.Text, idEmpresa, CtlGestionCampo)
        End If
        ctl = Nothing
    End Sub

    '<System.Web.Services.WebMethod()> _
    '<System.Web.Script.Services.ScriptMethod()> _
    'Public Shared Function GetMensaje() As String
    '    Dim ctl As New Controles.General
    '    Dim StrMensaje As String = ""
    '    Dim cogeCookie = HttpContext.Current.Request.Cookies.Get("idcliente")
    '    Dim pidusuario = cogeCookie.Value
    '    Dim idMensaje As Integer = 0
    '    Dim dt = ctl.BuscarSQL(":idcliente▓" & pidusuario, "GES008")
    '    If Not dt Is Nothing Then
    '        If dt.Rows.Count > 0 Then
    '            idMensaje = dt.Rows(0)(0)
    '            StrMensaje = " (" & dt.Rows(0)(0) & ") " & dt.Rows(0)(2)
    '            dt = Nothing
    '        Else
    '            dt = Nothing
    '        End If
    '    End If
    '    Return StrMensaje
    'End Function

    Public Function GetLlamada() As String
        Dim StrMensaje As String = ""
        Dim ctl As New Controles.General
        Dim dt1 As DataTable
        Crear_Cookies("sipid", "")
continua:
        dt1 = ctl.Trae_datos_php("/asterisk/leeragentes.php")
        If Not dt1 Is Nothing Then
            If dt1.Rows.Count > 3 Then
                dt1 = ctl.SelectDataTable(dt1, "id_llamada =  '" & HttpContext.Current.Request.Cookies.Get("Anexo").Value.ToString & "' and Contexto ='macro-llamadas-sistema'", "id_llamada")
                If dt1.Rows.Count > 0 Then
                    Crear_Cookies("sipid", dt1.Rows(0)(0))
                    Label28.Text = "Llamada en proceso: " & dt1.Rows(0)(0)
                Else
                    GoTo continua
                End If
            Else
                GoTo continua
            End If
        End If
        Return StrMensaje
    End Function

    '<System.Web.Services.WebMethod()> _
    '<System.Web.Script.Services.ScriptMethod()> _
    'Public Shared Function GetUploadStatus() As String
    '    Dim ctl As New Controles.General
    '    Dim StrMensaje As String = ""
    '    Dim cogeCookie = HttpContext.Current.Request.Cookies.Get("idusuario")
    '    Dim pidusuario = cogeCookie.Value
    '    Dim idMensaje As Integer = 0
    '    Dim dt = ctl.BuscarSQL(":idUsuario▓" & pidusuario, "QRYSIC001")
    '    If Not dt Is Nothing Then
    '        If dt.Rows.Count > 0 Then
    '            idMensaje = dt.Rows(0)(0)
    '            StrMensaje = " (" & dt.Rows(0)(0) & ") " & dt.Rows(0)(2)
    '            dt = Nothing
    '        Else
    '            dt = Nothing
    '        End If
    '    End If
    '    Return StrMensaje
    'End Function

    '<System.Web.Services.WebMethod()> _
    '<System.Web.Script.Services.ScriptMethod()> _
    '   Public Shared Function GrabarMensaje(ByVal lblCCidMensajes) As Boolean
    '    Try
    '        Dim ctl As New Controles.General
    '        ctl.BuscarSQL(":idMensaje▓" & lblCCidMensajes.ToString.Trim(), "QRYSIC002")
    '        Return True
    '    Catch ex As Exception
    '        Return False
    '    End Try
    'End Function

    Sub Exportar_PDF()

        Dim dt_1 As DataTable = Nothing
        'Dim dt_2 As DataTable = Nothing
        'Dim dt_3 As DataTable = Nothing
        Dim dt_4 As DataTable = Nothing
        Dim dt_5 As DataTable = Nothing
        Dim dt_6 As DataTable = Nothing
        Dim ctl As New BL.Cobranza
        Dim criterio As String = ""
        criterio = criterio & " RUC = '" & Dni_Activo & "'"

        Try
            'Se obtiene la data de la Liquidacion pero mas resumida para el PDF filtrado por el criterio enviado
            dt_1 = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_N_GEST063")
            ''Se obtiene los meses del cual se tiene deuda (Devengues) filtrado por el criterio enviado
            'dt_2 = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_N_GEST064")
            ''Se obtiene los años del cual se tiene deuda (Devengues) filtrado por el criterio enviado
            'dt_3 = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_N_GEST065")
            'Se obtiene la deuda total filtrado por el criterio enviado
            dt_4 = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_N_GEST066")
            'Se obtiene el nombre corto de la empresa filtrado por el criterio enviado
            dt_5 = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_N_GEST067")
            'Se obtiene la fecha de vencimiento de la carta
            dt_6 = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_N_GEST068")
        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al cargar la liquidacion.")
        End Try

        Dim oDoc As New iTextSharp.text.Document(PageSize.A4, 0, 0, 0, 0)
        Dim pdfw As iTextSharp.text.pdf.PdfWriter
        Dim cb As PdfContentByte
        Dim fuente_1 As iTextSharp.text.pdf.BaseFont
        Dim fuente_2 As iTextSharp.text.pdf.BaseFont
        Dim fuente_3 As iTextSharp.text.pdf.BaseFont
        Dim fuente_4 As iTextSharp.text.pdf.BaseFont
        Dim fuente_5 As iTextSharp.text.pdf.BaseFont
        Dim Milisegundos As String = CStr(Now.Millisecond)
        'Dim NombreArchivo_1 As String = "E:\Gestion_AFP\RepLiquidacion\RepLiquidacion" & Format(Now(), "yyyyMMddhhmmss" & Milisegundos) & ".pdf"
        Dim NombreArchivo_1 As String = "E:\Gestion_AFP\RepLiquidacion\" & dt_5(0)(0) & ".pdf"
        Dim imgLogo As iTextSharp.text.Image
        'Dim imgAFP As iTextSharp.text.Image

        Try
            pdfw = PdfWriter.GetInstance(oDoc, New FileStream(NombreArchivo_1, FileMode.Create, FileAccess.Write, FileShare.None))

            'Apertura del documento.
            oDoc.Open()
            cb = pdfw.DirectContent

            'Agregamos una pagina.
            oDoc.NewPage()

            'Iniciamos el flujo de bytes.
            cb.BeginText()

            imgLogo = iTextSharp.text.Image.GetInstance("E:\Gestion_AFP\img\LogoM.jpg")
            imgLogo.ScalePercent(5)
            imgLogo.SetAbsolutePosition(50, 730)
            oDoc.Add(imgLogo)

            'imgAFP = iTextSharp.text.Image.GetInstance("E:\Gestion_AFP\img\Logo_AFP.jpg")
            'imgAFP.ScalePercent(50)
            'imgAFP.SetAbsolutePosition(400, 740)
            'oDoc.Add(imgAFP)

            'Instanciamos el objeto para la tipo de letra.
            fuente_1 = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.BOLD).BaseFont

            'Seteamos el tipo de letra y el tamaño.
            cb.SetFontAndSize(fuente_1, 14)

            'Seteamos el color del texto a escribir.
            cb.SetColorFill(iTextSharp.text.BaseColor.BLACK)

            'Aqui es donde se escribe el texto.
            'Aclaracion: Por alguna razon la coordenada vertical siempre es tomada desde el borde inferior (de ahi que se calcule como "PageSize.A4.Height - 50")
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "LIQUIDACION", 300, PageSize.A4.Height - 150, 0)

            fuente_2 = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(fuente_2, 10)

            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, Format(Now, "Long Date"), 545, PageSize.A4.Height - 190, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Sres: " & txtnombre.Text, 50, PageSize.A4.Height - 220, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "RUC: " & txtDNI.Text, 50, PageSize.A4.Height - 250, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Por medio de la presente le hacemos llegar la liquidación de la deuda solicitada por PRIMA AFP.", 50, PageSize.A4.Height - 280, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "La información detallada se le adjunta a continuación :", 50, PageSize.A4.Height - 310, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "• Para los pagos de planillas generar Ticket a través del portal de AFP Net", 50, PageSize.A4.Height - 340, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "• El pago de los Gastos Administrativos se depositan a la cuenta de Estudio Martinez Consultores &", 50, PageSize.A4.Height - 360, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "  Abogados (indicado líneas abajo) los cuales están vigentes hasta el " & dt_6(0)(0).ToString & ".", 50, PageSize.A4.Height - 380, 0)

            'Aca va el tema del Periodo

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''Aca va el tema del Total de la deuda
            Dim DeudaTotal As Decimal = dt_4(0)(0).ToString

            Dim MontoComision As Decimal = DeudaTotal * 0.15

            If MontoComision < 50 Then
                MontoComision = 50
            End If

            Dim MontoIGV As Decimal = MontoComision * 0.18

            Dim MontoTotalComision As Decimal = MontoComision + MontoIGV
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            fuente_3 = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.BaseColor.LIGHT_GRAY).BaseFont
            cb.SetFontAndSize(fuente_3, 8)

            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "PRE-FACTURA / ESTUDIO MARTINEZ CONSULTORES & ABOGADOS S.A.C.", 400, PageSize.A4.Height - 625, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "SON:                       S/." & Format(MontoTotalComision, "#,###,###.00"), 348, PageSize.A4.Height - 710, 0)

            fuente_4 = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(fuente_4, 8)

            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "GASTOS DE COBRANZA AFP PRIMA", 330, PageSize.A4.Height - 650, 0)
            'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "DEVENGUES: PERIODOS (COLOCAR PERIODOS) " & Format(MontoComision, "#,###,###.00"), 50, PageSize.A4.Height - 675, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "DEVENGUES:         S/." & Format(MontoComision, "#,###,###.00"), 348, PageSize.A4.Height - 670, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "IGV - 18%:               S/." & Format(MontoIGV, "#,###,###.00"), 348, PageSize.A4.Height - 690, 0)
            'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "SON: (COLOCAMOS EL MONTO) " & Format(MontoTotalComision, "#,###,###.00"), 50, PageSize.A4.Height - 725, 0)

            fuente_5 = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(fuente_5, 7)

            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "CTA CTE SOLES BBVA: 0011 0732 0100012413 59 - CCI 011 732 000100012413 59", 50, PageSize.A4.Height - 740, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "CTA CTE SOLES BCP : 193 2601472 0 94 - CCI 002193 002601472 0 9415", 50, PageSize.A4.Height - 760, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ENVIAR DEPÓSITO A: afpcobranza@estudiomartinez.com", 50, PageSize.A4.Height - 780, 0)

            InsertarTabla(cb, dt_1, dt_4, New Single() {6.0F, 3.0F, 3.0F, 6.0F, 4.0F}, True)

            'Fin del flujo de bytes.
            cb.EndText()
            'Forzamos vaciamiento del buffer.
            pdfw.Flush()
            'Cerramos el documento.
            oDoc.Close()

            Response.AddHeader("Content-Disposition", "attachment; filename=" + NombreArchivo_1)
            Response.TransmitFile(NombreArchivo_1)

        Catch ex As Exception
            'Si hubo una excepcion y el archivo existe ...
            If File.Exists(NombreArchivo_1) Then
                'Cerramos el documento si esta abierto.
                'Y asi desbloqueamos el archivo para su eliminacion.
                If oDoc.IsOpen Then oDoc.Close()
                '... lo eliminamos de disco.
                File.Delete(NombreArchivo_1)
            End If
            'Throw New Exception("Error al generar archivo PDF (" & ex.Message & ")")
            CtlMensajes1.Mensaje("Error al generar archivo PDF (" & ex.Message & ")")
        Finally
            cb = Nothing
            pdfw = Nothing
            oDoc = Nothing
        End Try
    End Sub

    Private Sub InsertarTabla(ByRef pCb As pdf.PdfContentByte, ByRef pTabla_1 As DataTable, ByRef pTabla_2 As DataTable, ByRef pDimensionColumnas() As Single, ByVal pIncluirEncabezado As Boolean)

        Dim table As New iTextSharp.text.pdf.PdfPTable(pTabla_1.Columns.Count)
        table.TotalWidth = pCb.PdfDocument.PageSize.Width - 100

        table.SetWidths(pDimensionColumnas)

        table.SpacingBefore = 1.0F
        table.SpacingAfter = 1.0F

        If pIncluirEncabezado Then
            Dim fuenteEncabezado As text.Font = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL)
            'Dim fuenteEncabezado As iTextSharp.text.pdf.BaseFont = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont

            For Each oColumna As Data.DataColumn In pTabla_1.Columns

                Dim celda As New pdf.PdfPCell(New Phrase(oColumna.Caption, fuenteEncabezado))

                celda.Colspan = 1
                celda.Padding = 5
                celda.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY
                celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
                celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
                table.AddCell(celda)
            Next oColumna

        End If

        Dim fuenteDatos As text.Font = FontFactory.GetFont(FontFactory.COURIER, 7, iTextSharp.text.Font.NORMAL)

        For Each oFila As Data.DataRow In pTabla_1.Rows
            For Each oColumna As Data.DataColumn In pTabla_1.Columns
                Dim celda As New pdf.PdfPCell(New Phrase(oFila(oColumna), fuenteDatos))
                celda.Colspan = 1
                celda.Padding = 3
                celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
                celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
                table.AddCell(celda)
            Next oColumna
        Next oFila

        Dim Celda_TotalDeudaAFP As New PdfPCell(New Phrase("DEUDA TOTAL DE AFP                                                                                                                                               " & pTabla_2(0)(0), FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8, iTextSharp.text.Font.NORMAL)))
        Celda_TotalDeudaAFP.Colspan = 5
        Celda_TotalDeudaAFP.Padding = 5
        Celda_TotalDeudaAFP.BackgroundColor = iTextSharp.text.pdf.ExtendedColor.LIGHT_GRAY
        Celda_TotalDeudaAFP.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
        Celda_TotalDeudaAFP.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        table.AddCell(Celda_TotalDeudaAFP)

        table.WriteSelectedRows(0, -1, 50, 435, pCb)
        pCb.PdfWriter.Flush()
    End Sub

    Private Sub btnExportarLiquidacion_PDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarLiquidacion_PDF.Click
        Exportar_PDF()
    End Sub

    Private Sub imgLiquidacion_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgLiquidacion.Click
        MostrarLiquidacion()
    End Sub

    Function TraerCompromisos()

        Dim fnci As New BL.Cobranza
        Dim dt As DataTable
        Dim TotalCompromisosDevengues As Integer

        Dim TraeCantidadCompromisos As String
        TraeCantidadCompromisos = "select count(NumOperacion) as cantidad from  Compromiso where year(FechaCompromiso) = year(getdate()) and month(FechaCompromiso) = month(getdate()) and IdCliente = " & idcliente
        dt = fnci.FunctionConsulta(TraeCantidadCompromisos)
        TotalCompromisosDevengues = dt.Rows(0)("cantidad")

        Return TotalCompromisosDevengues

    End Function

    Sub CargaNombrePanel()
        lblCompromisos.Text = txtnombre.Text
    End Sub

    Private Sub btnGrabaAgenda_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabaAgenda.Click

        If validaciones() = 0 Then
            Exit Sub
        Else
            Dim ctl As New BL.Cobranza

            Dim FechaAgenda As String
            Dim HoraAgenda As Integer
            Dim MinutoAgenda As Integer

            FechaAgenda = txtFechaAgenda.Text
            HoraAgenda = txtHoraAgenda.Text
            MinutoAgenda = txtMinutoAgenda.Text

            Cambiar_Estado_Agendas_Anteriores(FechaAgenda, HoraAgenda, MinutoAgenda)
            'telefono_activo
            Try
                ctl.FunctionGlobal(":idusuarioƒ:idclienteƒ:idcarteraƒ:telefonoƒ:fechaagendaƒ:anioagendaƒ:mesagendaƒ:diaagendaƒ:horaagendaƒ:minutoagendaƒ:fechacargaƒ:estado▓" & _
                                    Hidusuario.Value & "ƒ" & idcliente & "ƒ" & idCartera & "ƒ" & NumTelf.Value & "ƒ" & Format(CDate(FechaAgenda), "dd/MM/yyyy") & "ƒ" & Format(CDate(FechaAgenda), "yyyy") & "ƒ" & _
                                    Format(CDate(FechaAgenda), "MM") & "ƒ" & Format(CDate(FechaAgenda), "dd") & "ƒ" & HoraAgenda & "ƒ" & MinutoAgenda & "ƒ" & DateTime.Now.ToString("dd/MM/yyyy") & "ƒA", "SQL_AGE_001")

                'ctl.FunctionGlobal(":idusuarioƒ:idclienteƒ:idcarteraƒ:telefonoƒ:fechaagendaƒ:anioagendaƒ:mesagendaƒ:diaagendaƒ:horaagendaƒ:minutoagendaƒ:fechacargaƒ:estadoƒ:situacion▓" & _
                '                    Hidusuario.Value & "ƒ" & idcliente & "ƒ" & idCartera & "ƒ" & NumTelf.Value & "ƒ" & Format(CDate(FechaAgenda), "dd/MM/yyyy") & "ƒ" & Format(CDate(FechaAgenda), "yyyy") & "ƒ" & _
                '                    Format(CDate(FechaAgenda), "MM") & "ƒ" & Format(CDate(FechaAgenda), "dd") & "ƒ" & HoraAgenda & "ƒ" & MinutoAgenda & "ƒ" & DateTime.Now.ToString("dd/MM/yyyy") & "ƒAƒPendiente", "SQL_AGE_001_PRUEBA")

                CtlMensajes1.Mensaje("Se ha agendado su llamada correctamente.")

            Catch ex As Exception
                CtlMensajes1.Mensaje("Hubo problemas para agendar su llamada (Grabar Agenda). Favor de comunicarse con TI")
            End Try
        End If
    End Sub

    Function validaciones()

        If txtFechaAgenda.Text = "" Or txtMinutoAgenda.Text = "" Or txtMinutoAgenda.Text = "" Then
            CtlMensajes1.Mensaje("Por favor ingresar datos en los campos requeridos.")
            Return 0
        ElseIf CInt(txtHoraAgenda.Text) > 23 Or CInt(txtHoraAgenda.Text) < 0 Then
            CtlMensajes1.Mensaje("La hora ingresada no puede ser menor que 0 ni mayor a 23.")
            Return 0
        ElseIf CInt(txtMinutoAgenda.Text) > 59 Or CInt(txtMinutoAgenda.Text) < 0 Then
            CtlMensajes1.Mensaje("Los minutos ingresados no pueden ser menor que 0 ni mayor a 59.")
            Return 0
            'ElseIf txtFechaAgenda.ToString("dd/MM/yyyy") < DateTime.Now.ToString("dd/MM/yyyy") Then
            'ElseIf (DateTime.Compare(DateTime.Now, CDate(txtFechaAgenda.Text)) >= 0) Then
        ElseIf (DateDiff(DateInterval.Day, CDate(Now.ToString("dd/MM/yyyy")), CDate(txtFechaAgenda.Text)) < 0) Then
            CtlMensajes1.Mensaje("La fecha agendada no puede ser menor que la fecha actual.")
            Return 0
            'ElseIf DateDiff("d", CDate(txtFechaAgenda.Text), DateTime.Now.ToString("dd/MM/yyyy")) Then
        ElseIf (DateDiff(DateInterval.Day, DateTime.Now, CDate(txtFechaAgenda.Text)) > 7) Then
            CtlMensajes1.Mensaje("La fecha agendada puede tener como máximo 1 semana de planificación.")
            Return 0
        Else
            Return 1
        End If
    End Function

    Sub Cambiar_Estado_Agendas_Anteriores(ByVal fecha As Date, ByVal Hora As Integer, ByVal Minuto As Integer)

        Dim criterio = ""
        criterio = " idUsuario = " & Hidusuario.Value & " AND idCliente = " & idcliente & " AND idCartera = " & idCartera & " AND Estado = 'A' "

        Try
            conexion.FunctionGlobal(":criterio▓" & criterio, "SQL_AGE_003")
            'conexion.FunctionGlobal(":criterio▓" & criterio, "SQL_AGE_003_PRUEBA")
            'CtlMensajes1.Mensaje("Se cambió el estado de la llamada agendada anteriormente.")
        Catch ex As Exception
            CtlMensajes1.Mensaje("Hubo problemas para agendar su llamada (Cambiar Estado Agendas). Favor de comunicarse con TI.")
        End Try
    End Sub

    Private Sub imgbtnAgenda_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnAgenda.Click
        CargarAgendaGestion()
    End Sub

    Sub CargarAgendaGestion()
        Dim ctl As New BL.Cobranza
        Dim dt As New DataTable
        Dim criterio As String = ""

        'ActualizaSituacionAgenda()

        criterio = " AND a.idUsuario = " & Obtiene_Cookies("idusuario")

        Try
            dt = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_AGE_004")
            'dt = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_AGE_004_PRUEBA")

            If dt.Rows.Count = 0 Then
                Exit Sub
            Else
                pnlAgendaGestion.Visible = True
                gvMostrarAgendaGestion.SourceDataTable = dt
            End If

        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al cargar la agenda.")
        End Try
    End Sub

    Sub ActualizaSituacionAgenda()
        Dim ctl As New BL.Cobranza
        Dim dt As New DataTable
        Dim criterio As String = ""

        criterio = " AND a.idUsuario = " & Obtiene_Cookies("idusuario")

        Try
            dt = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_AGE_005")
            'dt = ctl.FunctionGlobal(":criterio▓" & criterio, "SQL_AGE_005_PRUEBA")

        Catch ex As Exception
            CtlMensajes1.Mensaje("Ocurrio un error al revisar la situacion de la agenda.")
        End Try

        Dim Hora_Agenda As DateTime = CType(dt(0)(0)("HH:mm"), DateTime)
        Dim Hora_Sistema As DateTime = CType(Now.ToString("HH:mm"), DateTime)

        Dim result As Integer = 0
        Dim result1 As Integer = 0

        'result = DateTime.Compare(hora, hora_desa_inicio)
        'result1 = DateTime.Compare(hora, hora_desa_fin)

        If result > 0 And result1 < 0 Then

        End If
    End Sub

    Private Sub btnCerrarAgenda_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrarAgenda.Click
        pnlAgendaGestion.Visible = False
    End Sub

    Private Sub btnGrabarProcesoVenta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabarProcesoVenta.Click
        If validacion_seleccion_check() = 0 Then
            Exit Sub
        Else
            Dim ctl As New BL.Cobranza

            Dim idGestion As Integer = 0
            Dim Financiado As String
            Dim Efectivo As String

            If lblId_Gestion_PLM.Text <> "" Then
                idGestion = lblId_Gestion_PLM.Text
            End If

            If chbFinanciado.Checked = True Then
                Financiado = "SI"
            Else
                Financiado = "NO"
            End If

            If chbEfectivo.Checked = True Then
                Efectivo = "SI"
            Else
                Efectivo = "NO"
            End If

            Cambiar_Estado_ProcesoVenta_Anterior()

            Try
                ctl.FunctionGlobal(":idclienteƒ:idgestionƒ:idusuarioƒ:idcarteraƒ:financiadoƒ:efectivoƒ:fecharegistroƒ:estado▓" & _
                                    idcliente & "ƒ" & idGestion & "ƒ" & Hidusuario.Value & "ƒ" & idCartera & "ƒ" & Financiado & "ƒ" & Efectivo & "ƒ" & DateTime.Now.ToString("dd/MM/yyyy") & "ƒA", "SQL_PROVEN_001")

                CtlMensajes1.Mensaje("Se ha registrado el proceso de venta de su cliente correctamente.")

            Catch ex As Exception
                CtlMensajes1.Mensaje("Hubo problemas para registrar el proceso de venta de su cliente (Graba Proceso Venta). Favor de comunicarse con TI")
            End Try
        End If
    End Sub

    Function validacion_seleccion_check()
        If chbFinanciado.Checked = False And chbEfectivo.Checked = False Then
            CtlMensajes1.Mensaje("No ha seleccionado ninguna de las opciones.")
            Return 0
        Else
            Return 1
        End If
    End Function

    Sub Cambiar_Estado_ProcesoVenta_Anterior()

        Dim criterio = ""
        criterio = "idCliente = " & idcliente & " AND idCartera = " & idCartera & " AND Estado = 'A' "

        Try
            conexion.FunctionGlobal(":criterio▓" & criterio, "SQL_PROVEN_003")
            'CtlMensajes1.Mensaje("Se cambió el estado de la llamada agendada anteriormente.")
        Catch ex As Exception
            CtlMensajes1.Mensaje("Hubo problemas para registrar el proceso de venta de su cliente (Cambiar Estado). Favor de comunicarse con TI")
        End Try
    End Sub

End Class