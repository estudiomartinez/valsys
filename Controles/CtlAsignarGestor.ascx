﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlAsignarGestor.ascx.vb" Inherits="Controles.CtlAsignarGestor" %>

<%@ Register src="CtCombo.ascx" tagname="CtCombo" tagprefix="uc1" %>
<%@ Register src="CtlTxt.ascx" tagname="CtlTxt" tagprefix="uc2" %>

<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc3" %>

<table cellpadding="0" cellspacing="0" class="fondoPantalla">
<tr>
    <td colspan="2" class="titulo">
        <center>
            <uc3:CtlMensajes ID="CtlMensajes1" runat="server" />
            <asp:Label ID="lblTituloControl" runat="server" Text="ASIGNAR GESTORES" ForeColor="White" Font-Bold="true" Font-Size="16px"></asp:Label>
        </center>
    </td>
</tr>
<tr>
    <td><asp:Label ID="lblCartera" runat="server" ForeColor="White" text="CARTERA: "/></td>
    <td><uc1:CtCombo ID="cboCartera" runat="server" Longitud="150" Procedimiento="QRYC007" Condicion="" AutoPostBack="true" /></td>
    
</tr>
<tr>    
    <td><asp:Label ID="Label2" runat="server" ForeColor="White" text="GESTOR"/></td>
    <td><uc1:CtCombo ID="cboGestorA" runat="server" Longitud="200" AutoPostBack="true"/></td>    
</tr>    
<tr>    
    <td><asp:Label ID="Label3" runat="server" ForeColor="White" text="COND.INTERNA"/></td>
    <td><uc1:CtCombo ID="cbocint" runat="server" Longitud="100" Procedimiento="QRYCGC001" AutoPostBack="true"/></td>    
</tr>
<tr>
    <td><asp:Label ID="lblCantidad" runat="server" ForeColor="White" text="CANTIDAD DE CLIENTES: "/></td>
    <td><uc2:CtlTxt ID="txtCantidad" runat="server" Ancho="60" Desactiva="false"  /></td>
</tr>    
<tr>
    <td colspan="2">
        <fieldset>
        <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td><asp:Label ID="lblDe" runat="server" text="DE: "/></td>
            <td><uc2:CtlTxt ID="txtDesde" runat="server" Ancho="50" /></td>
            <td><asp:Label ID="lblHasta" runat="server" text="HASTA: "/></td>
            <td><uc2:CtlTxt ID="txtHasta" runat="server" Ancho="50" /></td>
            <td><asp:Label ID="lblA" runat="server" Text="A: " /></td>
            <td><uc1:CtCombo ID="cboGestor" runat="server" Longitud="200" Procedimiento="QRYCBG002" Condicion=""  /></td>            
        </tr>
        </table>
        </fieldset>
    </td>
</tr>
<tr>
    <td colspan="2">
        <table width="100%">
        <tr>
            <td></td>
            <td  style="text-align:right;" width="35px">
				<div class="curvo">
	            <asp:ImageButton id="btnGrabar" runat="server" Height="30px" Width="30px" ToolTip="Grabar" 
	            ImageUrl="~/Imagenes/BotonGrabar.png"/>
                <asp:Label id="lblGrabar" runat="server" Font-Size="11px" Text="Grabar"></asp:Label>
                </div>															 					
			</td>
            <td  style="text-align:right;" width="35px">
				<div class="curvo">
	            <asp:ImageButton id="BtnCerrar" runat="server" Height="30px" Width="30px" ToolTip="Cerrar"
	            ImageUrl="~/Imagenes/BotonCerrar.jpg"/>
                <asp:Label id="Label1" runat="server" Font-Size="11px" Text="Cerrar"></asp:Label>
                </div>															 					
			</td>
        </tr>
        </table>
    </td>
</tr>
</table>
