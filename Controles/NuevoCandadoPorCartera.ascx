﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NuevoCandadoPorCartera.ascx.vb" Inherits="Controles.NuevoCandadoPorCartera" %>
<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc1" %>
<%@ Register src="CtCombo.ascx" tagname="CtCombo" tagprefix="uc2" %>
<table style="width: 100%;" align="center">
<tr align="center">
<td align="center" style="text-align:center;" class="titulo">
    <asp:Label id="lblTituloControl" runat="server" ></asp:Label>
    <uc1:CtlMensajes ID="CtlMensajes1" runat="server" />
</td>
</tr>
</table>
<fieldset>
<table style="width: 100%">
        <tr>
			<td> &nbsp; </td>
		</tr>
		<tr>
			<td align="right">
			<asp:Label id="lblIdDeCartera" runat="server" Font-Size="11px" Text="Id de Cartera"></asp:Label>
			</td>
			<td>
			<asp:Label ID="lblIdCartera_aux" runat="server" Visible="false"></asp:Label>
			<asp:TextBox id="txtIdCartera" runat="server" Font-Size="11px" Width="90px" Enabled="False" BackColor="Gainsboro"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td align="right">
			<asp:Label id="lblNomCartera" runat="server" Font-Size="11px" Text="Nombre de la Cartera"></asp:Label>
			</td>
			<td>
			<asp:TextBox id="txtNomCartera" runat="server" Font-Size="11px" Width="245px" Enabled="False" BackColor="Gainsboro"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td align="right">
			<asp:Label id="Label3" runat="server" Font-Size="11px" Text="Tipo Cartera"></asp:Label>
			</td>
			<td>
			    <uc2:CtCombo ID="cboTipo" runat="server" Condicion=":condicion▓100" Procedimiento="QRYMG001" Activa="False" Color="Gainsboro" />
			</td>
		</tr>
		<tr>
		    <td align="right">
			    <asp:Label id="lblNumDias_1" runat="server" Font-Size="11px" Text="Duración del candado"></asp:Label>
			</td>
			<td>
			    <asp:TextBox id="txtNumDias" runat="server" Font-Size="11px" Width="90px"></asp:TextBox>
			    &nbsp;
			    <asp:Label id="lblNumDias_2" runat="server" Font-Size="11px" Text="Dias"></asp:Label>
			</td>
		</tr>
		<tr>
			<td align="right">
			<asp:Label id="lblTipoCandado" runat="server" Font-Size="11px" Text="Tipo de Candado"></asp:Label>
			</td>
			<td>
			<asp:TextBox id="txtTipoCandado" runat="server" Font-Size="11px" Width="90px" Text="Dinámico" Enabled="False" BackColor="Gainsboro"></asp:TextBox>			
			</td>
		</tr>
		<tr>
			<td align="right">
			<asp:Label id="lblFechaFin" runat="server" Font-Size="11px" Text="Fecha Finalización del candado"></asp:Label>
			</td>
			<td>
			<asp:TextBox id="txtFechaFin" runat="server" Font-Size="11px" Width="90px"></asp:TextBox>
			<img id="imgFechaFinCandado" visible="true"  alt="calendario" height="16" onclick="return showCalendar('imgFechaFinCandado','<%=txtFechaFin.ClientID%>','%d/%m/%Y','24', true);" src="Imagenes/calendario.png" width="18"/>
			</td>
		</tr>
		<tr>
			<td> &nbsp; </td>
		</tr>
	</table>
</fieldset>
<table style="width: 100%">
				<tr>
					<td align="center" style="text-align:center;">
					<div class="curvo">
					    <asp:ImageButton id="imgGrabar" runat="server" Height="30px" Width="35px" 
                            ImageUrl="~/imagenes/BotonGrabar.png" />
					    <asp:Label id="Label6" runat="server" Font-Size="11px" Text="Grabar"></asp:Label>
					</div>			
					</td>
					<td align="center" style="text-align:center;">
					<div class="curvo">
					<asp:ImageButton id="imgCerrar" runat="server" Height="30px" Width="35px" 
                            ImageUrl="~/imagenes/BotonCerrar.jpg" />
					<asp:Label id="Label7" runat="server" Font-Size="11px" Text="Cerrar"></asp:Label>
					</div>
					</td>
				</tr>
</table>