﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ReporteComposicionCarteraGestor.ascx.vb" Inherits="Controles.ReporteComposicionCarteraGestor" %>
<%@ Register src="CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc1" %>

<%@ Register src="CtCombo.ascx" tagname="CtCombo" tagprefix="uc3" %>
<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc2" %>
<table style="width: 100%;" align="center">
<tr align="center">
<td align="center" style="text-align:center;" class="titulo">
    <asp:Label id="lblTituloControl" runat="server" ></asp:Label>
    <uc2:CtlMensajes ID="CtlMensajes1" runat="server" />
</td>
</tr>
</table>


	<table style="width: 100%">
		<tr>
			<td valign="top">
			<fieldset style="height:510px;">
			<legend>FILTRAR POR</legend>
			<table style="width: 100%">
				<tr>
					<td style="height: 23px">
					<table style="width: 100%">
						<tr>
							<td>
			                    <asp:Label id="Label2" runat="server" Font-Size="11px" Text="CARTERA"></asp:Label>
					            <br />
			                    <uc3:CtCombo ID="cboCartera" runat="server" Activa="true" Procedimiento="QRYC007" Condicion="" />
					        </td>
					        <td>
					            <div id="GrupoGestor" runat="server">
			                        <asp:Label id="Label1" runat="server" Font-Size="11px" Text="GESTOR"></asp:Label>
					                <br />
					                <uc3:CtCombo ID="cboGestor" runat="server" Activa="true" 
                                    Condicion="" Longitud="100" />
                                </div>
					        </td>
							    <td align="left">
							        <div id="GrupoInicio" runat="server">
			                            FECHA INICIO
							            <br />
			                            <asp:TextBox ID="txtFechaInicio" runat="server"  Width="80px" Enabled = "false" BackColor="White"/>
                                        <img ID="txtFechaInicio" alt="calendario" height="16"  onclick="return showCalendar('txtFechaInicio','<%=txtFechaInicio.ClientID%>','%d/%m/%Y','24', true);" 
                                        src="Imagenes/calendario.png" width="18" />							
							            </div>
							    </td>
							    <td align="left">
							        <div id="GrupoFin" runat="server">
			                            FECHA FIN
							            <br />
			                            <asp:TextBox ID="txtFechaFin" runat="server"  Width="80px" Enabled = "false" BackColor="White"/>
                                        <img ID="txtFechaFin" alt="calendario" height="16" onclick="return showCalendar('txtFechaFin','<%=txtFechaFin.ClientID%>','%d/%m/%Y','24', true);" 
                                        src="Imagenes/calendario.png" width="18" />
							        </div>
							    </td>
							    <td align="center" style="text-align:center;">
							        <div class="curvo">
			                            <asp:ImageButton id="imgGenerarReporte" runat="server" Height="30px" 
                                        ImageUrl="~/imagenes/BotonGenerarReporte.jpg" Width="35px" />
			                            <asp:Label id="Label4" runat="server" Font-Size="11px" Text="Reporte"></asp:Label>
							        </div>
							    </td>
						    </tr>
					    </table>
					</td>
				</tr>
				<tr>
					<td>
					<fieldset>
		            <legend>CLIENTES ENCONTRADOS</legend>
			        <uc1:CtlGrilla ID="CtlGrilla1" runat="server" Ancho="400px" Largo="250px" Activa_ckeck="false" Activa_option="false" Desactiva_Boton="false" Activa_Delete="false" Activa_Edita="false" />
		            </fieldset>
					</td>
				</tr>
				<tr>
					<td>
					<table style="width: 100%">
						<tr>
							<td align="right" style="text-align:right;">
			<asp:Label id="Label5" runat="server" Font-Size="11px" Text="Total Clientes Gestionados"></asp:Label>
							</td>
							<td>
							<asp:TextBox id="txtTotalClienteCartera" Enabled="false" runat="server" Font-Size="11px" Height="21px" Width="106px"></asp:TextBox>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			    
			</table>
			</fieldset>
			</td>
			<td valign="top">
			    <fieldset style="height:510px;">
			    <legend>GRAFICAS</legend>
				    <asp:CHART id="Chart1" runat="server" Palette="BrightPastel" Height="500px" Width="600px" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)" BackColor="Transparent">
							<titles>
								<asp:Title ShadowColor="32, 0, 0, 0" ShadowOffset="3" Name="Title1" ForeColor="26, 59, 105"></asp:Title>
							</titles>
							<legends>
								<asp:Legend TableStyle="Wide" BackColor="Transparent" Alignment="Near" Docking="Top" Name="Default" LegendItemOrder="SameAsSeriesOrder" LegendStyle="Table"></asp:Legend>
							</legends>
							<borderskin SkinStyle="Sunken"></borderskin>
							<series>
								<asp:Series Name="Series1" ChartType="Pie" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240" Label="#PERCENT{P2}" LegendText="#VALX"></asp:Series>
							</series>
							<chartareas>
								<asp:ChartArea Name="ChartArea1" BackSecondaryColor="Transparent" BackColor="Transparent" Area3DStyle-Enable3D="true">			
								</asp:ChartArea>
							</chartareas>
                    </asp:CHART>
                </fieldset>
			</td>
		</tr>
	</table>