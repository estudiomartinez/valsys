﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ConsultaClientes.ascx.vb" Inherits="Controles.ConsultaClientes" %>
<%@ Register src="CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc1" %>
<%@ Register src="NuevoCLienteGestionarCliente.ascx" tagname="NuevoCLienteGestionarCliente" tagprefix="uc2" %>
<%@ Register src="CtCombo.ascx" tagname="CtCombo" tagprefix="uc3" %>


<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc4" %>


<div style="width:100%;">
<table style="width:100%">
    <tr>
        <td class="titulo">
            CONSULTA DE CLIENTE
            <uc4:CtlMensajes ID="CtlMensajes1" runat="server" />
        </td>
    </tr>
	<tr>
		<td>
			<fieldset style="margin:0; padding:0;" name="Group1">
			<legend>
			    FILTRAR POR
			</legend>
			<table style="width: 100%">
				<tr align="center">
					<td>
					<asp:Label id="Label1" runat="server" Font-Size="11px" Text="CARTERA"></asp:Label>
					<br />
					    <uc3:CtCombo ID="cboCartera" runat="server" Activa="true" 
                            Procedimiento="QRYC007" Condicion=""/>
					</td>
					<td>
					<asp:Label id="Label3" runat="server" Font-Size="11px" Text="CONDIC."></asp:Label>
					<br />
					<uc3:CtCombo ID="cboCondicion" runat="server" Activa="true" 
                            Procedimiento="QRYCGC001" />
					</td>
					<td>
					<asp:Label id="Label4" runat="server" Font-Size="11px" Text="SITUAC."></asp:Label>
					<br />
					<asp:TextBox id="txtSituac" runat="server" Font-Size="11px" Width="73px"></asp:TextBox>
					</td>
					<td>
					<asp:Label id="Label5" runat="server" Font-Size="11px" Text="DNI"></asp:Label>
					<br />
					<asp:TextBox id="txtDNI" runat="server" Font-Size="11px" Width="80px"></asp:TextBox>
					</td>
					<td>
					<asp:Label id="Label6" runat="server" Font-Size="11px" Text="CLIENTE"></asp:Label>
					<br />
					<asp:TextBox id="txtCliente" runat="server" Font-Size="11px" Width="188px"></asp:TextBox>
					</td>
					<td>
					<asp:Label id="Label7" runat="server" Font-Size="11px" Text="TELÉFONO 1"></asp:Label>
					<br />
					<asp:TextBox id="txtTelefono1" runat="server" Font-Size="11px"></asp:TextBox>
					</td>
					<td>
					<asp:Label id="Label8" runat="server" Font-Size="11px" Text="TELÉFONO 2"></asp:Label>
					<br />
					<asp:TextBox id="txtTelefono2" runat="server" Font-Size="11px"></asp:TextBox>
					</td>
					<td align="center" style="text-align:center;">
					<div class="curvo">
					<asp:ImageButton id="imgBuscar" runat="server" Height="30px" Width="35px" 
                            ImageUrl="~/imagenes/boton busqueda.jpg" />
					<asp:Label id="Label14" runat="server" Font-Size="11px" Text="Buscar"></asp:Label>
					</div>
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
	</tr>
	<tr>
		<td>
		<fieldset>
		<legend>CLIENTES ENCONTRADOS</legend>
			<uc1:CtlGrilla ID="CtlGrilla1" runat="server" Ancho="930px" Largo="400px" Activa_ckeck="false" Activa_option="false" Desactiva_Boton="false" OpocionNuevo="true" />
		</fieldset>
		</td>
	</tr>
	<tr>
		<td>
		<table style="width: 100%">
			<tr align="center">
				<td align="center" style="text-align:center;">
				<div class="curvo" id="GrupoCerrar" runat="server" visible="false">
				<asp:ImageButton id="imgCerrar" runat="server" Height="30px" Width="35px" 
                            ImageUrl="~/imagenes/BotonCerrar.jpg" />
					<asp:Label id="Label2" runat="server" Font-Size="11px" Text="Cerrar"></asp:Label>
				</div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</div>


<asp:Panel ID="pnlNuevaCartera" runat="server" Visible="false">
<div style="position:absolute; width:100% auto; top:60px; left:10%;" class="fondo1">
<table>
<tr>
<td>        
    <uc2:NuevoCLienteGestionarCliente ID="NuevoCLienteGestionarCliente1" 
        runat="server" titulo="INFORMACION CLIENTE Y OPERACION" ActivaGrupo1="false" ActivaGrupo2="true" ActivaGrupoMontos="false" />    
</td>
</tr>
</table>
</div>
</asp:Panel>


<asp:Panel ID="pnlModificarCartera" runat="server" Visible="false">
<div style="position:absolute; width:100% auto; top:60px; left:5%;" class="fondo1">
<table>
<tr>
<td>        
    <uc2:NuevoCLienteGestionarCliente ID="NuevoCLienteGestionarClienteM" 
        runat="server" titulo="INFORMACION CLIENTE Y OPERACION" ActivaGrupo1="false" ActivaGrupo2="true" ActivaGrupoMontos="false" />    
</td>
</tr>
</table>
</div>
</asp:Panel>