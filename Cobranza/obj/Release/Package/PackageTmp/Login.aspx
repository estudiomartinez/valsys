﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="Cobranza.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="Controles/CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc1" %>
<%@ Register src="~/Controles/CtCombo.ascx" tagname="CtCombo" tagprefix="uc3" %>
<%@ Register src="Controles/CtCombo.ascx" tagname="CtCombo" tagprefix="uc2" %>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<title>Acceso al Sistema</title>
<link href="Estilos.css" rel="stylesheet" type="text/css" />        
<link href="css/login.css" rel="stylesheet" type="text/css" />        
<style>
.Button
        {
            background-color: #000;
            color: #fff;
            font-size: 20px;
            width: 150px;
            font-weight: bold;
        }
        #mybutton:hover
        {
            background-color: #fff;
            color: #000;
        }
</style>
</head>
<body style="background-color:#<%=colorLogin1%>;" >  
    <form id="form1" runat="server">        
        <div id="Div1">
            reposne<table  class="curvoG1" align="center" style="background:#<%=colorLogin%>; margin-top:170px;"><tr>
            <td style="width:200px; background:<%=colorfondo%>;"> 
            	<table style="width:150px;" align="center"><tr><td class="<%=conGrupo%>"> 
                	<center><img width=240px height=<%=tablasize%>px src="Imagenes/<%=imgEmpresa1%>"></center> 
                </td></tr></table>
            </td>
            <td style="background:#<%=colorLogin1%>;">
                <table style="width:390px; color:<%=colorLetra%>; font-weight:bold; background:#<%=colorLogin%>;" align="center">
                	<tr><td colspan="2" align="center">
                	<center>
                	<table>
                	<tr>
                	<td class="curvoG1" style="width:155px; height:155px;" >                	
                	<img width=<%=imgsize%>px height=150px src="Imagenes/<%=imgEmpresa%>" ></td></tr>
                	</td>
                	</tr>
                	</table>
                	</center> 

                    <tr><td colspan=2 style="font-size:small;font-weight:bold;"> <center>I N I C I A R &nbsp;&nbsp; S E S I Ó N</center></td></tr>
                    <tr><td></td><td></td></tr>             
                    <tr><td style="font-size:small">Usuario</td><td><asp:TextBox ID="usuario" runat="server" AutoPostBack="true"></asp:TextBox></td></tr>
                    <tr><td td style="font-size:small">Empresa</td><td><uc2:CtCombo ID="cboEmpresa" runat="server" Longitud="250" Largo="25"  AutoPostBack="true" Procedimiento="QRYCO001"/></td></tr>
                    <tr><td style="font-size:small">Contraseña</td><td><asp:TextBox ID="clave" TextMode="Password" runat="server"></asp:TextBox></td></tr>
                    <%--<tr><td style="font-size:small">Perfil</td><td><uc2:CtCombo ID="cboPerfil" runat="server" Longitud="200" Largo="20" Activa="false" /></td></tr>--%>
                    <tr><td>&nbsp;</td></tr>
                    <tr><td colspan=2 align="center"><center>                                               
                    
                        <asp:Button ID="btnIngresar" runat="server" Text="Ingresar"    />
                        <asp:Button ID="Button2" runat="server" Text="Limpiar" /></center></td></tr>
                    
                    <tr><td>&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        
        <tr>
        <td></td> 
        <td colspan=2 align="center">
                        <center></center>
                        </td></tr>
        </table>       
    </div>
    
<uc1:CtlMensajes ID="CtlMensajes1" runat="server" />
    </form>
    <p align="center"></p>
        <p align="center">
            <asp:Label ID="lblMensaje" runat="server" Font-Names="Arial Narrow"  
                Font-Size="Small"  ForeColor="White" 
                Text="Derechos Reservados ESTUDIO MARTINEZ CONSULTORES & ABOGADOS"></asp:Label>
        </p>
        <p align="center">
            <asp:Label ID="lblMensaje0" runat="server" Font-Names="Arial Narrow"  
                Font-Size="Small"  ForeColor="White" 
                Text="www.estudiomartinez.com"></asp:Label>
        </p>
</form>
    </body>
</html>