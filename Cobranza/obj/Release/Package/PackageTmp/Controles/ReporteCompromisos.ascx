﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ReporteCompromisos.ascx.vb" Inherits="Controles.ReporteCompromisos" %>
<%@ Register src="CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc1" %>
<%@ Register src="CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc2" %>
<%@ Register src="CtCombo.ascx" tagname="CtCombo" tagprefix="uc3" %>
<table style="width: 100%;" align="center">
<tr align="center">
<td align="center" style="text-align:center;">
    <asp:Label id="lblTituloControl" runat="server" CssClass="titulo"></asp:Label>
    <uc2:CtlMensajes ID="CtlMensajes1" runat="server" />
    <asp:Timer ID="Timer1" runat="server" Enabled="False">
    </asp:Timer>
</td>
</tr>
</table>

	<table style="width: 100%">
		<tr>
			<td align="center" style="text-align:100%;" colspan="3">
	        <fieldset>
	        <legend>FILTRAR POR</legend>
	        <table style="width: 100%">
				<tr>
					<td>
			            <asp:Label id="Label2" runat="server" Font-Size="11px" Text="CARTERA"></asp:Label>
					<br />
			            <uc3:CtCombo ID="cboCartera" runat="server" Activa="true" 
                            Procedimiento="QRYC007" Condicion="" AutoPostBack="true" />
					</td>
					<td>
			            <asp:Label id="Label3" runat="server" Font-Size="11px" Text="GESTOR"></asp:Label>
					<br />
					    <uc3:CtCombo ID="cboGestor" runat="server" Activa="true" 
                            Condicion="" Longitud="100" />
					</td>
					<td>
					<div id="GrupoFechaInicio" runat="server">
			            FECHA INICIO
			            <br />
                        <asp:TextBox ID="txtFechaInicio" runat="server"  Width="80px" Enabled = "false" BackColor="White"/>
                        <img ID="txtFechaInicio" alt="calendario" height="16"  onclick="return showCalendar('txtFechaInicio','<%=txtFechaInicio.ClientID%>','%d/%m/%Y','24', true);" 
                        src="Imagenes/calendario.png" width="18" />							
					</div>
					</td>
					<td>
					<div id="GrupoFechaFin" runat="server">
			            FECHA FIN
					    <br />
                        <asp:TextBox ID="txtFechaFin" runat="server"  Width="80px" Enabled = "false" BackColor="White"/>
                        <img ID="txtFechaFin" alt="calendario" height="16" onclick="return showCalendar('txtFechaFin','<%=txtFechaFin.ClientID%>','%d/%m/%Y','24', true);" 
                        src="Imagenes/calendario.png" width="18" />
					</div>
					</td>
					<td align="center" style="text-align:center;">
					<div class="curvo" id="Div1" runat="server">
			        <asp:ImageButton id="imgBuscar" runat="server" Height="30px" 
                            ImageUrl="~/imagenes/boton busqueda.jpg" Width="45px" />
					<asp:Label id="Label19" runat="server" Font-Size="11px" Text="Buscar"></asp:Label>
					</div>
					</td>
					
					<td style="text-align:center;">
					<div class="curvo" id="GrupoReporte" runat="server" visible="false">
			        <asp:ImageButton id="imgGenerarReporte" runat="server" Height="30px" 
                            ImageUrl="~/imagenes/BotonGenerarReporte.jpg" Width="35px" />
					<br />
			        <asp:Label id="Label1" runat="server" Font-Size="11px" Text="Reporte"></asp:Label>
					</div>
					</td>
					
				</tr>
			</table>
	        </fieldset>			
			</td>
		</tr>
		<tr>
			<td colspan="3">
			<fieldset>
			<legend>RESULTADOS ENCONTRADOS</legend>
				<uc1:CtlGrilla ID="CtlGrilla1" runat="server" Largo="300px" Ancho="930px" Activa_ckeck="false" Activa_Delete="false" Activa_Edita="false" Activa_option="false" Desactiva_Boton="false"/>
			</fieldset>			
			</td>
		</tr>
		<tr align="center" style="text-align:center;">
		
		    <td style=" text-align:center;">
			<div class="curvoG" runat="server" visible="false" id="GrupoNoGestionado">
			<asp:ImageButton id="imgNoGestionados" runat="server" Height="30px" Width="35px" 
                            ImageUrl="~/imagenes/BotonGestiones.png" />
			<asp:Label id="Label5" runat="server" Font-Size="11px" Text="NO Gestionados"></asp:Label>
			</div>
			</td>
			<td align="center" style="text-align:center;">
			<div class="curvoG" id="GrupoReporteCompromiso" runat="server">
			<asp:ImageButton id="mgRpteCompromiso" runat="server" Height="30px" Width="35px" 
                    ImageUrl="~/imagenes/BotonReporte.jpg" />
			<asp:Label id="Label23" runat="server" Font-Size="9px" Text="Rpte Compro."></asp:Label>
			</div>
			<div class="modal" runat="server" visible="false" id="Carga1">
                   <div class="center">
                        <img alt="" src="Imagenes/loader.gif" />
                    </div>
            </div>
			
			</td>
		</tr>
	</table>