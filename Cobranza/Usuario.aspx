﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.Master" CodeBehind="Usuario.aspx.vb" Inherits="Cobranza.Usuario" 
    title="Usuarios" %>
    
<%@ Register src="~/Controles/CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc1" %>
<%@ Register src="~/Controles/CtCombo.ascx" tagname="CtCombo" tagprefix="uc2" %>
<%@ Register src="~/Controles/CtlTxt.ascx" tagname="CtlTxt" tagprefix="uc3" %>

<%@ Register src="~/Controles/NMUsuario.ascx" tagname="NMUsuario" tagprefix="uc4" %>

<%@ Register src="Controles/CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contenido" runat="server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<Triggers>
    <asp:PostBackTrigger ControlID ="NMUsuario1" />
</Triggers>
<ContentTemplate>


<table class="fondoPantalla" width="100%">
    <tr>
        <td colspan="2" class="titulo">
            <uc5:CtlMensajes ID="CtlMensajes1" runat="server" />
            <asp:Label ID="lblTitulo" runat="server" Text="USUARIOS" ForeColor="White"></asp:Label>
        </td>
    </tr>
	<tr>
		<td colspan="2">
		<fieldset>
		<legend> FILTRAR POR: </legend>
		<table width= "100%">
			<tr>
				<td width="200px"><asp:Label id="lblNombre" runat="server" Text="NOMBRES" ></asp:Label></td>
				<td width="200px"><asp:Label id="lblApellido" runat="server" Text="APELLIDOS" ></asp:Label></td>
                <td width="100px"><asp:Label id="lblPerfil" runat="server" Text="TIPO USUARIO"></asp:Label></td>
				<td><asp:Label id="lblUsuario" runat="server" Text="USUARIO"></asp:Label></td>
				<td></td>
				<td style="text-align:right" rowspan="2" width="30px">
				<div class="curvo">
				    <asp:ImageButton id="btnBuscar" runat="server" Height="30px" Width="30px" ToolTip="Buscar Usuarios" ImageUrl="~/Imagenes/BotonBusquedaPequena.png"/>
				    <asp:Label ID="lblBuscar" runat="server" Text="Buscar"></asp:Label>
				 </div>
				</td>
			</tr>
			<tr>
				<td><uc3:CtlTxt ID="txtNombres" runat="server" Ancho="100" /></td>
				<td><uc3:CtlTxt ID="txtApellidos" runat="server" Ancho="100" /></td>
				<td ><uc2:CtCombo ID="cboTipoUsuario" runat="server" longitud="100" /></td>
				<td><uc3:CtlTxt ID="txtUsuario" runat="server" Ancho="100" /></td>
				<td></td>
			</tr>
		</table>
		</fieldset></td>
	</tr>
	<tr>
		<td colspan="2">
		<fieldset>
		    <uc1:CtlGrilla ID="gvUsuario" runat="server" Ancho="950px" Largo="350px" Activa_ckeck="false" Desactiva_Boton="false" Activa_option="false"  OpocionNuevo="true" />
		</fieldset>
		</td>
	</tr>
</table>
<div id="DivMantenimiento" runat="server" style="height:auto; width:auto; position: absolute; left:30%; top:30%;">
    <uc4:NMUsuario ID="NMUsuario1" runat="server" visible="false" />
</div>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
