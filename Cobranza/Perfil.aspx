﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Perfil.aspx.vb" Inherits="Cobranza.Perfil"  MasterPageFile="~/MasterPage.Master" Title="Lista de Perfiles" %>

<%@ Register src="Controles/CtlTxt.ascx" tagname="CtlTxt" tagprefix="uc1" %>
<%@ Register src="Controles/CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc2" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="Contenido">
<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
<ProgressTemplate>
    <div class="modal">
        <div class="center">
            <img alt="" src="Imagenes/loader.gif" />
        </div>
    </div>
</ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" >
<ContentTemplate>
<table width="100%" class="fondoPantalla">
<tr>
    <td class="titulo"><asp:Label ID="lblTitulo" runat="server" Text="PERFILES" ForeColor="White"></asp:Label></td>
</tr>
<tr>
    <td>
        <fieldset>
        <table width="100%">
        <tr>
            <td width="100px"><asp:Label ID="lblNombre" runat="server" Text="NOMBRE DE PERFIL: " /></td>
            <td><uc1:CtlTxt ID="txtNombre" runat="server" Ancho ="100" /></td>
            <td style="text-align:right" width="30px">
				<div class="curvo">
				    <asp:ImageButton id="btnBuscar" runat="server" Height="30px" Width="30px" ToolTip="Buscar Perfiles" ImageUrl="~/Imagenes/BotonBusquedaPequena.png"/>
				    <asp:Label ID="lblBuscar" runat="server" Text="Buscar"></asp:Label>
				 </div>
			</td>
        </tr>
        </table>
        </fieldset>
    </td>
</tr>
<tr>
    <td>
        <fieldset>
            <uc2:CtlGrilla ID="gvPerfiles" runat="server" Ancho="950px" Largo="350px" Activa_ckeck="false" Desactiva_Boton="false" Activa_option="false"  OpocionNuevo="true" />
        </fieldset>
    </td>
</tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
