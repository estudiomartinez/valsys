﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.Master" CodeBehind="Candado.aspx.vb" Inherits="Cobranza.Candado"
    title="Administracion de candados del tipo fecha aplicado a la Gestion" %>

<%@ Register src="Controles/AdministrarCandadoCarteras.ascx" tagname="AdministrarCandado" tagprefix="uc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="Contenido" runat="server">
<asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
<ProgressTemplate>
    <div class="modal">
        <div class="center">
            <img alt="" src="Imagenes/loader.gif" />
        </div>
    </div>
</ProgressTemplate>
</asp:UpdateProgress>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<table  style="width:100%; text-align:center;">
<tr>
<td style="width:100%;text-align:center;" class="fondoPantalla">
    <uc1:AdministrarCandado ID="AdministrarCandadoCarteras1" runat="server" Titulo="CONSULTA DE CANDADO POR CARTERAS"/>
</td>
</tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>