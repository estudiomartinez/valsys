﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.Master" CodeBehind="Propuesta.aspx.vb" Inherits="Cobranza.Formulario_web125" 
    title="Propuesta" %>
<%@ Register src="Controles/CtlMensajes.ascx" tagname="CtlMensajes" tagprefix="uc1" %>
<%@ Register src="Controles/CtCombo.ascx" tagname="CtCombo" tagprefix="uc2" %>
<%@ Register src="Controles/CtlGrilla.ascx" tagname="CtlGrilla" tagprefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Contenido" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
<ProgressTemplate>
    <div class="modal">
        <div class="center">
            <img alt="" src="Imagenes/loader.gif" />
        </div>
    </div>
</ProgressTemplate>
</asp:UpdateProgress>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<table  style="width:100%; text-align:center;">
<tr>
<td style="width:100%;text-align:center;" class="fondoPantalla">
    <table style="width: 100%;" align="center">
        <tr>
            <td align="center" class="titulo">
                CONSULTA PROPUESTA
                <uc1:CtlMensajes ID="CtlMensajes1" runat="server" />
            </td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td>
                <fieldset>
                <table>
                    <tr>
                        <td>                
                            EMPRESA
                            <br />
                            <uc2:CtCombo ID="cboEmpresa" runat="server" Procedimiento="QRYMG002" AutoPostBack="true"/>                
                        </td>
                        <td>
                            CARTERA
                            <br />
                            <uc2:CtCombo ID="cboCartera" runat="server" Procedimiento="QRYC014" Longitud="100" />                
                        </td>
                        <td>
                            ESTADO
                            <br />
                            <uc2:CtCombo ID="cboEstado" runat="server" Condicion=":idtabla▓117" Procedimiento="SQL_N_GEST012" />                
                        </td>
                        <td>
			                FECHA INICIO
	                        <br />
                            <asp:TextBox ID="txtFechaInicio" runat="server"  Width="80px" Enabled = "false" BackColor="White"/>
                            <img ID="imgFechaInicio" alt="calendario" height="16"  onclick="return showCalendar('imgFechaInicio','<%=txtFechaInicio.ClientID%>','%d/%m/%Y','24', true);" 
                            src="Imagenes/calendario.png" width="18" />							
			            </td>
			            <td>
	                        FECHA FIN
			                <br />
                            <asp:TextBox ID="txtFechaFin" runat="server"  Width="80px" Enabled = "false" BackColor="White"/>
                            <img ID="imgFechaFin" alt="calendario" height="16" onclick="return showCalendar('imgFechaFin','<%=txtFechaFin.ClientID%>','%d/%m/%Y','24', true);" 
                            src="Imagenes/calendario.png" width="18" />
			            </td>
			            <td>
			                <div class="curvo" id="Div1" runat="server">
		                    <asp:ImageButton id="imgBuscar" runat="server" Height="30px" 
                                    ImageUrl="~/imagenes/boton busqueda.jpg" Width="45px" />
				            <asp:Label id="Label19" runat="server" Font-Size="11px" Text="Buscar"></asp:Label>
				            </div>
			            </td>
                    </tr>
                </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>                
                <fieldset>
                <uc3:CtlGrilla ID="gvPropuesta" runat="server" Activa_ckeck="false" Activa_Delete="false" Activa_option="false" Desactiva_Boton="false" OpocionNuevo="false" With_Grilla="1400px" Activa_Edita="false" Largo="500px"  />                
                </fieldset>
            </td>
        </tr>
    </table>    
</td>
</tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
